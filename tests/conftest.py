import pytest
import pathlib

DATADIR = pathlib.Path(__file__).absolute().parent / "data"


@pytest.fixture
def dataset_flat():
    return DATADIR / "flat(5).txt"

# make fixture 