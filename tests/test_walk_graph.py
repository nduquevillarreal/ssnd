import unittest
import numpy as np
import pandas as pd
from ssnd.algorithm import *


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        # PIPES INTERNAL ROUGHNESS
        self.material = "pvc"

        # COMMERCIAL DIAMETERS
        self.commercial_diameters = [
            float(val)
            for val in (0.225, 0.25, 0.35, 0.4, 0.5, 0.6, 0.80, 1, 1.20, 1.5, 2, 2.5, 3)
        ]

        # MINIMUM AND MAXIMUM VELOCITY
        self.v_min = 0.75
        self.v_max = 10

        # EXCAVATION LIMITS
        self.min_depth = 1.2  # Minimum excavation depth [m]
        self.max_depth = 10  # Maximum excavation depth [m]

        # KINEMATIC VISCOSITY OF WATER
        self.nu = round(1.14 / pow(10, 6), 8)

        self.manholes = pd.DataFrame.from_dict(
            [
                {"id": 1, "x": 1.0, "y": 13.0, "z": 12.0, "outlet": 0},
                {"id": 2, "x": 2.0, "y": 3.0, "z": 11.5, "outlet": 0},
                {"id": 3, "x": 8.0, "y": 9.0, "z": 11.5, "outlet": 0},
                {"id": 4, "x": 15.0, "y": 11.0, "z": 10.5, "outlet": 0},
                {"id": 5, "x": 14.0, "y": 1.0, "z": 10.0, "outlet": 1},
            ]
        )
        self.pipes = pd.DataFrame.from_dict(
            [
                {"id_up": 1, "id_down": 3, "type": 1, "design_flow": 0.2},
                {"id_up": 2, "id_down": 3, "type": 1, "design_flow": 0.1},
                {"id_up": 3, "id_down": 5, "type": 2, "design_flow": 0.6},
                {"id_up": 4, "id_down": 5, "type": 1, "design_flow": 0.4},
            ]
        )

    def test_algorithm(self):

        # Input data
        min_slopes = [
            0.0016859,
            0.0014650,
            0.0009354,
            0.0007828,
            0.0005813,
            0.0004559,
            0.0002998,
            0.0002227,
            0.0001746,
            0.0001297,
            0.0000887,
            0.0000659,
            0.0000516,
        ]
        max_slopes = [
            0.2997279,
            0.2604457,
            0.1662952,
            0.1391737,
            0.1033579,
            0.0810529,
            0.0533128,
            0.0395930,
            0.0310487,
            0.0230584,
            0.0157765,
            0.0117165,
            0.0091880,
        ]
        max_radius = [
            0.0666527,
            0.0740586,
            0.1036821,
            0.1184938,
            0.1481173,
            0.1777407,
            0.2433546,
            0.3041932,
            0.3650319,
            0.4562898,
            0.6065342,
            0.7581678,
            0.9098013,
        ]
        max_areas = [
            0.0297285,
            0.0367018,
            0.0719356,
            0.0939567,
            0.1468074,
            0.2114027,
            0.4310875,
            0.6735743,
            0.9699470,
            1.5155423,
            2.8460938,
            4.4470215,
            6.4037110,
        ]

        xy_distance = {
            1: 8.06225774829855,
            2: 8.48528137423857,
            3: 10.0,
            4: 10.04987562112089,
        }
        terrain_slopes = {
            1: 0.062017367294604234,
            2: 0.0,
            3: 0.15,
            4: 0.04975185951049946,
        }

        ideal_slope = 0.062017367294604234  # terrain slope for outer pipe (1,3)
        design_discharge = 0.2
        diameter_idx = 0

        # Expected result
        r_elevation_invert = []
        r_chosen_slopes = []
        pumps = []
        deepened = []
        diameter_tracker = []

        pipes = walk_graph(
            manholes=self.manholes,
            pipes=self.pipes,
            v_min=self.v_min,
            v_max=self.v_max,
            material=self.material,
            min_depth=self.min_depth,
            max_depth=self.max_depth,
            diameters=self.commercial_diameters,
            verbose=False,
        )

        # self.assertTrue(
        #     np.isclose(chosen_slopes, r_chosen_slopes, atol=10 ** (-5)).all(),
        #     "calculation of the best slope using the {} diameter failed!\n{}".format(elevation_invert, chosen_slopes),
        # )


if __name__ == "__main__":
    unittest.main()
