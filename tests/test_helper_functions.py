import unittest
import numpy as np


class TestHelperFunctions(unittest.TestCase):
    #     def test_complex_network(self):

    #         ###############################################################
    #         #### data that would be read or calculated!!
    #         manholes_read = list(range(17))[1:]
    #         manholes_height_top = dict(
    #             zip(
    #                 manholes_read,
    #                 [
    #                     120,
    #                     119.8,
    #                     119,
    #                     119.7,
    #                     117.9,
    #                     118.1,
    #                     116,
    #                     119.5,
    #                     118,
    #                     117.5,
    #                     115,
    #                     114.5,
    #                     113.5,
    #                     114,
    #                     113,
    #                     115,
    #                 ],
    #             )
    #         )
    #         manholes_slope = dict(zip(manholes_read, [0.01] * 15))
    #         manholes_length = dict(
    #             zip(
    #                 manholes_read,
    #                 [100, 10, 30, 110, 10, 20, 100, 100, 60, 100, 100, 300, 100, 400, 20],
    #             )
    #         )

    #         edges_read = np.array(
    #             [
    #                 (1, 2, 1),
    #                 (2, 3, 0),
    #                 (3, 5, 0),
    #                 (4, 3, 1),
    #                 (8, 6, 1),
    #                 (9, 6, 1),
    #                 (10, 6, 1),
    #                 (6, 5, 0),
    #                 (5, 7, 0),
    #                 (11, 14, 1),
    #                 (12, 14, 1),
    #                 (14, 15, 0),
    #                 (13, 15, 1),
    #                 (15, 7, 0),
    #                 (7, 16, 0),
    #             ]
    #         )
    #         last_manhole = 16
    #         ###############################################################

    #         manholes_height_bottom, pumps = walk_graph(
    #             manholes_height_top,
    #             manholes_slope,
    #             manholes_length,
    #             edges_read,
    #             last_manhole,
    #             0.5,
    #             5,
    #         )
    #         manholes_expected = {
    #             1: 119.5,
    #             2: 118.5,
    #             3: 118.1,
    #             4: 119.2,
    #             5: 115.8,
    #             6: 116,
    #             7: 112.3,
    #             8: 119,
    #             9: 117.5,
    #             10: 117,
    #             11: 114.5,
    #             12: 114,
    #             13: 113,
    #             14: 113.5,
    #             15: 112.5,
    #             16: 111.3,
    #         }
    #         # necessary because of floating representation of floats in dicts
    #         result_rounded = {}
    #         for key in manholes_height_bottom:
    #             result_rounded[key] = round(manholes_height_bottom[key], 2)

    #         self.assertEqual(result_rounded, manholes_expected, "Test complex network failed!")
    #         self.assertEqual(
    #             pumps,
    #             {14: (111, 113.5), 15: (109.5, 112.5)},
    #             "Test complex network failed - incorrect pump placements!",
    #         )
    # #
    pass


if __name__ == "__main__":
    unittest.main()
