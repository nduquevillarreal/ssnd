import unittest
import numpy as np
import pandas as pd
from ssnd.algorithm import *
from ssnd.hydraulic_parameters import *

# from ssnd.globals import *


class TestWalkGraph(unittest.TestCase):
    def test_walk_graph(self):
        """

        :return:
        """

        " Flat terrain (5 manholes)"
        # elevation = {1.0: 10.0, 2.0: 10.0, 3.0: 10.0, 4.0: 10.0, 5.0: 10.0}
        # manholes_outer = [1, 2, 4]
        # slopes = {1: {1.0: [0.001, 0.1]},
        #           2: {1.0: [0.01, 0.1]},
        #           3: {1.0: [0.001, 0.1]},
        #           4: {1.0: [0.001, 0.1]}}
        # pipes = {1: 3, 2: 3, 3: 5, 4: 5}
        # diameter_tracker = {1: 1, 2: 1, 3: 1, 4: 1, 5: 1}
        # terrain_slopes = {1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0}
        # xy_distance = {1: 10, 2: 10, 3: 10.0, 4: 10.0}
        # elevation_invert = {1.0: 10.0, 2.0: 10.0, 3.0: 10.0, 4.0: 10.0, 5.0: 10.0}
        # manhole_inflows_count = {3: 0, 5: -1}
        # manhole_inflows = {3: 2, 5: 2}
        #
        # # Results
        # r_elev = {1.0: 8.5, 2.0: 8.5, 3.0: 8.4, 4.0: 8.5, 5.0: 8.39}
        # r_pumps = {}
        # r_deepened = {}
        # r_choosen_slopes = {1: 0.001, 2: 0.01, 3: 0.001, 4: 0.001}
        # min_depth = 0.5
        # max_depth=5

        " Inclined plane (5 manholes)"
        elevation = {1.0: 10.0, 2.0: 10.0, 3.0: 8.0, 4.0: 6.0, 5.0: 4.0}
        manholes_outer = [1, 2, 4]
        slopes = {
            1: {1.0: [0.001, 0.1]},
            2: {1.0: [0.01, 0.1]},
            3: {1.0: [0.001, 0.1]},
            4: {1.0: [0.001, 0.1]},
        }
        pipes = {1: 3, 2: 3, 3: 5, 4: 5}
        diameter_tracker = {1: 1, 2: 1, 3: 1, 4: 1, 5: 1}
        terrain_slopes = {1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0}
        xy_distance = {1: 10, 2: 10, 3: 10.0, 4: 10.0}
        elevation_invert = {1.0: 10.0, 2.0: 10.0, 3.0: 8.0, 4.0: 6.0, 5.0: 4.0}
        manhole_inflows_count = {3: 0, 5: -1}
        manhole_inflows = {3: 2, 5: 2}
        min_depth = 0.5
        max_depth = 5

        # Results
        r_elev = {1.0: 0.99, 2.0: 0.9, 3.0: 0.99, 4.0: 0.99, 5.0: 3.5}
        r_choosen_slopes = {1: 0.001, 2: 0.01, 3: 0.001, 4: 0.001}
        r_pumps = {}
        r_deepened = {1: (0.5, 0.99), 2: (0.5, 0.9), 3: (0.5, 0.99), 4: (0.5, 0.99)}

        # Run algorithm
        prepared_data = {
            "elevation": elevation,
            "manholes_outer": manholes_outer,
            "slopes": slopes,
            "pipes": pipes,
            "diameter_tracker": diameter_tracker,
            "terrain_slopes": terrain_slopes,
            "xy_distance": xy_distance,
            "elevation_invert": elevation_invert,
            "manhole_inflows_count": manhole_inflows_count,
            "manhole_inflows": manhole_inflows,
        }

        final_elevation_invert, chosen_slopes, pumps, deepened = walk_graph(
            prepared_data, min_depth, max_depth, verbose=False
        )

        self.assertEqual(final_elevation_invert, r_elev, "Invert elevations failed!")

        self.assertEqual(chosen_slopes, r_choosen_slopes, "The slopes are not right!")

        self.assertEqual(pumps, r_pumps, "There should not be any pumps!")

        self.assertEqual(
            deepened, r_deepened, "There should not be any deepened manholes!"
        )

    def test_walk_graph(self):
        """

        :return:
        """

        " Flat terrain (5 manholes)"
        # elevation = {1.0: 10.0, 2.0: 10.0, 3.0: 10.0, 4.0: 10.0, 5.0: 10.0}
        # manholes_outer = [1, 2, 4]
        # slopes = {1: {1.0: [0.001, 0.1]},
        #           2: {1.0: [0.01, 0.1]},
        #           3: {1.0: [0.001, 0.1]},
        #           4: {1.0: [0.001, 0.1]}}
        # pipes = {1: 3, 2: 3, 3: 5, 4: 5}
        # diameter_tracker = {1: 1, 2: 1, 3: 1, 4: 1, 5: 1}
        # terrain_slopes = {1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0}
        # xy_distance = {1: 10, 2: 10, 3: 10.0, 4: 10.0}
        # elevation_invert = {1.0: 10.0, 2.0: 10.0, 3.0: 10.0, 4.0: 10.0, 5.0: 10.0}
        # manhole_inflows_count = {3: 0, 5: -1}
        # manhole_inflows = {3: 2, 5: 2}
        #
        # # Results
        # r_elev = {1.0: 8.5, 2.0: 8.5, 3.0: 8.4, 4.0: 8.5, 5.0: 8.39}
        # r_pumps = {}
        # r_deepened = {}
        # r_choosen_slopes = {1: 0.001, 2: 0.01, 3: 0.001, 4: 0.001}
        # min_depth = 0.5
        # max_depth=5

        " Inclined plane (5 manholes)"
        elevation = {1.0: 10.0, 2.0: 10.0, 3.0: 8.0, 4.0: 6.0, 5.0: 4.0}
        manholes_outer = [1, 2, 4]
        slopes = {
            1: {1.0: [0.001, 0.1]},
            2: {1.0: [0.01, 0.1]},
            3: {1.0: [0.001, 0.1]},
            4: {1.0: [0.001, 0.1]},
        }
        pipes = {1: 3, 2: 3, 3: 5, 4: 5}
        diameter_tracker = {1: 1, 2: 1, 3: 1, 4: 1, 5: 1}
        terrain_slopes = {1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0}
        xy_distance = {1: 10, 2: 10, 3: 10.0, 4: 10.0}
        elevation_invert = {1.0: 10.0, 2.0: 10.0, 3.0: 8.0, 4.0: 6.0, 5.0: 4.0}
        manhole_inflows_count = {3: 0, 5: -1}
        manhole_inflows = {3: 2, 5: 2}
        min_depth = 0.5
        max_depth = 5

        # Results
        r_elev = {1.0: 0.99, 2.0: 0.9, 3.0: 0.99, 4.0: 0.99, 5.0: 3.5}
        r_choosen_slopes = {1: 0.001, 2: 0.01, 3: 0.001, 4: 0.001}
        r_pumps = {}
        r_deepened = {1: (0.5, 0.99), 2: (0.5, 0.9), 3: (0.5, 0.99), 4: (0.5, 0.99)}

        # Run algorithm
        prepared_data = {
            "elevation": elevation,
            "manholes_outer": manholes_outer,
            "slopes": slopes,
            "pipes": pipes,
            "diameter_tracker": diameter_tracker,
            "terrain_slopes": terrain_slopes,
            "xy_distance": xy_distance,
            "elevation_invert": elevation_invert,
            "manhole_inflows_count": manhole_inflows_count,
            "manhole_inflows": manhole_inflows,
        }

        final_elevation_invert, chosen_slopes, pumps, deepened = walk_graph(
            prepared_data, min_depth, max_depth, verbose=False
        )

        self.assertEqual(final_elevation_invert, r_elev, "Invert elevations failed!")

        self.assertEqual(chosen_slopes, r_choosen_slopes, "The slopes are not right!")

        self.assertEqual(pumps, r_pumps, "There should not be any pumps!")

        self.assertEqual(
            deepened, r_deepened, "There should not be any deepened manholes!"
        )

    def test_minmax_slopes(self):
        "Flat terrain (5 manholes)"
        network_dict = {
            "manholes": [
                {"id": 1, "x": 1.0, "y": 13.0, "z": 10.0, "outfall": 0},
                {"id": 2, "x": 2.0, "y": 3.0, "z": 10.0, "outfall": 0},
                {"id": 3, "x": 8.0, "y": 9.0, "z": 10.0, "outfall": 0},
                {"id": 4, "x": 15.0, "y": 11.0, "z": 10.0, "outfall": 0},
                {"id": 5, "x": 14.0, "y": 1.0, "z": 10.0, "outfall": 1},
            ],
            "pipes": [
                {"id_up": 1, "id_down": 3, "type": 1, "design_flow": 0.2},
                {"id_up": 2, "id_down": 3, "type": 1, "design_flow": 0.1},
                {"id_up": 3, "id_down": 5, "type": 2, "design_flow": 0.6},
                {"id_up": 4, "id_down": 5, "type": 1, "design_flow": 0.4},
            ],
        }
        commercial_diameters = [
            float(val)
            for val in (0.225, 0.25, 0.35, 0.4, 0.5, 0.6, 0.80, 1, 1.20, 1.5, 2, 2.5, 3)
        ]

        # Data for PVC pipes
        v_min = 0.75
        v_max = 10
        n = 0.009
        roughness = 0.00003

        delta_slope = 0.0005
        max_iteration = 1000

        # Results
        r_min_slopes = [
            0.00169,
            0.00147,
            0.00094,
            0.00078,
            0.00058,
            0.00046,
            0.00030,
            0.00022,
            0.00017,
            0.00013,
            0.00009,
            0.00007,
            0.00005,
        ]
        r_max_slopes = [
            0.29973,
            0.26045,
            0.16630,
            0.13917,
            0.10336,
            0.08105,
            0.05331,
            0.03959,
            0.03105,
            0.02306,
            0.01578,
            0.01172,
            0.00919,
        ]

        # Run algorithm -----------------------------------------------------------------------------------------------
        manholes, pipes = network_to_dataframe(network_dict)

        min_slopes, max_slopes, _, _, _, _ = prepare_hydraulics_data(
            pipes, commercial_diameters, v_min, v_max, n, verbose=False
        )
        # end algorithm -----------------------------------------------------------------------------------------------
        print(min_slopes, r_min_slopes)
        print((min_slopes - r_min_slopes))
        # check results
        self.assertTrue(
            np.isclose(min_slopes, r_min_slopes, atol=10 ** (-5)).all(),
            "calculation of minimum slopes with full pipe failed!",
        )
        self.assertTrue(
            np.isclose(max_slopes, r_max_slopes, atol=10 ** (-5)).all(),
            "calculation of maximum slopes with full pipe failed!",
        )
        # chosen_slopes = find_valid_slopes_diameters(pipes, commercial_diameters, v_min=0.75, v_max=10.0, n=0.009,
        #                                      roughness=0.00003, delta_slope=0.0005, max_iteration=1000, max_slope=0.11)
        # final_elevation_invert, chosen_slopes, pumps, deepened = walk_graph(prepared_data, max_depth, verbose=False)

    def test_valid_slopes(self):

        # input data ---------------------------------------------------------------------------------------------------
        "Flat terrain (5 manholes)"
        network_dict = {
            "manholes": [
                {"id": 1, "x": 1.0, "y": 13.0, "z": 10.0, "outfall": 0},
                {"id": 2, "x": 2.0, "y": 3.0, "z": 10.0, "outfall": 0},
                {"id": 3, "x": 8.0, "y": 9.0, "z": 10.0, "outfall": 0},
                {"id": 4, "x": 15.0, "y": 11.0, "z": 10.0, "outfall": 0},
                {"id": 5, "x": 14.0, "y": 1.0, "z": 10.0, "outfall": 1},
            ],
            "pipes": [
                {"id_up": 1, "id_down": 3, "type": 1, "design_flow": 0.2},
                {"id_up": 2, "id_down": 3, "type": 1, "design_flow": 0.1},
                {"id_up": 3, "id_down": 5, "type": 2, "design_flow": 0.6},
                {"id_up": 4, "id_down": 5, "type": 1, "design_flow": 0.4},
            ],
        }

        self.pipes = pd.DataFrame.from_dict(
            [
                {"id_up": 1, "id_down": 3, "type": 1, "design_flow": 0.2},
                {"id_up": 2, "id_down": 3, "type": 1, "design_flow": 0.1},
                {"id_up": 3, "id_down": 5, "type": 2, "design_flow": 0.6},
                {"id_up": 4, "id_down": 5, "type": 1, "design_flow": 0.4},
            ]
        )

        mh_up = 0

        commercial_diameters = [
            float(val)
            for val in (0.225, 0.25, 0.35, 0.4, 0.5, 0.6, 0.80, 1, 1.20, 1.5, 2, 2.5, 3)
        ]

        design_discharge = None
        ideal_slope = None
        diameter_idx = None
        min_slopes = None
        max_slopes = None
        filling_hydraulic_radius = None
        max_areas = None

        # Data for PVC pipes
        v_min = 0.75
        v_max = 10
        n = 0.009
        roughness = 0.00003

        delta_slope = 0.0005
        max_iteration = 1000
        # end of input data --------------------------------------------------------------------------------------------

        # Results
        r_min_slopes = {
            1: {
                0.225: [0.135686, 0.299686],
                0.25: [0.077465, 0.260446],
                0.35: [0.012935, 0.166295],
                0.4: [0.006783, 0.139174],
                0.5: [0.002081, 0.103358],
                0.6: [0.001456, 0.081053],
                0.8: [0.001300, 0.053313],
                1.0: [0.001723, 0.039593],
                1.2: [0.001675, 0.031049],
                1.5: [0.001630, 0.023058],
                2.0: [0.001589, 0.015777],
                2.5: [0.002066, 0.011717],
                3.0: [0.002052, 0.009188],
            },
            2: {
                0.225: [0.034186, 0.299686],
                0.25: [0.019465, 0.260446],
                0.35: [0.003435, 0.166295],
                0.4: [0.001783, 0.139174],
                0.5: [0.002081, 0.103358],
                0.6: [0.001956, 0.081053],
                0.8: [0.002300, 0.053313],
                1.0: [0.002223, 0.039593],
                1.2: [0.002175, 0.031049],
                1.5: [0.002130, 0.023058],
                2.0: [0.002589, 0.015777],
                2.5: [0.002566, 0.011717],
                3.0: [0.003052, 0.009188],
            },
            3: {
                0.35: [0.115935, 0.166295],
                0.4: [0.056783, 0.139174],
                0.5: [0.017581, 0.103358],
                0.6: [0.006956, 0.081053],
                0.8: [0.001300, 0.053313],
                1.0: [0.001223, 0.039593],
                1.2: [0.001175, 0.031049],
                1.5: [0.001130, 0.023059],
                2.0: [0.001089, 0.015777],
                2.5: [0.001066, 0.011717],
                3.0: [0.001052, 0.009188],
            },
            4: {
                0.35: [0.051435, 0.166295],
                0.4: [0.025283, 0.139174],
                0.5: [0.008081, 0.103358],
                0.6: [0.002956, 0.081053],
                0.8: [0.001300, 0.053313],
                1.0: [0.001223, 0.039593],
                1.2: [0.001175, 0.031049],
                1.5: [0.001130, 0.023059],
                2.0: [0.001089, 0.015777],
                2.5: [0.001566, 0.011717],
                3.0: [0.001552, 0.009188],
            },
        }

        # Run algorithm -----------------------------------------------------------------------------------------------
        manholes, pipes = network_to_dataframe(network_dict)
        chosen_slopes, d_idx = find_best_diameter_slope(
            self.pipes,
            mh_up,
            design_discharge,
            ideal_slope,
            diameter_idx,
            min_slopes,
            max_slopes,
            filling_hydraulic_radius,
            max_areas,
            v_min,
            v_max,
            n,
            roughness,
            diameters=commercial_diameters,
            verbose=False,
        )
        # print(chosen_slopes)
        for m_id in list(chosen_slopes):
            for d, val in chosen_slopes[m_id].copy().items():
                chosen_slopes[m_id][d] = np.round(val, decimals=6)
        # print(chosen_slopes)
        # end algorithm -----------------------------------------------------------------------------------------------

        # check results
        # print(chosen_slopes)
        self.assertTrue(
            np.isclose(list(chosen_slopes), list(r_min_slopes), atol=10 ** (-5)).all(),
            "calculation of minimum feasible slopes failed!",
        )
        # final_elevation_invert, chosen_slopes, pumps, deepened = walk_graph(prepared_data, max_depth, verbose=False)

    def test_depths(self):
        # random value init!
        # checking if all max depth are kept!
        ###############################################################
        #### data that would be read or calculated!!

        "Flat terrain (5 manholes)"
        elevation = {1.0: 10.0, 2.0: 10.0, 3.0: 10.0, 4.0: 10.0, 5.0: 10.0}
        manholes_outer = [1, 2, 4]
        slopes = {
            1: {1.0: [0.001, 0.1]},
            2: {1.0: [0.01, 0.1]},
            3: {1.0: [0.001, 0.1]},
            4: {1.0: [0.001, 0.1]},
        }
        pipes = {1: 3, 2: 3, 3: 5, 4: 5}
        diameter_tracker = {1: 1, 2: 1, 3: 1, 4: 1, 5: 1}
        terrain_slopes = {1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0}
        xy_distance = {1: 10, 2: 10, 3: 10.0, 4: 10.0}
        elevation_invert = {1.0: 10.0, 2.0: 10.0, 3.0: 10.0, 4.0: 10.0, 5.0: 10.0}
        manhole_inflows_count = {3: 0, 5: -1}
        manhole_inflows = {3: 2, 5: 2}
        min_depth = 0.5
        max_depth = 5

        # Results
        check = {1.0: True, 2.0: True, 3.0: True, 4.0: True, 5.0: True}

        # Run algorithm ----------------------------------------------------------------------------------------------
        prepared_data = {
            "elevation": elevation,
            "manholes_outer": manholes_outer,
            "slopes": slopes,
            "pipes": pipes,
            "diameter_tracker": diameter_tracker,
            "terrain_slopes": terrain_slopes,
            "xy_distance": xy_distance,
            "elevation_invert": elevation_invert,
            "manhole_inflows_count": manhole_inflows_count,
            "manhole_inflows": manhole_inflows,
        }
        final_elevation_invert, _, _, _ = walk_graph(
            prepared_data, min_depth, max_depth, verbose=False
        )
        b = check_depth(elevation, final_elevation_invert, min_depth, max_depth)
        # end algorithm ----------------------------------------------------------------------------------------------

        self.assertTrue(
            (check == b), "Manholes did not fulfill the min depth max depth criteria!"
        )

    #############################
    # [TO UPDATE]

    def test_too_long_pipe(self):
        ###############################################################
        #### graph layout
        # 1->2->3

        ###############################################################
        #### data that would be read or calculated!!
        manholes_read = [1, 2, 3]
        manholes_height_top = dict(zip(manholes_read, [120, 120, 120]))
        manholes_slope = dict(zip(manholes_read, [0.01, 0.01]))
        manholes_length = dict(zip(manholes_read, [100, 500]))

        edges_read = np.array([(1, 2, 1), (2, 3, 0)])
        last_manhole = 3
        ###############################################################

        self.assertRaises(
            ValueError,
            walk_graph,
            manholes_height_top,
            manholes_slope,
            manholes_length,
            edges_read,
            last_manhole,
            0.5,
            5,
        )


if __name__ == "__main__":
    unittest.main()
