def test_main(dataset_flat):
    from ssnd import walk_graph, read_network_file, network_to_dataframe

    manholes, pipes = network_to_dataframe(read_network_file(dataset_flat), False)

    design_pipes = walk_graph(
        manholes=manholes,
        pipes=pipes,
    )

    assert not design_pipes.empty
