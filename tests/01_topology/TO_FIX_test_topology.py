import pathlib
import unittest
import pandas as pd

from pathlib import Path

# --- Spatial geometry ---
# import geopandas as gpd
from shapely.geometry import Point, Polygon, LineString

from ssnd.topology_delineation import *


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        # Import Blocks .shp file from UB in geopandas dataframe
        # File should be in the current working directory
        self.filename = r"../01_topology/test_data_topology/200_(2018)_Blocks.shp"
        self.blocks = import_blocks_to_geopandas(self.filename)

    def test_import_blocks_to_geopandas(self):
        columns = [
            "geometry",
            "BlockID",
            "BasinID",
            "CentreX",
            "CentreY",
            "Neighbours",
            "Active",
            "pLU_RES",
            "pLU_COM",
            "pLU_ORC",
            "pLU_LI",
            "pLU_HI",
            "pLU_CIV",
            "pLU_SVU",
            "pLU_RD",
            "pLU_TR",
            "pLU_PG",
            "pLU_REF",
            "pLU_UND",
            "pLU_NA",
            "pLU_WAT",
            "pLU_FOR",
            "pLU_AGR",
            "Population",
            "AvgElev",
            "MaxElev",
            "MinElev",
            "HasRiver",
            "RiverNames",
            "HasLake",
            "LakeNames",
            "Total_jobs",
            "Blk_WD",
            "Blk_WW",
        ]

        blocks1 = import_blocks_to_geopandas(
            self.filename, columns=columns, module="xx"
        )  # returns geopandas dataframe with the blocks and Data from UrbanBEATS
        self.assertEqual(
            list(blocks1.columns),
            columns,
            msg="Block columns do not coincide with input parameter columns",
        )  # add assertion here

        columns_complete = [
            "BlockID",
            "BasinID",
            "CentreX",
            "CentreY",
            "Neighbours",
            "Active",
            "pLU_RES",
            "pLU_COM",
            "pLU_ORC",
            "pLU_LI",
            "pLU_HI",
            "pLU_CIV",
            "pLU_SVU",
            "pLU_RD",
            "pLU_TR",
            "pLU_PG",
            "pLU_REF",
            "pLU_UND",
            "pLU_NA",
            "pLU_WAT",
            "pLU_FOR",
            "pLU_AGR",
            "Population",
            "AvgElev",
            "MaxElev",
            "MinElev",
            "Slope_PCT",
            "Slope_DEG",
            "Aspect_DEG",
            "HasRiver",
            "RiverNames",
            "HasLake",
            "LakeNames",
            "MiscAtot",
            "MiscAimp",
            "MiscThresh",
            "UND_Type",
            "UND_av",
            "OpenSpace",
            "AGreenOS",
            "ASquare",
            "PG_av",
            "REF_av",
            "ANonW_Util",
            "SVU_avWS",
            "SVU_avWW",
            "SVU_avSW",
            "SVU_avOTH",
            "RoadTIA",
            "ParkBuffer",
            "RD_av",
            "HouseOccup",
            "ResParcels",
            "ResFrontT",
            "avSt_RES",
            "WResNstrip",
            "ResAllots",
            "ResDWpLot",
            "ResHouses",
            "ResLotArea",
            "ResRoof",
            "avLt_RES",
            "ResHFloors",
            "ResLotTIA",
            "ResLotEIA",
            "ResGarden",
            "ResRoofCon",
            "ResLotALS",
            "ResLotARS",
            "HDRFlats",
            "HDRRoofA",
            "HDROccup",
            "HDR_TIA",
            "HDR_EIA",
            "HDRFloors",
            "av_HDRes",
            "HDRGarden",
            "HDRCarPark",
            "LIjobs",
            "LIestates",
            "avSt_LI",
            "LIAfront",
            "LIAfrEIA",
            "LIAestate",
            "LIAeBldg",
            "LIFloors",
            "LIAeLoad",
            "LIAeCPark",
            "avLt_LI",
            "LIAeLgrey",
            "LIAeEIA",
            "LIAeTIA",
            "HIjobs",
            "HIestates",
            "avSt_HI",
            "HIAfront",
            "HIAfrEIA",
            "HIAestate",
            "HIAeBldg",
            "HIFloors",
            "HIAeLoad",
            "HIAeCPark",
            "avLt_HI",
            "HIAeLgrey",
            "HIAeEIA",
            "HIAeTIA",
            "COMjobs",
            "COMestates",
            "avSt_COM",
            "COMAfront",
            "COMAfrEIA",
            "COMAestate",
            "COMAeBldg",
            "COMFloors",
            "COMAeLoad",
            "COMAeCPark",
            "avLt_COM",
            "COMAeLgrey",
            "COMAeEIA",
            "COMAeTIA",
            "ORCjobs",
            "ORCestates",
            "avSt_ORC",
            "ORCAfront",
            "ORCAfrEIA",
            "ORCAestate",
            "ORCAeBldg",
            "ORCFloors",
            "ORCAeLoad",
            "ORCAeCPark",
            "avLt_ORC",
            "ORCAeLgrey",
            "ORCAeEIA",
            "ORCAeTIA",
            "Total_jobs",
            "Blk_TIA",
            "Blk_EIA",
            "Blk_EIF",
            "Blk_TIF",
            "Blk_RoofsA",
            "WD_HHIn",
            "WD_HHHot",
            "WD_HHNonPo",
            "HH_GreyW",
            "HH_BlackW",
            "WD_In",
            "WD_Out",
            "WD_Hot",
            "WW_ResG",
            "WW_ResB",
            "WD_COM",
            "WD_HotCOM",
            "WD_Office",
            "WD_HotOff",
            "WD_LI",
            "WD_HotLI",
            "WD_HI",
            "WD_HotHI",
            "WD_NRes",
            "WD_HotNRes",
            "WD_NResIrr",
            "WW_ComG",
            "WW_ComB",
            "WW_IndG",
            "WW_IndB",
            "WD_POSIrr",
            "Blk_WD",
            "Blk_WDIrr",
            "Blk_WDHot",
            "Blk_WW",
            "Blk_WWG",
            "Blk_WWB",
            "Blk_Loss",
            "HasSWW",
            "HasWWTP",
            "HasCarved",
            "Sww_down_i",
            "ModAvgElev",
            "DEM_sink",
            "SwwType",
            "DesgFlow",
            "geometry",
        ]
        blocks2 = import_blocks_to_geopandas(
            self.filename, columns=None, module="xx"
        )  # returns geopandas dataframe with the blocks and Data from UrbanBEATS
        self.assertEqual(
            list(blocks2.columns),
            columns_complete,
            msg="Block columns do not coincide with total amount of features in the original shape file",
        )  # add assertion here

        blocks3 = import_blocks_to_geopandas(
            self.filename, columns=None, module="top"
        )  # returns geopandas dataframe with the blocks and Data from UrbanBEATS
        self.assertEqual(
            len(blocks3), 1056, msg="Different number of blocks"
        )  # add assertion here
        self.assertEqual(
            list(blocks3.columns),
            columns,
            msg="Block columns do not coincide for module topology",
        )  # add assertion here

    def test_get_sww_blocks(self):
        sww_blocks = get_sww_blocks(self.blocks)
        self.assertEqual(
            len(sww_blocks), 240, msg="Different number of blocks"
        )  # add assertion here

    def test_copy_dem(self):

        # Expected outcome
        r_mod_avg_elev = self.blocks["AvgElev"].head(10)

        copy_dem(self.blocks)

        self.assertFalse(
            "ModAvgElev" not in self.blocks.columns,
            msg="ModAvgElev not in the dataframe",
        )

        mod_avg_elev = self.blocks["ModAvgElev"].head(10)
        self.assertTrue(
            np.isclose(mod_avg_elev, r_mod_avg_elev, atol=10 ** (-5)).all(),
            "Copy DEM failed!",
        )

    def test_get_sinks_df(self):

        # Input test blocks
        test_dict = {
            "BlockID": [22, 23, 62, 63, 64, 103, 104, 105],
            "geometry": [
                "POLYGON ((2695895.219529456 1241842.898472791, 2695895.219529456 1242042.898472791, 2696095.219529456 1242042.898472791, 2696095.219529456 1241842.898472791, 2695895.219529456 1241842.898472791))",
                "POLYGON ((2696095.219529456 1241842.898472791, 2696095.219529456 1242042.898472791, 2696295.219529456 1242042.898472791, 2696295.219529456 1241842.898472791, 2696095.219529456 1241842.898472791))",
                "POLYGON ((2695695.219529456 1242042.898472791, 2695695.219529456 1242242.898472791, 2695895.219529456 1242242.898472791, 2695895.219529456 1242042.898472791, 2695695.219529456 1242042.898472791))",
                "POLYGON ((2695895.219529456 1242042.898472791, 2695895.219529456 1242242.898472791, 2696095.219529456 1242242.898472791, 2696095.219529456 1242042.898472791, 2695895.219529456 1242042.898472791))",
                "POLYGON ((2696095.219529456 1242042.898472791, 2696095.219529456 1242242.898472791, 2696295.219529456 1242242.898472791, 2696295.219529456 1242042.898472791, 2696095.219529456 1242042.898472791))",
                "POLYGON ((2695695.219529456 1242242.898472791, 2695695.219529456 1242442.898472791, 2695895.219529456 1242442.898472791, 2695895.219529456 1242242.898472791, 2695695.219529456 1242242.898472791))",
                "POLYGON ((2695895.219529456 1242242.898472791, 2695895.219529456 1242442.898472791, 2696095.219529456 1242442.898472791, 2696095.219529456 1242242.898472791, 2695895.219529456 1242242.898472791))",
                "POLYGON ((2696095.219529456 1242242.898472791, 2696095.219529456 1242442.898472791, 2696295.219529456 1242442.898472791, 2696295.219529456 1242242.898472791, 2696095.219529456 1242242.898472791))",
            ],
            "ModAvgElev": [
                437.0,
                437.3,
                437.71,
                437.98,
                438.36,
                436.35,
                438.52,
                439.58,
            ],
            "DEM_sink": [0, 0, 0, 0, 0, 0, 0, 0],
            "Neighbours": [
                "23, 62, 63, 64",
                "22, 24, 63, 64, 65",
                "23, 63, 103, 104",
                "22, 23, 62, 64, 103, 104, 105",
                "22, 23, 24, 63, 65, 104, 105",
                "62, 63, 104",
                "62, 63, 64, 103, 105",
                "63, 64, 104",
            ],
        }
        test_blocks = gpd.GeoDataFrame.from_dict(data=test_dict)

        print(type(test_blocks))

        # Expected outcome
        r_sinks = [22, 34]

        sinks_df = get_sinks_df(test_blocks)

        self.assertEqual()


if __name__ == "__main__":
    unittest.main()
