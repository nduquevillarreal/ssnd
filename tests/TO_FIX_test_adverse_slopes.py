import networkx as nx
from ssnd.algorithm import *
from ssnd.help_functions.helper_functions import network_to_dataframe, read_network_file
import itertools
from ssnd.hydraulic_parameters import *


# read network file
file_ = "Test_network.csv"
path_ = r"C:\Users\fabrizia\Desktop"

file = os.path.join(path_, file_)

# create graph
graph = pd.read_csv(
    file, header=None, skiprows=[0], names=["From", "To", "type", "flow"]
)
print(graph)
G = nx.from_pandas_edgelist(graph, "From", "To", create_using=nx.DiGraph)

filename = "test_adv_slopes.txt"

path = r"..\test\data"
file = os.path.join(path, filename)
manholes, pipes = network_to_dataframe(read_network_file(file))
elevation_invert, chosen_slopes, pumps, deepened, diameters, design_flow = walk_graph(
    manholes, pipes, verbose=True
)

# prepare graph attributes
elevations = elevation_invert
xy_distance, terrain_slopes = calculate_terrain_slope_xy_distance(manholes, pipes)
distance = xy_distance
flows = design_flow

# create arrays of source nodes, paths, nodes with adverse slopes, nodes with adverse slopes which are not pumps
sources = []
paths = []
adverse_nodes = []
nodes_to_fix = []


# function to set attributes to network nodes
def set_attributes(G, elevations, distance, flows):
    for node in G.nodes():
        nx.set_node_attributes(G, elevations, name="elevation")
        nx.set_node_attributes(G, distance, name="length")
        nx.set_node_attributes(G, flows, name="design flow")
    return


# identify sources (0 in degree)
def identify_sources(G):
    for node in G.nodes():
        degree = G.in_degree(node)
        if degree == 0:
            sources.append(node)
    return G


# find paths from sources to outfall
def find_simple_path(G, sources):
    for source in sources:
        path = list(nx.all_simple_paths(G, source, 8))
        path_ = list(itertools.chain(*path))
        paths.append(path_)
    return paths


# identify nodes with wrong (adverse slopes)
def identify_adverse_nodes(G, paths):
    for path in paths:
        for node in range(len(path) - 1):
            if (
                G.nodes[(path[node])]["elevation"]
                < G.nodes[(path[node + 1])]["elevation"]
            ):
                adverse_nodes.append(path[node])
    return adverse_nodes


# find nodes with adverse slopes which are not pumps--> should be fixed and print them
def nodes_to_fix_(adverse_nodes, pumps):
    for i in range(len(adverse_nodes)):
        node = adverse_nodes[i]
        if node not in pumps.keys():
            # if node not in nodes_to_fix: [Fabrizia]
            nodes_to_fix.append(node)
    print(nodes_to_fix)

    return nodes_to_fix


set_attributes(G, elevations, distance, flows)
identify_sources(G)
find_simple_path(G, sources)
identify_adverse_nodes(G, paths)
nodes_to_fix_(adverse_nodes, pumps)
print("Invert elevations: ", elevations)

identify_adverse_nodes(G, paths)
my_list = nodes_to_fix_(adverse_nodes, pumps)

my_set = set()
for l in my_list:
    my_set.add(l)

print(nodes_to_fix_(adverse_nodes, pumps))

print(len(my_set))
print(my_set)
print(len(pumps))
print(list(nx.attracting_components(G)))
print("Invert elevations: ", elevations)
