import unittest

# import numpy as np
from ssnd.algorithm import *


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        # PIPES INTERNAL ROUGHNESS
        self.roughness = (
            0.00003  # Absolute roughness 'ks' {"pvc": 0.00003, "concrete": 0.00006}
        )
        self.n = 0.009  # Manning's roughness coefficient 'n' {"pvc": 0.009, "concrete": 0.013}

        # COMMERCIAL DIAMETERS
        self.commercial_diameters = [
            float(val)
            for val in (0.225, 0.25, 0.35, 0.4, 0.5, 0.6, 0.80, 1, 1.20, 1.5, 2, 2.5, 3)
        ]

        # MINIMUM AND MAXIMUM VELOCITY
        self.v_min = 0.75
        self.v_max = 10.0

        # KINEMATIC VISCOSITY OF WATER
        self.nu = round(1.14 / pow(10, 6), 8)

    def test_prepare_hydraulics_data(self):

        # Expected result
        r_max_radius = [
            0.0666527,
            0.0740586,
            0.1036821,
            0.1184938,
            0.1481173,
            0.1777407,
            0.2433546,
            0.3041932,
            0.3650319,
            0.4562898,
            0.6065342,
            0.7581678,
            0.9098013,
        ]
        r_max_areas = [
            0.0297285,
            0.0367018,
            0.0719356,
            0.0939567,
            0.1468074,
            0.2114027,
            0.4310875,
            0.6735743,
            0.9699470,
            1.5155423,
            2.8460938,
            4.4470215,
            6.4037110,
        ]
        # min slopes for a velocity of 0.75 m/s
        r_min_slopes = [
            0.0016859,
            0.0014650,
            0.0009354,
            0.0007828,
            0.0005813,
            0.0004559,
            0.0002998,
            0.0002227,
            0.0001746,
            0.0001297,
            0.0000887,
            0.0000659,
            0.0000516,
        ]
        # max slopes for a velocity of 10.0 m/s
        r_max_slopes = [
            0.2997279,
            0.2604457,
            0.1662952,
            0.1391737,
            0.1033579,
            0.0810529,
            0.0533128,
            0.0395930,
            0.0310487,
            0.0230584,
            0.0157765,
            0.0117165,
            0.0091880,
        ]

        min_slopes, max_slopes, max_radius, max_areas = prepare_hydraulics_data(
            self.commercial_diameters, self.v_min, self.v_max, self.n
        )

        self.assertTrue(
            np.isclose(min_slopes, r_min_slopes, atol=10 ** (-5)).all(),
            "calculation of minimum slopes with full pipes failed!\n{}".format(
                min_slopes
            ),
        )
        self.assertTrue(
            np.isclose(max_slopes, r_max_slopes, atol=10 ** (-5)).all(),
            "calculation of maximum slopes with full pipes failed!\n{}".format(
                max_slopes
            ),
        )
        self.assertTrue(
            np.isclose(max_radius, r_max_radius, atol=10 ** (-5)).all(),
            "calculation of maximum hydraulic radius with full pipes failed!\n{}".format(
                max_radius
            ),
        )
        self.assertTrue(
            np.isclose(max_areas, r_max_areas, atol=10 ** (-5)).all(),
            "calculation of maximum wet areas with full pipes failed!\n{}".format(
                max_areas
            ),
        )


if __name__ == "__main__":
    unittest.main()
