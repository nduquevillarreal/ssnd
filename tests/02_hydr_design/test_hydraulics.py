import unittest

# import numpy as np
import pandas as pd
from ssnd.hydraulic_parameters import *


class TestHydraulics(unittest.TestCase):
    def setUp(self) -> None:

        # PIPES INTERNAL ROUGHNESS
        self.roughness = (
            1.5e-6  # Absolute roughness 'ks' {"pvc": 0.0000015, "concrete": 0.003}
        )
        self.n = 0.009  # Manning's roughness coefficient 'n' {"pvc": 0.009, "concrete": 0.013}

        # MINIMUM AND MAXIMUM VELOCITY
        self.v_min = 0.75
        self.v_max = 10.0

        # KINEMATIC VISCOSITY OF WATER
        self.nu = 1.14e-6

        # Network information
        # @Topography: mild slopes
        self.manholes = pd.DataFrame.from_dict(
            [
                {"id": 1, "x": 1.0, "y": 13.0, "z": 12.0, "outfall": 0},
                {"id": 2, "x": 2.0, "y": 3.0, "z": 11.5, "outfall": 0},
                {"id": 3, "x": 8.0, "y": 9.0, "z": 11.5, "outfall": 0},
                {"id": 4, "x": 15.0, "y": 11.0, "z": 10.5, "outfall": 0},
                {"id": 5, "x": 14.0, "y": 1.0, "z": 10.0, "outfall": 1},
            ]
        )
        self.pipes = pd.DataFrame.from_dict(
            [
                {"id_up": 1, "id_down": 3, "type": 1, "design_flow": 0.2},
                {"id_up": 2, "id_down": 3, "type": 1, "design_flow": 0.1},
                {"id_up": 3, "id_down": 5, "type": 2, "design_flow": 0.6},
                {"id_up": 4, "id_down": 5, "type": 1, "design_flow": 0.4},
            ]
        )

    def test_maximum_filling_ratio(self):
        """test_maximum_filling_ratio from small to larger diameters"""
        # Input
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]

        # Expected result
        r_filling_ratios = [0.7, 0.7, 0.8, 0.8, 0.85]

        for i, d in enumerate(d_list):
            filling = get_maximum_filling_ratio(d)

            self.assertTrue(
                np.isclose(filling, r_filling_ratios[i], atol=10 ** (-5)).all(),
                f"calculation of the maximum filling ratio failed for {d} m diameter!\n"
                f"Expected: {r_filling_ratios[i]} -> got{filling}",
            )

    def test_max_flow_depth(self):
        # Input
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]

        # Expected result
        r_max_water_levels = [0.245, 0.42, 0.488, 1.2, 1.36]

        for i, d in enumerate(d_list):
            max_water_level = get_max_flow_depth(d)

            self.assertTrue(
                np.isclose(
                    max_water_level, r_max_water_levels[i], atol=10 ** (-5)
                ).all(),
                f"calculation of the maximum flow depth failed for {d} m diameter!\n"
                f"Expected: {r_max_water_levels[i]} -> got {max_water_level}",
            )

    def test_angle(self):
        # Input
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        water_levels = [0.245, 0.42, 0.488, 1.2, 1.36]

        # Expected result
        r_angles = [3.964626346, 3.964626346, 4.428594871, 4.428594871, 4.692387647]

        for i, d in enumerate(d_list):
            angle = get_angle(d, water_levels[i])

            self.assertTrue(
                np.isclose(angle, r_angles[i], atol=10 ** (-5)).all(),
                f"calculation of the angle failed for {d} m diameter!\n"
                f"Expected: {r_angles[i]} -> got {angle}",
            )

    def test_wetted_perimeter(self):
        # Input
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        angles = [3.964626346, 3.964626346, 4.428594871, 4.428594871, 4.692387647]

        # Expected result
        r_perimeters = [0.693809611, 1.189387904, 1.350721436, 3.321446153, 3.753910117]

        for i, d in enumerate(d_list):
            perimeter = get_wetted_perimeter(d, angles[i])

            self.assertTrue(
                np.isclose(perimeter, r_perimeters[i], atol=10 ** (-5)).all(),
                f"calculation of the perimeter failed for {d} m diameter!\n"
                f"Expected: {r_perimeters[i]} -> got{perimeter}",
            )

    def test_top_width(self):
        # Input
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        angles = [3.964626346, 3.964626346, 4.428594871, 4.428594871, 4.692387647]

        # Expected result
        r_top_widths = [0.320780299, 0.549909083, 0.488, 1.2, 1.142628549]

        for i, d in enumerate(d_list):
            top_width = get_top_width(d, angles[i])

            self.assertTrue(
                np.isclose(top_width, r_top_widths[i], atol=10 ** (-5)).all(),
                f"calculation of the top width failed for {d} m diameter!\n"
                f"Expected: {r_top_widths[i]} -> got {top_width}",
            )

    def test_hydraulic_radius(self):
        # Input
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        angles = [3.964626346, 3.964626346, 4.428594871, 4.428594871, 4.692387647]

        # Expected result
        r_radius = [0.10368212, 0.177740777, 0.18555789, 0.456289892, 0.485227399]

        for i, d in enumerate(d_list):
            radius = get_hydraulic_radius(d, angles[i])

            self.assertTrue(
                np.isclose(radius, r_radius[i], atol=10 ** (-5)).all(),
                f"calculation of the radius failed for {d} m diameter!\n"
                f"Expected: {r_radius[i]} -> got {radius}",
            )

    def test_area(self):
        # Input
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        angles = [3.964626346, 3.964626346, 4.428594871, 4.428594871, 4.692387647]

        # Expected result
        r_areas = [0.071935651, 0.211402731, 0.250637019, 1.515542308, 1.821500041]

        for i, d in enumerate(d_list):
            area = get_area(d, angles[i])

            self.assertTrue(
                np.isclose(area, r_areas[i], atol=10 ** (-5)).all(),
                f"calculation of the area failed for {d} m diameter!\n"
                f"Expected: {r_areas[i]} -> got{area}",
            )

    def test_velocity(self):
        # Input
        n = 0.009
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        radius = [0.10368212, 0.177740777, 0.18555789, 0.456289892, 0.485227399]
        slopes = [0.166263579, 0.081051628, 0.076527776, 0.023056144, 0.021243891]

        # Expected result
        r_velocities = [9.999047522, 9.999917412, 9.999692646, 9.999494824, 10.00010888]

        for i, s in enumerate(slopes):
            vel = get_velocity(slopes[i], radius[i], n)

            self.assertTrue(
                np.isclose(vel, r_velocities[i], atol=10 ** (-5)).all(),
                f"calculation of the velocity failed for {d_list[i]} m diameter and slope {s}!\n"
                f"Expected: {r_velocities[i]} -> got{vel}",
            )

    def test_manning_slope(self):
        # Input
        n = 0.009
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        radius = [0.10368212, 0.177740777, 0.18555789, 0.456289892, 0.485227399]
        velocities = [9.999047522, 9.999917412, 9.999692646, 9.999494824, 10.00010888]

        # Expected result
        r_slopes = [0.166263579, 0.081051628, 0.076527776, 0.023056144, 0.021243891]

        for i, v in enumerate(velocities):
            slope = get_slope(radius[i], velocities[i], n)

            self.assertTrue(
                np.isclose(slope, r_slopes[i], atol=10 ** (-5)).all(),
                f"calculation of manning slope with full pipe failed for {d_list[i]} m diameter and velocity {v}!\n"
                f"Expected: {r_slopes[i]} -> got{slope}",
            )

    def test_tau(self):
        # Input
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        radius = [0.10368212, 0.177740777, 0.18555789, 0.456289892, 0.485227399]
        slopes = [0.166263579, 0.081051628, 0.076527776, 0.023056144, 0.021243891]

        # Expected result
        r_taus = [
            169.1102772,
            141.3246204,
            139.305262,
            103.204,
            101.1226351,
        ]  # [N.m-2]  Tau = density [kg.m-3] *gravity [m.s-2]*R[m]*S

        for i, d in enumerate(d_list):
            tau = get_tau(slopes[i], radius[i])

            self.assertTrue(
                np.isclose(tau, r_taus[i], atol=10 ** (-5)).all(),
                f"calculation of the shear stress failed for {d} m diameter!\n"
                f"Expected: {r_taus[i]} -> got {tau}",
            )

    def test_froude(self):
        # Input
        gravity = 9.81
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        velocities = [9.999047523, 9.999917412, 9.999692646, 9.999494824, 10.00010888]
        areas = [0.071935651, 0.211402731, 0.250637019, 1.515542308, 1.821500041]
        top_widths = [0.320780299, 0.549909083, 0.488, 1.2, 1.142628549]

        # Expected result
        r_fr_numbers = [6.741489807, 5.149345829, 4.454915293, 2.840862062, 2.528763227]

        for i, d in enumerate(d_list):
            fr = get_froude(velocities[i], areas[i], top_widths[i], gravity)

            self.assertTrue(
                np.isclose(fr, r_fr_numbers[i], atol=10 ** (-5)).all(),
                f"calculation of the Froude number failed for {d} m diameter!\n"
                f"Expected: {r_fr_numbers[i]} -> got {fr}",
            )

    def test_flow(self):
        # Input
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        velocities = [9.999047523, 9.999917412, 9.999692646, 9.999494824, 10.00010888]
        areas = [0.071935651, 0.211402731, 0.250637019, 1.515542308, 1.821500041]

        # Expected result
        r_flows = [0.719287997, 2.114009846, 2.506293155, 15.15465746, 18.21519872]

        for i, d in enumerate(d_list):
            flow = get_flow(areas[i], velocities[i])

            self.assertTrue(
                np.isclose(flow, r_flows[i], atol=10 ** (-5)).all(),
                f"calculation of the flow failed for {d} m diameter!\n"
                f"Expected: {r_flows[i]} -> got {flow}",
            )

    def test_normal_depth(self):
        # Input
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        slopes = [0.166263579, 0.081051628, 0.076527776, 0.023056144, 0.021243891]
        design_flows = [0.225896996, 0.663918313, 0.864060099, 5.224662089, 6.649541316]

        # Expected result
        r_yn = [0.1225, 0.21, 0.244, 0.6, 0.68]

        for i, d in enumerate(d_list):
            yn, flow = get_normal_depth(d_list[i], design_flows[i], slopes[i], self.n)

        self.assertTrue(
            np.isclose(yn, r_yn[i], atol=10 ** (-3)).all(),
            f"calculation of the normal depth failed for {d} m diameter!\n"
            f"Expected: {r_yn[i]} -> got {yn}",
        )

    def test_check_constraints(self):
        # Input
        roughness = 1.5e-6  # PVC absolute roughness coefficient
        d_list = [0.35, 0.6, 0.61, 1.5, 1.6]
        filling_ratios = [0.7, 0.7, 0.8, 0.8, 0.85]
        max_water_levels = [0.245, 0.42, 0.488, 1.2, 1.36]
        velocities = [9.999047522, 9.999917412, 9.999692646, 9.999494824, 10.00010888]
        taus = [169.1102772, 141.3246204, 139.305262, 103.204, 101.1226351]
        fr_numbers = [6.741489807, 5.149345829, 4.454915293, 2.840862062, 2.528763227]

        # Expected result
        min_velocities = [0.75, 0.8, 0.8, 0.8, 0.8]
        max_velocity = 10.0
        min_tau = 2.0
        subcritical_froude = 0.7
        supercritical_froude = 1.5

        # True
        for i, d in enumerate(d_list):
            boolean = check_constraints(
                d_list[i],
                velocities[i],
                taus[i],
                max_water_levels[i],
                fr_numbers[i],
                roughness,
            )

            selected_message = ""
            if velocities[i] < min_velocities[i]:
                selected_message = f"Expected higher than min velocity: {min_velocities[i]} -> got {velocities[i]}\n"
            elif velocities[i] > max_velocity:
                selected_message = f"Expected lower than max velocity: {max_velocity} -> got {velocities[i]}\n"
            elif taus[i] < min_tau:
                selected_message = f"Expected higher than min shear stress: {min_tau} -> got {taus[i]}\n"
            elif filling_ratios[i] > 0.8 and fr_numbers[i] > subcritical_froude:
                selected_message = f"Expected Froude number between {subcritical_froude} and {supercritical_froude} -> got {fr_numbers[i]}\n"
            elif filling_ratios[i] > 0.8 and fr_numbers[i] > max_velocity:
                selected_message = f"Expected Froude number between {subcritical_froude} and {supercritical_froude} -> got {fr_numbers[i]}\n"

            self.assertTrue(
                boolean,
                f"check_constraints failed for {d} m diameter!\n "
                f"{selected_message}",
            )


if __name__ == "__main__":
    unittest.main()
