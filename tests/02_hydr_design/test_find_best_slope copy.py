import numpy as np
import pandas as pd
from ssnd.algorithm import *


def test_find_best_slope():
    self.pipes = pd.DataFrame.from_dict(
        [
            {"id_up": 0, "id_down": 2, "type": 1, "design_flow": 0.01},
            {"id_up": 1, "id_down": 3, "type": 1, "design_flow": 0.0009},
            {"id_up": 2, "id_down": 4, "type": 2, "design_flow": 0.02},
            {"id_up": 3, "id_down": 4, "type": 2, "design_flow": 0.0109},
            {"id_up": 4, "id_down": 5, "type": 2, "design_flow": 0.0409},
        ]
    )

    df_hydraulics = pd.DataFrame.from_dict({
        "diameter": [0.225, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 1.0, 1.2, 2.5, 3.0],
        "min_slopes": [
            0.001687,
            0.001974,
            0.00115,
            0.000784,
            0.000582,
            0.00045700000000000005,
            0.000301,
            0.000224,
            0.000176,
            6.7e-05,
            5.2999999999999994e-05,
        ],
        "max_slopes": [
            0.299727,
            0.35069500000000003,
            0.20424,
            0.139173,
            0.103357,
            0.081052,
            0.053312,
            0.039592,
            0.031048,
            0.011716,
            0.009187,
        ],
        "max_hydraulic_radius": [
            0.06665279150116553,
            0.0592469257788138,
            0.0888703886682207,
            0.1184938515576276,
            0.1481173144470345,
            0.1777407773364414,
            0.24335460921242485,
            0.30419326151553105,
            0.36503191381863725,
            0.7581678101919247,
            0.9098013722303095,
        ],
        "max_areas": [
            0.029728508985182346,
            0.02348919228458852,
            0.052850682640324165,
            0.09395676913835407,
            0.14680745177867824,
            0.21140273056129666,
            0.4310875896941091,
            0.6735743588970453,
            0.9699470768117452,
            4.4470215834432025,
            6.403711080158211,
        ],
    })

    mh_up = 0

    # Input data of the limits for slopes and hydraulic radius given the pipes diameter
    max_radius = [
        0.0666527,
        0.0740586,
        0.1036821,
        0.1184938,
        0.1481173,
        0.1777407,
        0.2433546,
        0.3041932,
        0.3650319,
        0.4562898,
        0.6065342,
        0.7581678,
        0.9098013,
    ]

    max_areas = [
        0.0297285,
        0.0367018,
        0.0719356,
        0.0939567,
        0.1468074,
        0.2114027,
        0.4310875,
        0.6735743,
        0.9699470,
        1.5155423,
        2.8460938,
        4.4470215,
        6.4037110,
    ]

    min_slopes = [
        0.0016859,
        0.0014650,
        0.0009354,
        0.0007828,
        0.0005813,
        0.0004559,
        0.0002998,
        0.0002227,
        0.0001746,
        0.0001297,
        0.0000887,
        0.0000659,
        0.0000516,
    ]

    max_slopes = [
        0.2997279,
        0.2604457,
        0.1662952,
        0.1391737,
        0.1033579,
        0.0810529,
        0.0533128,
        0.0395930,
        0.0310487,
        0.0230584,
        0.0157765,
        0.0117165,
        0.0091880,
    ]

    # set parameters
    ideal_slope = self.terrain_slopes[1]  # terrain slope for outer pipe (1,2)
    design_discharge = 0.01
    diameter_idx = 0

    # Expected result
    r_slope = 0.07741736729460467
    r_idx = 1

    slope, idx = find_best_diameter_slope(
        pipes=self.pipes,
        df_hydraulics=df_hydraulics,
        id_up=mh_up,
        design_discharge=design_discharge,
        ideal_slope=ideal_slope,
        incoming_diameter_idx=diameter_idx,
        v_min=self.v_min,
        v_max=self.v_max,
        n=self.n,
        roughness=self.roughness,
        min_discharge=self.min_discharge,
        min_slope_limit=0.001,
        max_slope_limit=0.1,
        diameters=self.commercial_diameters,
        verbose=False,
        count=0,
    )

    assert np.isclose(
        slope, r_slope, atol=10 ** (-5)
    ).all(), f"calculation of the best slope using the {idx} diameter failed!\n{slope}"

def test_calculate_terrain_slope_xy_distance():

    # Expected result
    r_lengths = {0: 100, 1: 100, 2: 100, 3: 100, 4: 100}
    r_slopes = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0}

    lengths, slopes = calculate_terrain_slope_xy_distance(self.manholes, self.pipes)
    print(lengths)
    print(slopes)
    for id_up, length in lengths.items():
        assert np.isclose(
                length, r_lengths[id_up], atol=10 ** (-5)
        ).all(), "calculation of pipe lengths failed!\n{}".format(length)

    for id_up, slope in slopes.items():
        assert np.isclose(
                slope, r_slopes[id_up], atol=10 ** (-5)
        ).all(), "calculation of the terrain slopes failed!\n{}".format(slope)
        

