# In[]:
"""
@file
@author Natalia Duque, Christian Foerster
@section LICENSE

Sequenced Sewer Networks Design (SSND)

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# In[]:
from ssnd.defaults import commercial_diameters
from ssnd.help_functions.helper_functions import *
from ssnd.hydraulic_parameters import *


# In[]:
def prepare_hydraulics_data(diameters=commercial_diameters, v_min=0.75, v_max=10.0, n=0.009, verbose=False):
    """Prepare arrays with the minimum and maximum slope based on the minimum and maximum velocities
    assuming a maximum filling ratio for the each diameter.

    :param diameters: list of commercial diameters in m
    :param v_min: minimum velocity
    :param v_max: maximum velocity
    :param n: Manning's n coefficient
    :param verbose: printing function
    :return: df_df_hydraulics data frame with hydraulic parameters

    """

    # Prepare arrays for the hydraulic calculations
    max_hydraulic_radius = []
    max_areas = []
    min_slopes = []
    max_slopes = []

    # calculating the max filling ratio and the min slopes
    for idx, diameter in enumerate(diameters):
        verbose_(idx, verbose)
        # Hydraulic parameters for the maximum filling ratio of the pipe with a specific diameter
        y = get_max_flow_depth(diameter)
        angle = get_angle(diameter, y)
        max_hydraulic_radius.append(get_hydraulic_radius(diameter, angle))
        max_areas.append(get_area(diameter, angle))
        verbose_("max_hydraulic_radius: {}".format(max_hydraulic_radius), verbose)
        verbose_("max_areas: {}".format(max_areas), verbose)

        # Calculate minimum and maximum slope for the given diameter

        min_slopes.append(get_slope(max_hydraulic_radius[-1], v_min, n))
        max_slopes.append(get_slope(max_hydraulic_radius[-1], v_max, n))
        verbose_("min_slopes: {}".format(min_slopes), verbose)
        verbose_("max_slopes: {}".format(max_slopes), verbose)

    dict = {
        "diameter": diameters,
        "min_slopes": min_slopes,
        "max_slopes": max_slopes,
        "max_hydraulic_radius": max_hydraulic_radius,
        "max_areas": max_areas,
    }

    # columns= {"min_slopes", "max_slopes", "max_hydraulic_radius", "max_areas"}
    df_hydraulics = pd.DataFrame.from_dict(dict)
    # list(df_hydraulics["max_hydraulic_radius"])
    verbose_("Hydraulic parameters for each  pipe diameter under max filling ratio", verbose)
    verbose_("df_hydraulics \n".format(df_hydraulics), verbose)

    return df_hydraulics

# In[]:
def find_best_diameter_slope(
    *,
    pipes,
    df_hydraulics,
    id_up,
    design_discharge,
    ideal_slope,
    incoming_diameter_idx=0,
    v_min=0.75,
    v_max=10.0,
    n=0.009,  # for PVC [Source: Butler and Davies(2011), Urban Drainage]
    roughness=0.00003,  # for PVC [Source: Butler and Davies(2011), Urban Drainage]
    min_discharge=0.001,
    min_slope_limit=0.001,
    max_slope_limit=0.1,
    delta_slope=0.0001,
    diameters=commercial_diameters,
    verbose=False,
    count=0,
):
    """Find a diameter-slope feasible combination for each pipe

    :param pipes: dataframe with pipes information [id_up, id_down, type, design-flow]
    :param id_up: ID of the pipe, given by the ID of its upstream manhole
    :param design_discharge: design flow of the pipe to be designed

    :param v_min: minimum flow velocity limit default: 0.75
    :param v_max: maximum flow velocity limit default: 10.0
    :param min_slope_limit: minimum slope limit default: 0.001
    :param max_slope_limit: maximum slope limit default: 0.10
    :param delta_slope: slope change default:0.0001
    :param diameters: list of commercial diameters
                      default:[0.225, 0.25, 0.35, 0.4, 0.5, 0.6, 0.80, 1.0, 1.20, 1.5, 2.0, 2.5, 3.0]
    :param verbose: Boolean, print statements if True

    :return slope: design slope for the pipe
    :return idx: index for the selected pipe diameter
    """

    # for pipes with almost no discharge
    if design_discharge < min_discharge:
        # design_discharge = min_discharge
        verbose_(f"{count} :: min discharge pipe {id_up}", verbose=verbose)

        ## In this cases the hydaulic restrictions will not be fullfilled under any diameter-slope combination
        ## the smallest diameter or incoming diameter and the minimum slope for that diameter will be chosen
        ## the pipes will be oversized, however a pipe is required where there is people

        diameter = diameters[incoming_diameter_idx]
        min_slope = df_hydraulics.min_slopes[incoming_diameter_idx]

        # calculate the real flow depth
        normal_depth, exact_flow = get_normal_depth(diameter, design_discharge, min_slope, n)

        # recalculate geometric parameters
        angle = get_angle(diameter, normal_depth)
        h_radius = get_hydraulic_radius(diameter, angle)
        area = get_area(diameter, angle)

        # recalculate hydraulic parameters
        velocity = get_velocity(min_slope, h_radius, n)
        tau = get_tau(min_slope, h_radius)
        top_width = get_top_width(diameter, angle)
        froude = get_froude(area, velocity, top_width)

        pipes.loc[id_up, "diameter"] = diameter
        pipes.loc[id_up, "slope"] = min_slope
        pipes.loc[id_up, "filling_ratio"] = normal_depth / diameter
        pipes.loc[id_up, "normal_depth"] = normal_depth
        pipes.loc[id_up, "angle"] = angle
        pipes.loc[id_up, "h_radius"] = h_radius
        pipes.loc[id_up, "area"] = area
        pipes.loc[id_up, "velocity"] = velocity
        pipes.loc[id_up, "froude"] = froude
        pipes.loc[id_up, "tau"] = tau
        pipes.loc[id_up, "exact_flow"] = exact_flow
        return min_slope, incoming_diameter_idx

    # loop over diameters starting from the diameter of the previous manhole that is given as input.
    for idx in range(incoming_diameter_idx, len(diameters)):
        diameter: float = diameters[idx]

        max_hyd_rad = df_hydraulics.max_hydraulic_radius[idx]
        max_area = df_hydraulics.max_areas[idx]
        min_slope = df_hydraulics.min_slopes[idx]
        max_slope = df_hydraulics.max_slopes[idx]

        # set start slope values at the minimum slope for the following iterations
        if min_slope <= ideal_slope <= max_slope:
            start_slope = ideal_slope
        else:
            start_slope = min_slope

        velocity = v_min

        # max_slope is the minimum slope between the slope limit defined in defaults (e.g. 10%)
        # and the max_slope for the pipe diameter
        print(max_slope_limit, max_slope)
        max_s = min(max_slope_limit, max_slope)

        for slope in np.arange(start_slope, max_s, delta_slope):

            if velocity > v_max:
                break

            # make sure discharge fits through the pipe
            max_discharge = max_area * get_velocity(slope, max_hyd_rad, n)

            if design_discharge < max_discharge:

                # calculate the real flow depth
                normal_depth, exact_flow = get_normal_depth(diameter, design_discharge, slope, n)

                # recalculate geometric parameters
                angle = get_angle(diameter, normal_depth)
                h_radius = get_hydraulic_radius(diameter, angle)
                area = get_area(diameter, angle)

                # recalculate hydraulic parameters
                velocity = get_velocity(slope, h_radius, n)
                tau = get_tau(slope, h_radius)
                top_width = get_top_width(diameter, angle)
                froude = get_froude(area, velocity, top_width)

                # check hydraulic constraints
                checked = check_constraints(diameter, velocity, tau, normal_depth, froude, roughness)

                if checked:
                    verbose_(f" FOUND IT {id_up} {diameter} {slope}", verbose=verbose)
                    # Add values for the hydraulic design to data frame
                    pipes.loc[id_up, "diameter"] = diameter
                    pipes.loc[id_up, "slope"] = slope
                    pipes.loc[id_up, "filling_ratio"] = normal_depth / diameter
                    pipes.loc[id_up, "normal_depth"] = normal_depth
                    pipes.loc[id_up, "angle"] = angle
                    pipes.loc[id_up, "h_radius"] = h_radius
                    pipes.loc[id_up, "area"] = area
                    pipes.loc[id_up, "velocity"] = velocity
                    pipes.loc[id_up, "froude"] = froude
                    pipes.loc[id_up, "tau"] = tau
                    pipes.loc[id_up, "exact_flow"] = exact_flow
                    return slope, idx

# In[]:
def walk_graph(
    *,
    manholes,
    pipes,
    material="pvc",
    min_depth=1.0,
    max_depth=10.0,
    type_external=1,
    v_min=0.75,
    v_max=10.0,
    min_slope_limit=0.001,
    max_slope_limit=0.10,
    delta_slope=0.0001,
    diameters=commercial_diameters,
    verbose=False,
):
    """Main design algorithm that 'walks' the graph from the extremes to the outlet.


    :param manholes: dataframe with manholes information [id, x, y, z, outlet]
    :param pipes: dataframe with pipes information [id_up, id_down, type, design_flow]
    :param material: pipe material default:"pvc"
    :param min_depth: minimum excavation depth :default:1.0
    :param max_depth: maximum excavation depth :default:1.0
    :param type_external: numerical value to describe outer-manholes default:1
    :param v_min: minimum flow velocity limit default: 0.75
    :param v_max: maximum flow velocity limit default: 10.0
    :param min_slope_limit: minimum slope limit default: 0.001
    :param max_slope_limit: maximum slope limit default: 0.10
    :param delta_slope: slope change default:0.0001
    :param diameters: list of commercial diameters
                      default:[0.225, 0.25, 0.35, 0.4, 0.5, 0.6, 0.80, 1.0, 1.20, 1.5, 2.0, 2.5, 3.0]
    :param verbose: Boolean, print statements if True
    :return: pipes: dataframe with the design of the pipes in the network
            df_hydraulics: dataframe with the hydraulic parameters of each designed pipe
    """

    # ------------------
    # --- INPUT DATA ---
    # ------------------
    verbose_("\n----------------------------", verbose=True)
    verbose_("INPUT DATA", verbose=True)
    verbose_("pipes \n {}".format(pipes), verbose=True)
    verbose_("manholes \n {}".format(manholes), verbose=True)

    # dict -> design discharge for each pipe
    design_discharge = dict(zip(pipes.id_up, pipes.design_flow))

    # dict -> map nodes for fast access {edge_id_up: [id_up, id_down]}
    edges = dict([(pipes.id_up[k], [pipes.id_up[k], pipes.id_down[k]]) for k in pipes.index])
    verbose_("edges: {}".format(edges), verbose)

    # list -> of outlet manholes
    outlet_manholes = manholes.id[manholes.outlet == 1].values
    verbose_("Outlet: {}".format(outlet_manholes), verbose)

    # list -> of external manholes (i.e. upstream-most manholes)
    external_manholes = pipes.id_up[pipes.type == type_external].values  # List
    verbose_("external mh: {}".format(external_manholes), verbose)

    # dict -> map repeated downstream IDs and its frequency (No. of incoming pipes) per manhole
    internal_mh, count_mh_in = np.unique(pipes.id_down.values, return_counts=True)
    count_pipes_in = dict(zip(internal_mh, count_mh_in))
    verbose_("internal_mh {} ".format(internal_mh), verbose)
    verbose_("count_pipes_in {}".format(count_pipes_in), verbose)

    # dict -> get elevation dictionary
    terrain_elevations = dict(zip(manholes.id, manholes.z))
    verbose_("terrain_elevation: {}".format(terrain_elevations), verbose)

    # get terrain slopes and xy - distances
    xy_distances, terrain_slopes = calculate_terrain_slope_xy_distance(manholes, pipes)
    verbose_("xy_distance: {}".format(xy_distances), verbose)

    # create a dictionary of nodes and their incoming manholes
    upstream_manholes = {}
    for edge in edges.values():
        upstream_manholes.setdefault(edge[1], []).append(edge[0])  # {downstream_id: [incoming upstream_ids]}
    verbose_("upstream_manholes: {}".format(upstream_manholes), verbose)
    verbose_("----------------------------\n", verbose=True)
    # ---

    # --------------------------------------------------------
    # --- PREPARE EMPTY DICTIONARIES TO STORE CALCULATIONS ---
    # --------------------------------------------------------
    pipes = add_empty_columns_df(pipes)

    verbose_("\n----------------------------", verbose=True)
    verbose_("PREPARING EMPTY DICTIONARIES", verbose=True)
    # prepare dict to store exact design flow {id: flow} [Float64]
    design_flows = dict([(k, 0.0) for k in pipes.id_up])
    # verbose_("design_flows: {}".format(design_flows), verbose)

    # dict with pump info {key: mh, value: (previous bottom elevation, new bottom elevation)}
    pumps = dict([(k, 0.0) for k in pipes.id_up])
    verbose_("pumps: {}".format(pumps), verbose)

    # dict with deepened manholes because of steep terrain {mh: (min_depth, new_depth)}
    drops = dict([(k, 0.0) for k in pipes.id_up])
    verbose_("drops: {}".format(drops), verbose)

    # dict to keep track of the chosen slopes. { manhole_up: slope}
    chosen_slopes = dict([(k, 0.0) for k in pipes.id_up])
    verbose_("chosen_slopes: {}".format(chosen_slopes), verbose)

    # prepare count dictionary of total incoming pipes to a manhole {id: pipes_in}[Int],
    # to compare with 'count_pipes_in'
    pipes_in = dict([(k, 0) for k in internal_mh])
    verbose_("pipes_in: {}".format(pipes_in), verbose)

    # prepare diameter ID tracker
    diameters_idx_tracker = dict([(k, 0) for k in edges.keys()])
    verbose_("diameters_idx_tracker: {}".format(diameters_idx_tracker), verbose)

    # prepare dict to store chosen diameters in m {id: diameter} [Float64]
    diameters_tracker = dict([(k, 0.0) for k in edges.keys()])
    pipe_lengths = dict([(k, 0.0) for k in pipes.id_up])
    verbose_("diameter_tracker: {}".format(diameters_tracker), verbose)

    # upstream and downstream invert elevations of each pipe (Pipe ID : mh_up)
    up_elevations = dict([(k, 0.0) for k in pipes.id_up])  # {Up_ID : up_elevation}
    up_depths = dict([(k, 0.0) for k in pipes.id_up])  # {Up_ID : up_depth}
    verbose_("'-> up_elevations: {}".format(up_elevations), verbose)

    from_which_up_mh = dict([(i, []) for i in internal_mh])  # {Up_ID : [up_mh]}
    incoming_down_elevations = dict(
        [(i, {}) for i in internal_mh]
    )  # {Up_ID : [from_which_up, Down_elevations]} # TODO change structure
    down_elevations = dict([(k, 0.0) for k in pipes.id_up])  # {Up_ID : down_depth}

    incoming_down_depths = dict([(i, {}) for i in internal_mh])  # {Up_ID : [Down_elevations]}
    down_depths = dict([(k, 0.0) for k in pipes.id_up])  # {Up_ID : down_depth}
    verbose_("'-> down_elevations: {}".format(incoming_down_elevations), verbose)

    verbose_("---------------------------- \n", verbose=True)
    # ---

    # -----------------------
    # --- START ALGORITHM ---
    # -----------------------
    verbose_("\n----------------------------", verbose=True)
    verbose_("START ALGORITHM", verbose=True)
    # add break criteria for reaching last mh:
    for mh_out in outlet_manholes:
        pipes_in[mh_out] = -1
        verbose_("pipes_in: {}".format(pipes_in), verbose)

        # assign biggest diameter to the last manhole (this does not assign the diameter to the last pipe)
        # needed for breaking criteria when more than one pipe is coming into the outlet manhole
        diameters_idx_tracker[mh_out] = len(diameters) - 1  # last diameter (idx starts in zero)
        verbose_("diameter_tracker: {}".format(diameters_idx_tracker), verbose)

    # calculate static hydraulic parameters for each pipe diameter
    roughness, n = get_roughness(material=material)
    df_hydraulics = prepare_hydraulics_data(diameters, v_min, v_max, n, verbose=verbose)
    verbose_("df_hydraulics \n", verbose=verbose)
    print_full_df(df_hydraulics)

    __chosen_diameter = 0.0  # temporal variable

    # --------------------------------------------------
    # --- Start each iteration from an outer manhole ---
    # --------------------------------------------------
    __iter = 1
    count_nn = 0
    for external_mh in external_manholes:
        verbose_("\n----------------------------", verbose=verbose)
        verbose_("ITERATION {}/{}".format(__iter, len(external_manholes)), verbose=verbose)

        mh_down = edges[external_mh][1]  # get the downstream manhole
        pipes_in[mh_down] += 1  # counts the number of pipes coming into a downstream manhole

        verbose_("\n** mh_up {}, mh_down {} **".format(external_mh, mh_down), verbose)

        # set ideal slope to the highest between the terrain slope and the min_slope_limit
        # slope can not be zero
        ideal_slope = max(terrain_slopes[external_mh], min_slope_limit)
        if ideal_slope < min_slope_limit:  # flat or negative slope
            verbose_(
                f"\n !! ERROR !! pipe {external_mh}: ideal_slope {ideal_slope} < min_slope_limit   \n",
                verbose=True,
            )
            ideal_slope = min_slope_limit

        # design_flows[external_mh] = design_discharge[external_mh]

        verbose_(
            "# Finding best diameter-slope for {} -> {}".format(external_mh, mh_down),
            verbose,
        )
        count_nn += 1  # TODO Temporal for debugging.. delete
        slope, dia_idx = find_best_diameter_slope(
            pipes=pipes,
            df_hydraulics=df_hydraulics,
            id_up=external_mh,
            design_discharge=design_discharge[external_mh],
            ideal_slope=ideal_slope,
            incoming_diameter_idx=diameters_idx_tracker[external_mh],
            v_min=v_min,
            v_max=v_max,
            n=n,
            min_slope_limit=min_slope_limit,
            max_slope_limit=max_slope_limit,
            delta_slope=delta_slope,
            roughness=roughness,
            diameters=diameters,
            verbose=False,
            count=count_nn,
        )

        # check for min slope limit
        if type(slope) == "list":
            verbose_(f"slope is a list --> {slope}", verbose=verbose)
        if slope < min_slope_limit:
            verbose_(f"\n!! WARNING MIN_SLOPE !! slope {slope}, diameter {dia_idx}\n", verbose=True)

        else:

            delta_h = slope * xy_distances[external_mh]  # upstream vs downstream elevation difference in m

            pipe_lengths[external_mh] = np.sqrt(np.square(delta_h) + np.square(xy_distances[external_mh]))

            # save data in dictionaries
            chosen_slopes[external_mh] = slope  # chosen slope for the pipe with id mh_up
            diameters_idx_tracker[
                external_mh
            ] = dia_idx  # index of the chosen diameter from the list of commercial diameters
            __chosen_diameter = diameters[dia_idx]  # diameter in m
            diameters_tracker[external_mh] = __chosen_diameter

            verbose_(
                "  diameter {} , slope {}, delta_h {}".format(__chosen_diameter, slope, delta_h),
                verbose,
            )

            # Save invert elevations. For outer pipes start at the minimum excavation elevation (include pipe diameter)
            if external_mh in up_elevations.keys():
                if up_elevations[external_mh]:  # if there are values in the list
                    verbose_(
                        "  # There are already up_elevations[{}]: {}".format(external_mh, up_elevations[external_mh]),
                        verbose,
                    )
                up_elevations[external_mh] = terrain_elevations[external_mh] - min_depth - __chosen_diameter
                up_depths[external_mh] = terrain_elevations[external_mh] - up_elevations[external_mh]

            if mh_down in incoming_down_elevations.keys():
                from_which_up_mh[mh_down].append(external_mh)
                incoming_down_elevations[mh_down][external_mh] = up_elevations[external_mh] - delta_h

                # TODO use the dataframe
                incoming_down_depths[mh_down][external_mh] = (
                    terrain_elevations[external_mh] - incoming_down_elevations[mh_down][external_mh]
                )

                down_elevations[external_mh] = incoming_down_elevations[mh_down][external_mh]
                down_depths[external_mh] = incoming_down_depths[mh_down][external_mh]

            verbose_(
                "  Invert elev up {}, down {}".format(
                    up_elevations[external_mh], incoming_down_elevations[mh_down][external_mh]
                ),
                verbose,
            )

            verbose_("diameters {}".format(diameters_tracker), verbose)

            # set at least same diameter for mh_down as mh_up
            if diameters_idx_tracker[external_mh] > diameters_idx_tracker[mh_down]:
                __min_idx_diameter = diameters_idx_tracker[external_mh]  # incoming diameter
                diameters_idx_tracker[mh_down] = __min_idx_diameter
                diameters_tracker[mh_down] = diameters[__min_idx_diameter]

                verbose_("  set at least same diameter for mh_down as mh_up", verbose)
                verbose_("  diameters {}".format(diameters_tracker), verbose)

            # --------- CREATE DROP ? -----------------

            verbose_("elevation {} -> {}".format(external_mh, mh_down), verbose)
            verbose_("  upstream elevations {}".format(up_elevations), verbose)
            verbose_("  downstream elevations {}".format(incoming_down_elevations), verbose)

        # keep moving along the path until you reach a node that has multiple inflowing pipes
        verbose_(
            "\n## Keep moving along the path until you reach a node that has multiple inflowing pipes",
            verbose,
        )
        verbose_(f"ITERATION {__iter} MANHOLE_UP {mh_down}", verbose)
        while pipes_in[mh_down] == count_pipes_in[mh_down]:

            previous_down = mh_down  # save temporarily

            # update mh_IDs for new pipe
            mh_up = mh_down  # new mh_up is the downstream mh from the upstream pipe
            mh_down = edges[mh_up][1]  # retrieve downstream manhole of the edge
            pipes_in[mh_down] += 1  # count incoming pipe
            verbose_(f"\n** mh_up {mh_up}, mh_down {mh_down} **", verbose)

            # Save invert elevations. For inner pipes consider upstream elevation for the previous pipe
            if mh_up not in up_elevations.keys():
                verbose_(
                    f"\n!! WARNING !! mh {mh_up} not in upstream elevations\n",
                    verbose,
                )
                verbose_(f"upstream manholes {upstream_manholes}", verbose)

            # set new elevation where the lowest mh_up ends
            verbose_(
                f"  >> set new elevation where the lowest mh_up ends:\n"
                "   >> to_evaluate: {up_elevations[mh_up]} , previous: {down_elevations[previous_down][-1]}",
                verbose,
            )

            if up_elevations[mh_up]:
                # If there is already an up_elevation saved, do not update the elevation
                # means that a pump was included from the previous pipe
                verbose_(" PUMP at mh {} : {} ".format(mh_up, pumps[mh_up]), verbose)
            else:
                # set new elevation at the lowest mh_up ends
                up_elevations[mh_up] = min(incoming_down_elevations[previous_down].values())

                up_depths[mh_up] = terrain_elevations[mh_up] - up_elevations[mh_up]

            # -------- from above
            # find the diameter and slope of the pipe
            verbose_(
                "# Finding best diameter-slope for {} -> {}".format(mh_up, mh_down),
                verbose=verbose,
            )
            count_nn += 1
            slope, dia_idx = find_best_diameter_slope(
                pipes=pipes,
                df_hydraulics=df_hydraulics,
                id_up=mh_up,
                design_discharge=design_discharge[mh_up],
                ideal_slope=ideal_slope,
                incoming_diameter_idx=diameters_idx_tracker[mh_up],
                v_min=v_min,
                v_max=v_max,
                n=n,
                min_slope_limit=min_slope_limit,
                max_slope_limit=max_slope_limit,
                delta_slope=delta_slope,
                roughness=roughness,
                diameters=diameters,
                verbose=False,
                count=count_nn,
            )
            # if design dis is close to zero!
            if slope < min_slope_limit:
                verbose_(
                    "\n!! WARNING MIN_SLOPE !! slope {}, diameter {}\n".format(slope, dia_idx),
                    verbose,
                )

            delta_h = slope * xy_distances[mh_up]  # upstream vs downstream elevation difference in m
            # verbose_("\nmh_up {}, mh_down {}".format(mh_up, mh_down), verbose)

            pipe_lengths[mh_up] = np.sqrt(np.square(delta_h) + np.square(xy_distances[mh_up]))

            verbose_(
                "  diameter {} , slope {}, delta_h {}".format(diameters[dia_idx], slope, delta_h),
                verbose,
            )

            if mh_down not in incoming_down_elevations.keys():
                verbose_(
                    "\n!! WARNING !! mh {} not in downstream elevations\n".format(mh_down),
                    verbose,
                )
            from_which_up_mh[mh_down].append(mh_up)
            # dict key [current_id][coming from mh_up]
            incoming_down_elevations[mh_down][mh_up] = up_elevations[mh_up] - delta_h
            incoming_down_depths[mh_down][mh_up] = (
                terrain_elevations[mh_down] - incoming_down_elevations[mh_down][mh_up]
            )

            down_elevations[mh_up] = incoming_down_elevations[mh_down][mh_up]
            down_depths[mh_up] = incoming_down_depths[mh_down][mh_up]

            verbose_(
                "  invert elev up {}, down {}".format(up_elevations[mh_up], incoming_down_elevations[mh_down][mh_up]),
                verbose,
            )

            # save data in dictionaries
            chosen_slopes[mh_up] = slope  # chosen slope for the pipe with id mh_up
            diameters_idx_tracker[mh_up] = dia_idx  # index of the chosen diameter from the list of commercial diameters
            __chosen_diameter = diameters[dia_idx]  # diameter in m for calculations
            diameters_tracker[mh_up] = __chosen_diameter  # save in diameter_tracker dictionary
            # verbose_("  diameters {}".format(diameters_tracker), verbose)

            verbose_("diameters {}".format(diameters_tracker), verbose)
            # set at least same diameter for mh_down as mh_up
            if mh_down not in outlet_manholes:
                if diameters_idx_tracker[mh_up] > diameters_idx_tracker[mh_down]:
                    __min_idx_diameter = diameters_idx_tracker[mh_up]  # incoming diameter
                    diameters_idx_tracker[mh_down] = __min_idx_diameter
                    diameters_tracker[mh_down] = diameters[__min_idx_diameter]

                    verbose_("  set at least same diameter for mh_down as mh_up", verbose)
                    verbose_("  diameters {}".format(diameters_tracker), verbose)

            # CHECK EXCAVATION LIMITS
            """ DROPS """
            # Check at the UPSTREAM manhole
            # if the pipe slope is too shallow:
            # not reaching the minimal excavation depth of the upstream manhole
            if up_elevations[mh_up] > terrain_elevations[mh_up] - min_depth - __chosen_diameter:
                verbose_(
                    "NEEDS A DROP: Invert elevation does not meet min excavation limit",
                    verbose,
                )
                verbose_("  Drop at mh_up {} : {}".format(mh_up, ""), verbose)

                min_exc_elev = terrain_elevations[mh_up] - min_depth - __chosen_diameter
                verbose_(
                    "up_elevations[mh_up] > min_exc_elevation ".format(up_elevations[mh_up], min_exc_elev),
                    verbose,
                )

            # Check at the DOWNSTREAM manhole
            # if the pipe slope is too shallow:
            # not reaching the minimal excavation depth of the downstream manhole
            # down_invert < terrain - (min_exc + diameter)
            if incoming_down_elevations[mh_down][mh_up] > terrain_elevations[mh_down] - (min_depth + __chosen_diameter):
                verbose_(
                    "NEEDS A DROP: Invert elevation does not meet min excavation limit",
                    verbose,
                )
                desired_down_elev = terrain_elevations[mh_down] - (min_depth + __chosen_diameter)
                drop = incoming_down_elevations[mh_down][mh_up] - desired_down_elev
                verbose_("  Drop at mh_down {} : {}".format(mh_down, drop), verbose)

                incoming_down_elevations[mh_down][mh_up] -= drop
                incoming_down_depths[mh_down][mh_up] -= drop  # update the dictionary

                down_elevations[mh_up] = incoming_down_elevations[mh_down][mh_up]
                down_depths[mh_up] = incoming_down_depths[mh_down][mh_up]

                # update upstream elevation
                up_elevations[mh_up] -= drop
                up_depths[mh_up] -= drop  # update the dictionary
                drops[mh_up] = drop  # {mh_up: (drop, old_elev, new_elev)}

            """ PUMPS """
            # Check at the DOWNSTREAM manhole
            # When reaching the max excavation limit at the downstream manhole, no more space for downstream pipes
            min_down = min(incoming_down_elevations[mh_down].values())
            if min_down < terrain_elevations[mh_down] - max_depth:
                verbose_(
                    "NEEDS A PUMP: Invert elevation reached max excavation limit",
                    verbose,
                )
                # if the downstream elevation reaches exactly the max elevation limit
                if max_depth == min_down:
                    # Do not elevate this pipe, but set the upstream elevation for
                    # the UPSTREAM elevation (at mh_DOWN) at the minimum excavation limit
                    verbose_(
                        " pipe fits, but new up_elev at mh_down {} is set to the min_exc_elev".format(mh_down),
                        verbose,
                    )
                    min_exc_down = terrain_elevations[mh_down] - min_depth - __chosen_diameter
                    pumping_h = min_exc_down - min_down
                    up_elevations[
                        mh_down
                    ] = min_exc_down  # set new_up_elev for downstream pipe (i.e. at mh_down) at the minimum excavation
                    pumps[mh_down] = pumping_h

                # If the downstream elevation is deeper than the max elevation limit
                else:
                    # Elevate this pipe
                    # the UPSTREAM elevation pipe at the minimum excavation limit
                    verbose_(
                        " pipe does not fit, Elevate mh_up {} to the min_exc_elev".format(mh_up),
                        verbose,
                    )
                    min_exc_up = terrain_elevations[mh_up] - min_depth - __chosen_diameter
                    pumping_h = min_exc_up - up_elevations[mh_up]
                    # elevate the pipe at both ends, update upstream and downstream elevations
                    up_elevations[mh_up] = min_exc_up
                    incoming_down_elevations[mh_down][mh_up] += pumping_h  # (update last entry of the list)
                    incoming_down_depths[mh_down][mh_up] += pumping_h  # update the dictionary

                    down_elevations[mh_up] = incoming_down_elevations[mh_down][mh_up]
                    down_depths[mh_up] = incoming_down_depths[mh_down][mh_up]

                    pumps[mh_up] = pumping_h
                    verbose_(" PUMP at mh {} : {}".format(mh_up, pumps[mh_up]), verbose)

            # Check at the UPSTREAM manhole
            # if upstream elevation is deeper than the maximum excavation limit
            if up_elevations[mh_up] < terrain_elevations[mh_up] - max_depth:
                verbose_("!! WARNING !!: this should not happen", verbose)
                verbose_(
                    "  NEEDS A PUMP: Invert elevation reached max excavation limit",
                    verbose,
                )

            verbose_("elevation {} -> {}".format(mh_up, mh_down), verbose)
            verbose_("  upstream elevations {}".format(up_elevations), verbose)
            verbose_("  downstream elevations {}".format(incoming_down_elevations), verbose)

            # -------------------

            # TODO check if it is redundant
            down_elevations[mh_up] = incoming_down_elevations[mh_down][mh_up]
            down_depths[mh_up] = incoming_down_depths[mh_down][mh_up]

        # Iteration finished from an outer manhole, start from another outer manhole in the next iteration
        __iter += 1

    # convert diameter indexes to actual diameters
    diameters_tracker, chosen_slopes = retrieve_diameters(
        diameters_idx_tracker, chosen_slopes, diameters_tracker, diameters
    )

    # update dataframe. add columns with the new pipes design
    # Index with the IDs of mh_up
    pipes["diameter"] = pipes["id_up"].map(diameters_tracker)
    pipes["length"] = pipes["id_up"].map(pipe_lengths)
    pipes["slope"] = pipes["id_up"].map(chosen_slopes)
    pipes["tElev_up"] = pipes["id_up"].map(terrain_elevations)
    pipes["tElev_down"] = pipes["id_down"].map(terrain_elevations)
    pipes["terrain_slope"] = pipes["id_up"].map(terrain_slopes)
    pipes["up_elevation"] = pipes["id_up"].map(up_elevations)
    pipes["down_elevation"] = pipes["id_up"].map(down_elevations)
    pipes["up_depth"] = pipes["id_up"].map(up_depths)
    pipes["down_depth"] = pipes["id_up"].map(down_depths)
    pipes["avg_depth"] = pipes["id_up"].map(down_depths)
    pipes["pump"] = pipes["id_up"].map(pumps)
    pipes["drop"] = pipes["id_up"].map(drops)

    # Index with the IDs of mh_down
    pipes["from_up_mh"] = pipes["id_down"].map(from_which_up_mh)

    return pipes.copy()

# %%
