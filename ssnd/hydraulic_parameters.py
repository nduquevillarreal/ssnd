"""
@file
@author Natalia Duque, Christian Foerster
@section LICENSE

Sequenced Sewer Networks Design (SSND)

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import numpy as np

from ssnd.help_functions.helper_functions import verbose_

""" CALCULATE HYDRAULIC PARAMETERS WITH NUMPY ARRAYS"""


def calculate_terrain_slope_xy_distance(manholes, pipes):
    """
    Calculate terrain slope and horizontal distances over the pipe network
    :param manholes
    :param pipes
    :return: dictionary with horizontal distances for each pipe, dictionary with terrain slope along each pipe
    """

    "prepare x, y, z coordinates per manhole"
    manhole_coordinates = {}
    for _, manhole in manholes.iterrows():
        manhole_coordinates[manhole.id] = manhole.values[1:4]

    "lists of x, y, z coordinates for the upstream and downstream manholes of each pipe"
    # xyz_up = [[up_coordinates pipe 1], [up_coordinates pipe 2], ... ]
    xyz_up = np.zeros((pipes.shape[0], 3), dtype=float)  # initialize array in zeros

    # xyz_down = [[down_coordinates pipe 1], [down_coordinates pipe 2], ... ]
    xyz_down = np.zeros((pipes.shape[0], 3), dtype=float)  # initialize array in zeros

    idx_coorList = 0  # index to fill the xyy lists for up and down ids.
    for i, pipe in pipes.iterrows():
        # loop over the rows (pipes) and save the coordinates of the specific upstream and downstream manholes
        xyz_up[idx_coorList, :] = manhole_coordinates[pipe.id_up]
        xyz_down[idx_coorList, :] = manhole_coordinates[pipe.id_down]
        idx_coorList += 1

    "calculate horizontal and vertical distances"
    # horizontal_dist = sqrt((x_up - x_down)2 + (y_up - y_down)2)
    # operation between lists of lists can be done index by index
    #   [[  up_x1,   up_y1,  up_z1],   [  up_x2,  up_y2,  up_z2], ... ]
    # - [[down_x1, down_y1,down_z1],   [down_x2,down_y2,down_z2], ... ]

    # [:, :-1] all rows, all columns except the last
    horizontal_dist = np.sqrt(np.sum(np.square(xyz_up[:, :-1] - xyz_down[:, :-1]), axis=1))

    # [:, -1]  all rows, only the last column
    height = xyz_up[:, -1] - xyz_down[:, -1]

    terrain_slope = height / horizontal_dist

    "add info to dataframe"
    pipes["length"] = horizontal_dist
    pipes["terrain_slope"] = terrain_slope

    return dict(zip(pipes.id_up.values, horizontal_dist)), dict(zip(pipes.id_up.values, terrain_slope))


"""HYDRAULIC EQUATIONS FOR SINGLE PIPES"""


def get_roughness(material="pvc"):
    """Returns the Absolute roughness (Ks) and Manning's Coefficient (n) in newly-laid foul or combined sewers
        Ks is given in [m] and n [s/m^1/3]
        [Source: Butler and Davies(2011), Urban Drainage]
        Table 8.2 Typical values of roughness (ks Pipe material)

    :param material: String defining pipe material eg. pvc, concrete, clay, fibre-cement, brick
    :return: Absolute roughness (ks) and Manning's Coefficient (n)
    """
    material = material.lower()

    if material == "pvc":
        roughness = 1.5E-6  # Absolute roughness
        n = 0.009  # Manning's roughness coefficient
    elif material == "polymer":
        roughness = 3E-5  # Absolute roughness
        n = 0.009  # Manning's roughness coefficient
    elif material == "clay":
        roughness = 3E-5  # Absolute roughness
        n = 0.013  # Manning's roughness coefficient
    elif material == "concrete":
        roughness = 6E-5  # Absolute roughness
        n = 0.013  # Manning's roughness coefficient
    elif material == "fibre-cement":
        roughness = 1.5E-5  # Absolute roughness
        n = 0.013  # Manning's roughness coefficient
    elif material == "brick":
        roughness = 6E-4  # Absolute roughness
        n = 0.013  # Manning's roughness coefficient
    else:
        roughness = 1.5E-3  # Where existing pipe conditions are unknown,ks = 1.5 mm for foul sewers
        n = 0.013
        verbose_(
            f"The material was not specified. The Absolute roughness and Manning's roughness coefficient were set to Ks={roughness} and n={n}",
            verbose=True,
        )
    return roughness, n


def get_maximum_filling_ratio(diameter):
    """Calculate maximum filling ratio based on pipe's diameter
    Source: Butler and Davies (2011)
    :param diameter: [m]
    :return max_filling_ratio [-]
    """
    # Maximum filling ratio in general
    return 0.8
    if diameter <= 0.6:
        return 0.7
    elif 0.6 < diameter <= 1.5:
        return 0.8
    elif diameter > 1.5:
        return 0.85


def get_max_flow_depth(diameter):
    """Calculate maximum flow depth based on pipe's diameter and it corresponding maximum filling ratio
    :param diameter: pipe diameter [m]
    :return: maximum flow depth [m]
    """
    return get_maximum_filling_ratio(diameter) * diameter


def get_angle(diameter, y):
    """Calculate flow angle subtended by water surface at centre of pipe
    Source: Butler and Davies (2011)
    :param diameter: pipe diameter [m]
    :param y: flow depth [m]
    :return: angle subtended by water surface at centre of pipe [rad]
    """
    angle = 2 * np.arccos(1 - 2 * y / diameter)
    return angle


def get_wetted_perimeter(diameter, angle):
    """Calculate wetted perimeter for circular pipes
    Source: Butler and Davies (2011)
    :param diameter: pipe diameter [m]
    :param angle: angle subtended by water surface at centre of pipe [rad]
    :return: cross-sectional wetted perimeter [m]
    """
    return angle * diameter / 2


def get_top_width(diameter, angle):
    """Calculate top width [m]
    Source: Butler and Davies (2011)
    @:param diameter: pipe diameter in [m]
    @:param angle: angle subtended by water surface at centre of pipe [rad]
    :return Top-Width at the surface of the water in the pipe [m]
    """
    return diameter * np.sin(angle / 2)


def get_hydraulic_radius(diameter, angle):
    """Calculate hydraulic radius in circular pipes
    Source: Butler and Davies (2011)
    @:param diameter: pipe diameter in [m]
    @:param angle: angle subtended by water surface at centre of pipe [rad]
    :return: hydraulic radius [m]
    """
    return (diameter / 4) * (1 - np.sin(angle) / angle)


def get_area(diameter, angle):
    """Calculate cross-sectional wetted area for circular pipes
    Source: Butler and Davies (2011)
    :param diameter: pipe diameter [m]
    :param angle: angle subtended by water surface at centre of pipe [rad]
    :return: cross-sectional wetted area [m2]
    """
    return (np.power(diameter, 2) / 8) * (angle - np.sin(angle))


def get_tau(slope: float, radius: float, gravity: float = 9.81, density: float = 1000.0) -> float:
    """Calculate shear stress on the walls of the pipe
    :param slope: pipe slope [-]
    :param radius: hydraulic radius [m]
    :param gravity: default: Earth's gravity of 9.81 m.s-2
    :param density: default: water_density of 1000 kg.m-3
    :return: shear stress at the wall of the pipes [N.m-2]
    """
    return gravity * density * radius * slope


def get_velocity(slope: float, radius: float, n: float) -> float:
    """Calculate flow velocity with Manning-Strickler's equation
    Source: Butler and Davies (2011)
    :param slope: pipe slope [-]
    :param radius: hydraulic radius [m]
    :param n: Gauckler–Manning roughness coefficient [s.m-1/3]
    :return: flow velocity [m.s-1]
    """
    # Manning-Strickler equation
    return 1 / n * np.power(radius, 2.0 / 3) * np.sqrt(slope)


def get_slope(radius, velocity, n):
    """Calculate pipe slope given a flow velocity, by using Manning-Strickler's equation

    :param radius: hydraulic radius [m]
    :param velocity: flow velocity [m.s-1]
    :param n: Gauckler–Manning roughness coefficient [s.m-1/3]
    :return: pipe slope [-]
    """
    return np.square(velocity * n / np.power(radius, 2.0 / 3))


def get_froude(velocity:float, area:float, top_width:float, gravity: float = 9.81):
    """Calculate Froude number
    Source: Butler and Davies (2011)
    :param area: wetted area for circular pipes [m]
    :param velocity: flow velocity [m/s]
    :param top_width: Top-Width at the surface of the water in the pipe [m]
    :param gravity: gavitational acceleration [m.s-2] default: Earth's gravity of 9.81 m.s-2
    :return: Froude number [-]
    """

    return velocity / np.sqrt((gravity * area / top_width))


def get_flow(area:float, velocity:float):
    """Calculate flow
    Source: Butler and Davies (2011)
    :param area: hydraulic area [m2]
    :param velocity: flow velocity [m/s]
    :return flow [m3/s]
    """
    return area * velocity


def get_normal_depth(diameter, design_q, slope, n):
    """Calculate normal depth

    :param diameter: pipe diameter [m]
    :param design_q: design flow [m3/s]
    :param slope: [-]
    :param n: kinematic viscosity of water [m2/s]
    :return: Normal flow depth [m]
    """
    yni = 0
    ynf = get_max_flow_depth(diameter)
    yn = (ynf + yni) / 2

    while abs(yni - ynf) > 0.0001:

        angle = get_angle(diameter, yn)
        area = get_area(diameter, angle)
        radius = get_hydraulic_radius(diameter, angle)
        velocity = get_velocity(slope, radius, n)
        flow = get_flow(area, velocity)

        if flow > design_q:
            ynf = yn
        else:
            yni = yn

        yn = (ynf + yni) / 2

    return yn, flow


def check_constraints(diameter, velocity, tau, yn, froude, roughness):
    """Check hydraulic constraints fo max and min flow velocity, minimum shear stress

    :param diameter: pipe diameter[m]
    :param velocity:flow velocity [m/s]
    :param tau: shear stress [N/m2]
    :param yn: normal flow depth [m]
    :param froude: Froude's number [-]
    :param roughness: absolute pipe's roughness Ks [m]
    :return: Boolean: True is all constraints are met
    """
    check = True

    # Check minimum velocity
    if diameter <= 0.5 and round(velocity, 2) <= 0.75:
        return False
    if diameter > 0.5 and round(velocity, 2) <= 0.8:
        return False

    # Check maximum velocity
    if roughness >= 0.0001 and round(velocity, 2) > 5:
        return False

    if roughness < 0.0001 and round(velocity, 2) > 10:
        return False

    # Check minimum shear stress
    if diameter >= 0.45 and round(tau, 2) <= 2:
        return False

    # Check filling ratio when shear stress <= 2
    if yn / diameter <= 0.1 and round(tau, 2) <= 2:
        return False

    #     Check Froude's number and filling rate for quasi-critical flow (0.7 < froude > 1.5)
    #         if 0.7 < froude and froude < 1.5 and yn / diameter > 0.8:
    #             check = False

    return check
