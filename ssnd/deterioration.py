"""
@file
@author Natalia Duque
@section LICENSE

Sequenced Sewer Networks Design (SSND)

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from pathlib import Path

import os
import numpy as np
import pandas as pd

from scipy.optimize import curve_fit

import matplotlib.pyplot as plt
from ssnd.help_functions.helper_functions import cm2inch, verbose_

# ----------------------------------------------------------------
# ----------- PROBABILITY DISTRIBUTION FUNCTIONS -----------------
# ----------------------------------------------------------------
def gompertz_model(variables_x, shape_param_a, scale_param_b):
    """[Wikipedia] The Gompertz curve or Gompertz function
        is a type of mathematical model for a time series, named after Benjamin Gompertz (1779–1865).
        It is a sigmoid function which describes growth as being slowest at the start and end of a given time period.
    :param variables_x: array of pipe ages (variable)
    :param shape_param_a: shape parameter
    :param scale_param_b: shape parameter
    :return: cumulative probability of failure
    """

    return np.where(variables_x > 0, np.exp(-np.exp(shape_param_a - scale_param_b * variables_x)), 0)


def weibull_density_function(variables_x, shape_param_a, scale_param_b):
    """Weibull - Failure Probability density distribution.
    [Source: Wikipedia]

    :param variables_x: array of pipe ages (variable)
    :param shape_param_a: shape parameter (sets the displacement along the x-axis (translates the graph to the left or right)
    :param scale_param_b: scale parameter (sets the growth rate)
    :return:
    """
    k = (1 / shape_param_a) ** scale_param_b
    return np.where(
        variables_x > 0,
        k * scale_param_b * (variables_x ** (scale_param_b - 1)) * (np.exp(-k * (variables_x**scale_param_b))),
        0,
    )


def weibull_cummulative_function(variables_x, shape_param_a, scale_param_b):
    """Weibull - cumulative probability distribution.
    [Source: Wikipedia]

    :param variables_x: array of pipe ages (variable)
    :param shape_param_a: shape parameter (sets the displacement along the x-axis (translates the graph to the left or right)
    :param scale_param_b: scale parameter (sets the growth rate)
    :return:
    """
    # For x ≥ 0, and F(x; k; λ) = 0 for x < 0.

    return np.where(variables_x > 0, 1 - np.exp(-((variables_x / shape_param_a) ** scale_param_b)), 0)


def weibull_survival_density_function(variables_x, shape_param_a, scale_param_b):
    """Weibull - Survival Probability density distribution.
    survival = 1 - cumulative_failure
    [Source: Wikipedia]

    :param variables_x: array of pipe ages (variable)
    :param shape_param_a: shape parameter (sets the displacement along the x-axis (translates the graph to the left or right)
    :param scale_param_b: scale parameter (sets the growth rate)
    :return:
    """
    k = (1 / shape_param_a) ** scale_param_b
    return np.where(
        variables_x > 0,
        1 - (k * scale_param_b * (variables_x ** (scale_param_b - 1)) * (np.exp(-k * (variables_x**scale_param_b)))),
        1,
    )


def weibull_survival_function(variables_x, shape_param_a, scale_param_b):
    """Weibull - Survival curve.
    survival = 1 - cumulative_failure
    [Source: Wikipedia]

    :param variables_x: array of pipe ages (variable)
    :param shape_param_a: shape parameter (sets the displacement along the x-axis (translates the graph to the left or right)
    :param scale_param_b: scale parameter (sets the growth rate)

    :param x:
    :param a:
    :param b:
    :return:
    """
    return np.where(variables_x > 0, np.exp(-((variables_x / shape_param_a) ** scale_param_b)), 1.0)


# ----------------------------------------------
# ----------- DETERIORATION MODEL -------------
# ----------------------------------------------
def run_deterioration_model(
    data_ages=None, data_failures=None, data_sim_times=None, distribution="Weibull", verbose=False
):
    """Plot deterioration model and get parameters [a, b] and covariance of the fitted curve

    :param data_ages:
    :param data_failures:
    :param data_sim_times: simulation time [years]
    :param distribution:
    :param verbose:
    :return: param_optimal, param_covariance, failure_fit
    """

    plt.subplots(figsize=(cm2inch((9, 9))), dpi=300)  # convert centimeters in inches (input *tuple)
    text_kwargs = dict(ha="center", va="center", fontsize=10, color="k")

    plt.plot(data_ages, data_failures, marker="s", linestyle="None", c="k", label="Reference data")

    """
    Fit distribution to Data (scipy.optimize.curve_fit () )
    Use non-linear least squares to fit a function, f, to data.
        :return popt: Optimal values for the parameters so that the sum of the squared residuals of f(xdata, *popt)-ydata is minimized.
        :return pcov: The estimated covariance of popt.
    """
    if distribution.lower().startswith("w"):
        plt.title("Weibull distribution")
        param_optimal, param_covariance = curve_fit(
            f=weibull_cummulative_function, xdata=data_ages, ydata=data_failures
        )
        verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)
        a = param_optimal[0]  # first parameter from the fitting process
        b = param_optimal[1]  # second parameter from the fitting process
        failure_curve = [weibull_cummulative_function(t, a, b) for t in data_sim_times]
        plt.plot(
            data_sim_times, failure_curve, c="b", linestyle="-", label=f"Fitted curve \n[a:{round(a)}, b:{round(b,2)}]"
        )
        failure_fit = dict(
            [(i, weibull_cummulative_function(i, param_optimal[0], param_optimal[1])) for i in data_ages]
        )
        verbose_(f"Cumulative failure: {failure_fit}", verbose=verbose)

    elif distribution.lower().startswith("g"):
        plt.title("Gompertz distribution")
        param_optimal, param_covariance = curve_fit(f=gompertz_model, xdata=data_ages, ydata=data_failures)
        verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)
        a = param_optimal[0]  # first parameter from the fitting process
        b = param_optimal[1]  # second parameter from the fitting process
        failure_curve = [gompertz_model(t, a, b) for t in data_sim_times]
        plt.plot(
            data_sim_times, failure_curve, c="m", linestyle="-", label=f"Fitted curve \n[a:{round(a)}, b:{round(b,2)}]"
        )
        failure_fit = dict([(i, gompertz_model(i, param_optimal[0], param_optimal[1])) for i in data_ages])
        verbose_(f"Cumulative failure: {failure_fit}", verbose=verbose)

    else:
        plt.title("Deterioration_60-60Curve-60Curve models")
        param_optimal, param_covariance = curve_fit(
            f=weibull_cummulative_function, xdata=data_ages, ydata=data_failures
        )
        verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)
        a = param_optimal[0]  # first parameter from the fitting process
        b = param_optimal[1]  # second parameter from the fitting process
        failure_curve = [weibull_cummulative_function(t, a, b) for t in data_sim_times]
        plt.plot(
            data_sim_times,
            failure_curve,
            c="b",
            linestyle="-",
            label=f"Fitted Weibull curve \n[a:{round(a)}, b:{round(b,2)}]",
        )
        failure_fit = dict(
            [(i, weibull_cummulative_function(i, param_optimal[0], param_optimal[1])) for i in data_ages]
        )
        verbose_(f"Cumulative failure: {failure_fit}", verbose=verbose)

        param_optimal, param_covariance = curve_fit(f=gompertz_model, xdata=data_ages, ydata=data_failures)
        verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)
        a = param_optimal[0]  # first parameter from the fitting process
        b = param_optimal[1]  # second parameter from the fitting process
        failure_curve = [gompertz_model(t, a, b) for t in data_sim_times]
        plt.plot(
            data_sim_times,
            failure_curve,
            c="m",
            linestyle="-",
            label=f"Fitted Gompertz curve \n[a:{round(a)}, b:{round(b,2)}]",
        )
        failure_fit = dict([(i, gompertz_model(i, param_optimal[0], param_optimal[1])) for i in data_ages])
        verbose_(f"Cumulative failure: {failure_fit}", verbose=verbose)

        pass

    # plt.axvline(initial_age, color='r', linestyle='--', label='Initial age')

    plt.xlabel("Age [years]", **text_kwargs)
    plt.ylabel("Cumulative probability of failure [%]")
    plt.legend(loc="lower right")
    plt.minorticks_on()
    plt.grid(True, which="minor", alpha=0.2)
    plt.grid(True, which="major", alpha=0.5)
    plt.tight_layout()
    # plt.show()
    plt.savefig(f"../Data/figures/P2_{distribution}-distribution.png")
    plt.close()
    return param_optimal, param_covariance, failure_fit


# -------------------------------------------------------
# -------- RUN DETERIORATION PIPE-BY-PIPE ---------------
# -------------------------------------------------------
def pipes_rehabilitation(
    pipes, initial_pipes_age, year_i, year_0, year_step, distribution="Weibull", a=None, b=None, verbose=False
):

    # np.random.seed(77)  # Use for debugging
    if f"age_{year_i}" not in pipes.columns:
        # Save initial pipe age for the initial year
        # add 1 to simulate replacement at the END of the year
        pipes[f"age_{year_i}"] = initial_pipes_age + 1

    year = year_i  # starting year for the rehabilitation strategy (starts before the base year year_0)

    while year <= year_0:

        # Initialize columns
        if f"prob_failure_{year}" not in pipes.columns:
            pipes[f"prob_failure_{year}"] = 0.0
            pipes[f"failed_{year}"] = 0

        for id_up, pipe in pipes.iterrows():
            # Pre-processing rehabilitation strategy before the simulation time
            cond_failure = cond_prob_failure(
                pipe,  # pipe object
                year,
                year_step,
                distribution,
                a,
                b,
            )

            pipes.loc[id_up, f"prob_failure_{year}"] = cond_failure
            # randomly select pipes that will fail in current year using the conditional probability of failure of the single pipe as a weight for the function
            # n = number of trials, p = probability of each trial
            binary_failed = np.random.binomial(n=1, p=cond_failure, size=None)
            pipes.loc[id_up, f"failed_{year}"] = binary_failed

            next_age = pipe[f"age_{year}"] + year_step

            if f"age_{year + year_step}" not in pipes.columns:
                pipes[f"age_{year + year_step}"] = 1  # initialize : Age ad the End of the period
                pipes.loc[id_up, f"age_{year + year_step}"] = next_age  # save new value
            else:
                pipes.loc[id_up, f"age_{year + year_step}"] = next_age  # save new value

            if binary_failed:
                pipes.loc[id_up, "years_failed"] = year
                pipes.loc[id_up, f"age_{year+year_step}"] = 1  # End of the year

        year += year_step

    return pipes


#
# def set_pipes_age(pipes, pipe_age=30):
#     pipes = add_empty_columns_df(pipes, columns=["age"])
#     ages = dict([(k, 0) for k in pipes.id_up])
#
#     for id_up, pipe in pipes.iterrows():
#         ages[id_up] = random.randint(0, 150)
#
#     pipes["age"] = pipes["id_up"].map(ages)
#     return True


def network_deterioration(
    pipes=None,
    initial_pipes_age=0,
    distribution="Weibull",
    a=None,
    b=None,
    year_i=None,
    year_0=None,
    year_f=None,
    year_step=1,
    verbose=False,
):

    """Preprocess sewer deterioration X years before the base year for the simulation"""
    pipes = pipes_rehabilitation(
        pipes=pipes,
        initial_pipes_age=initial_pipes_age,
        year_i=year_i,  # initial year for preprocessing sewer deterioration
        year_0=year_0 - 1,  # base year
        year_step=year_step,
        distribution=distribution,
        a=a,
        b=b,
        verbose=verbose,
    )

    # TODO include new pipes from the designs every 5 years (Input hydraulic designs)
    # assume all the new pipes are built at the same time
    """Run over simulation time, starting from base year"""
    pipes = pipes_rehabilitation(
        pipes=pipes,
        initial_pipes_age=initial_pipes_age,
        year_i=year_0,  # base year
        year_0=year_f,  # end of simulation time
        year_step=year_step,
        distribution=distribution,
        a=a,
        b=b,
        verbose=verbose,
    )
    # with pd.option_context('display.max_rows', 10):
    #     pd.set_option("display.max_columns", pipes.shape[1])  # None or 1000
    #     # print(pipes)

    # frequencies_per_simulation[montecarlo_iter] = failures_per_age.sort()
    # print(f"iter: {montecarlo_iter}  Frequencies:{frequencies_per_simulation[montecarlo_iter]}")

    return pipes


def cond_prob_failure(
    pipe,
    year=None,
    yearly_step=None,
    distribution="Weibull",
    a=None,
    b=None,
):
    """Calculate the conditional probability of failure for each pipe
    given the current age of the pipe and the time step for the simulation
    of the deterioration model.

    :param pipe: Dataframe of the pipe in the network
    :param yearly_step: Time step for the simulation of the deterioration model [years] default: 5 years
    :param a:
    :param b:
    :param data_ages:
    :param data_failures:
    :param data_sim_times:
    :param distribution: Deterioration_60-60Curve-60Curve model : 'Weibull' or 'Gompertz'
    :return: Conditional probability of failure for the specific pipe.
    """

    if a is None or b is None:
        verbose_("Please give the distribution parameters a and b", True)
        a = float(input("a:"))
        b = float(input("b:"))

    if distribution.lower().startswith("w"):
        F0 = weibull_cummulative_function(pipe[f"age_{year}"], a, b)
        F1 = weibull_cummulative_function(pipe[f"age_{year}"] + yearly_step, a, b)
        p_cond_failure = (F1 - F0) / (1 - F0)

    elif distribution.lower().startswith("g"):
        F0 = gompertz_model(pipe[f"age_{year}"], a, b)
        F1 = gompertz_model(pipe[f"age_{year}"] + yearly_step, a, b)
        p_cond_failure = (F1 - F0) / (1 - F0)

    else:
        verbose_(" Chose a deterioration model: 'Weibull' or 'Gompertz' ", verbose=True)

    return p_cond_failure


def get_distributions(year_i=1978, year_f=2038, result_dir=None, columns_needed=[f"age", f"prob_failure", f"failed"]):

    if not result_dir:
        result_dir = Path(rf"../Data/data/USTER/output/csv/Deterioration_{year_f - year_i}")

    columns_needed = [
        f"{column}_{year}" for year in range(year_i, year_f + 1) for column in [f"age", f"prob_failure", f"failed"]
    ]

    columns_age = [f"{column}_{year}" for year in range(year_i, year_f + 1) for column in [f"age"]]
    files_count = 0

    min_age = 9999
    max_age = 0

    for file in result_dir.iterdir():
        st = file.name
        iter = int(st[st.find("[") + 1 : st.rfind("]")])

        #     file = next(Path.joinpath(result_dir, f) for f in result_dir.iterdir() if Path.joinpath(result_dir, f).is_file())
        if file.is_file():
            files_count += 1
            # Get last column of dataframe in each simulation
            pipes = pd.read_csv(file, sep=";", header=0, usecols=columns_age)

            # Get youngest and oldest pipe ages to define the size of the new data frame
            min_age = min(min_age, min(pipes.min(axis=0, skipna=True, numeric_only=True)))
            max_age = max(max_age, max(pipes.max(axis=0, skipna=True, numeric_only=True)))

    # Dataframes to store frequencies
    # Dictionary of Dataframes with the failure data per age across the years of simulation for each random sample
    # The Dictionary contains the frequency, density and cumulative probability of failure of different pipe ages
    df__ = pd.DataFrame(index=range(files_count), data=0, columns=range(min_age, max_age + 1))

    # Prepare empty dictionaries
    df_distributions = dict(
        [
            (k, df__.copy())
            for k in [
                "frequency_failure",
                "frequency_survival",
                "frequency_pipes",
                "density_survival",
                "density_failure",
                "cumulative_survival",
                "cumulative_failure",
            ]
        ]
    )

    # LOOP PER SIMULATION RUN
    iter = 0

    for file in result_dir.iterdir():

        st = file.name
        iter = int(st[st.find("[") + 1 : st.rfind("]")])

        pipes = pd.read_csv(file, sep=";", header=0, usecols=columns_needed)  # Get dataframe

        amount_pipes = pipes.shape[0]

        frequency_failure = dict([(k, 0) for k in range(min_age, max_age + 1)])
        frequency_survival = dict([(k, 0) for k in range(min_age, max_age + 1)])

        frequency_pipes = dict([(k, 0) for k in range(min_age, max_age + 1)])

        for idx, pipe in pipes.iterrows():
            continue_ = True

            for year in range(year_i, year_f + 1):
                age = pipe[f"age_{year}"]

                if continue_:
                    if pipe[f"failed_{year}"] == 1:
                        continue_ = False

                        frequency_pipes[age] += 1
                        frequency_failure[age] += 1
                    else:
                        frequency_pipes[age] += 1
                        frequency_survival[age] += 1

                        # if year == year_f:
                        #     continue_ = False
                        #     # print(f" NON Failure pipe {idx}")

        # ---------- Finished loop through the pipes --------------

        # Save to dataframes
        for age in range(min_age, max_age + 1):
            df_distributions["frequency_failure"].loc[iter, age] = frequency_failure[
                age
            ]  # save failing pipes in the DataFrame
            df_distributions["frequency_survival"].loc[iter, age] = frequency_survival[
                age
            ]  # save surviving pipes in the DataFrame
            df_distributions["frequency_pipes"].loc[iter, age] = frequency_pipes[age]  # save pipes in the DataFrame

    # Density distributions
    df_distributions["density_failure"] = df_distributions["frequency_failure"].divide(amount_pipes, fill_value=np.NaN)

    for k, col_survival in df_distributions["frequency_survival"].items():
        df_distributions["density_survival"][k] = 1 - (
            (df_distributions["frequency_pipes"][k] - col_survival) / (amount_pipes)
        )
    # df_distributions["density_survival"] = df_distributions["frequency_survival"].divide(amount_pipes, fill_value=np.NaN)

    # Cumulative distributions
    sum = 0
    for k, v in df_distributions["density_failure"].iteritems():
        if k - 1 == 0:
            df_distributions["cumulative_failure"][k] = v
            df_distributions["cumulative_survival"][k] = 1 - v
        else:
            sum += v
            #         print(k, "\n", sum)
            df_distributions["cumulative_failure"][k] = sum
            df_distributions["cumulative_survival"][k] = 1 - sum

    return df_distributions


def plot_distributions(
    dataframe,
    type_dist="cumulative",
    label="failures",
    reference_data=[[1, 55, 75, 100, 135], [0, 0.25, 0.50, 0.75, 0.99]],
    distribution="Weibull",
    years_tot_sim=None,
    output_folder="output/figures/Deterioration_",
    verbose=False,
):
    """Build FREQUENCIES the plot"""
    min_values = []
    max_values = []
    avg_values = []
    std_values = []

    key = type_dist + "_" + label

    for idx, f in dataframe[key].iteritems():
        min_values.append(min(dataframe[key][idx]))
        max_values.append(max(dataframe[key][idx]))
        avg_values.append(np.mean(dataframe[key][idx]))
        std_values.append(np.std(dataframe[key][idx]))
    totals = dataframe[key].sum(axis=1)

    lower_bound = []
    upper_bound = []
    for item1, item2 in zip(avg_values, std_values):
        lower_bound.append(item1 - item2)
        upper_bound.append(item1 + item2)

    "Fit input distribution"
    __d_ages = np.array(reference_data[0])  # Reference quantile values from master thesis of Arreaza Bauer (2011)
    __d_failures = np.array(reference_data[1])  # Reference quantiles

    param_optimal, param_covariance = curve_fit(f=weibull_cummulative_function, xdata=__d_ages, ydata=__d_failures)
    verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)

    a = param_optimal[0]  # first parameter from the fitting process
    b = param_optimal[1]  # second parameter from the fitting process

    "Data"
    x = dataframe[key].columns
    y = avg_values

    "Build the plot"

    if type_dist.lower().startswith("d") and "surv" in label.lower():
        fig1, (ax, ax0) = plt.subplots(
            2, 1, sharex=True, figsize=(cm2inch((9, 9))), dpi=300, gridspec_kw={"height_ratios": [4, 1]}
        )
    else:
        fig1, ax = plt.subplots(figsize=(cm2inch((9, 9))), dpi=300)

    fig1.set_facecolor("white")

    # x axis labels
    ax.set_xlabel("Age [years]")
    max_x_labels = max(len(x), max(__d_ages))
    x_pos = np.arange(max_x_labels + 5, step=np.round(max_x_labels / 5, 0))

    if type_dist.lower().startswith("f"):
        ax.set_title("Frequency distribution")
        ax.set_ylabel(f"Frequency of {label} [# pipes]")

    elif type_dist.lower().startswith("d"):
        ax.set_title("Probability density function")
        ax.set_ylabel(f"Probability of {label} [-]")

        if "fail" in label.lower():
            "Plot input probability density distribution"
            failure_fit = dict([(i, weibull_density_function(i, a, b)) for i in range(max(__d_ages) + 1)])
            ax.plot(
                failure_fit.keys(),
                failure_fit.values(),
                c="black",
                linestyle="-",
                linewidth=0.5,
                mfc="none",
                label=f"Reference curve \n[a:{round(a, 2)} b:{round(b, 2)}]",
            )
        if "surv" in label.lower():

            # spec2 = gridspec.GridSpec(ncols=1, nrows=2, figure=fig1)
            # ax = fig1.add_subplot(spec2[0, 0])
            # ax0 = fig1.add_subplot(spec2[1, 0])
            # fig1.set_facecolor("white")

            # ax.bar(x, y, align='center', alpha=0.3, color="grey", label=label.capitalize())
            ax0.bar(x, y, align="center", alpha=0.3, color="grey", label=label.capitalize())

            "Plot input probability density distribution"
            survival_fit = dict([(i, weibull_survival_density_function(i, a, b)) for i in range(max(__d_ages) + 1)])
            ax.plot(
                survival_fit.keys(),
                survival_fit.values(),
                c="black",
                linestyle="-",
                linewidth=0.5,
                mfc="none",
                label=f"Reference curve \n[a:{round(a, 2)} b:{round(b, 2)}]",
            )

            ax.set_ylim(bottom=min(survival_fit.values()) - 0.02, top=max(survival_fit.values()) + 0.001)
            ax0.set_ylim(bottom=0, top=0.01)

            # hide the spines between ax and ax2
            ax.spines["bottom"].set_visible(False)
            ax0.spines["top"].set_visible(False)
            # ax0.spines['bottom'].set_visible(False)
            # ax.xaxis.tick_top()
            # ax.xaxis.tick_bottom()
            ax.tick_params(top=False, bottom=False)  # don't put tick labels at the top
            ax.set_xlabel("")
            ax0.xaxis.tick_bottom()
            ax0.set_xlabel("Age [years]")
            # max_x_labels = max(len(x), max(__d_ages))
            # x_pos = np.arange(max_x_labels + 5, step=np.round(max_x_labels / 5, 0))
            ax0.set_xticks(x_pos)
            ax0.grid(True, alpha=0.3, linewidth=0.1)

            d = 0.002  # how big to make the diagonal lines in axes coordinates
            # arguments to pass to plot, just so we don't keep repeating them
            kwargs = dict(transform=ax.transAxes, lw=0.1, color="k", clip_on=False)
            ax.plot((-d, +d), (-d, +d), **kwargs)  # top-left diagonal
            ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

            kwargs.update(transform=ax0.transAxes)  # switch to the bottom axes
            # ax0.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
            ax0.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal
            # fig1 = fig2

    elif type_dist.lower().startswith("c"):

        if "fail" in label.lower():
            verbose_("FAILURE PlOT", verbose=verbose)
            ax.set_title("Cumulative density function")
            ax.set_ylabel(f"Cumulative {label} probability [-]")

            "Plot input cumulative  distribution"
            failure_fit = dict([(i, weibull_cummulative_function(i, a, b)) for i in range(max(__d_ages) + 1)])
            ax.plot(
                __d_ages,
                __d_failures,
                c="black",
                marker="s",
                markersize=4,
                linestyle="",
                linewidth=0.5,
                mfc="none",
                label="Reference points",
            )
            ax.plot(
                failure_fit.keys(),
                failure_fit.values(),
                c="black",
                linestyle="-",
                linewidth=0.5,
                mfc="none",
                label=f"Reference curve \n[a:{round(a, 2)} b:{round(b, 2)}]",
            )

            "plot fitted curve based on histogram"
            x = list(dataframe[key].columns)
            y = avg_values
            param_optimal, param_covariance = curve_fit(
                f=weibull_cummulative_function, xdata=x, ydata=y, p0=[a, b], maxfev=1000
            )
            verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)

            a = param_optimal[0]  # first parameter from the fitting process
            b = param_optimal[1]  # second parameter from the fitting process

            failure_curve = [weibull_cummulative_function(k, a, b) for k in x]

            ax.plot(
                x,
                failure_curve,
                c="slategrey",
                alpha=1,
                linestyle="-",
                linewidth=0.5,
                label=f"Fitted curve \n[a:{round(a, 2)} b:{round(b, 2)}]",
            )
            failure_fit2 = dict([(i, weibull_cummulative_function(i, param_optimal[0], param_optimal[1])) for i in x])

        if "surv" in label.lower():
            verbose_("SURVIVAL PlOT", verbose=verbose)
            ax.set_title("Survival curve")
            ax.set_ylabel(f"Cumulative {label} probability [-]")

            "Plot input cumulative  distribution"
            survival_fit = dict([(i, weibull_survival_function(i, a, b)) for i in range(max(__d_ages) + 1)])
            ax.plot(
                survival_fit.keys(),
                survival_fit.values(),
                c="black",
                linestyle="-",
                linewidth=0.5,
                mfc="none",
                label=f"Reference curve \n[a:{round(a, 2)} b:{round(b, 2)}]",
            )

            "plot fitted curve based on histogram"
            x = list(dataframe[key].columns)
            y = avg_values

            param_optimal, param_covariance = curve_fit(
                f=weibull_survival_function, xdata=x, ydata=y, p0=[a, b], maxfev=1000
            )
            verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)
            a = param_optimal[0]  # first parameter from the fitting process
            b = param_optimal[1]  # second parameter from the fitting process

            survival_curve = [weibull_survival_function(k, a, b) for k in x]

            ax.plot(
                x,
                survival_curve,
                c="slategrey",
                alpha=1,
                linestyle="-",
                linewidth=0.5,
                label=f"Fitted curve \n[a:{round(a, 2)} b:{round(b, 2)}]",
            )

    else:
        verbose_("type_dist should be 'frequency', 'density', 'cumulative'")

    if label.lower().startswith("f"):
        color = "coral"
    elif label.lower().startswith("s"):
        color = "cornflowerblue"
    else:
        color = "midnightblue"

    ax.bar(x, y, align="center", alpha=0.3, color="grey", label=label.capitalize())
    ax.plot(x, y, color=color, linewidth=0.5, label="Average")
    ax.fill_between(x, lower_bound, upper_bound, alpha=0.3, linewidth=0, color=color, label="Standard deviation")

    ax.set_xticks(x_pos)
    ax.legend(loc="best", fontsize=5)
    ax.grid(True, alpha=0.3, linewidth=0.1)

    plt.tight_layout()

    # Save the figure and show
    output_filename = f"P2_{years_tot_sim}-Yr_[{dataframe[key].shape[0]}]_{type_dist}_{label}.png"
    output_file = os.path.join(output_folder, output_filename).replace("\\", "/")
    fig1.savefig(output_file, dpi=900)
    plt.show()

    return fig1
