"""
@file
@author Natalia Duque, Christian Foerster
@section LICENSE

Sequenced Sewer Networks Design (SSND)

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from genericpath import exists
import time

from algorithm import *
from defaults import *
from help_functions.helper_functions import *
from deterioration import *

verbose = True  # set to TRUE for debugging

# Start counting the execution time
st = float(time.time())

""" READ INPUT FILE """
# Working path and input file

path = Path(rf"Data/data/{case_study}")

    
""" START ALGORITHMS """

__run_design = input(
    "\nRun the hydraulic design? [Yes/No]: "
)  # Set to True if you have to (re)design the network at the different time steps
for year in range(year_0, year_f + 1, year_step):

    if __run_design.lower().startswith("y"):
        """ORGANISE DATA"""
        verbose_(f" Running hydraulic design {year} ...", verbose=True)
        # Read input file with the topology
        input_folder = path / r"output\shp\Topology"
        if input_folder.is_dir:
            print(f"input_folder path is not correct: \n{input_folder}")
        filename = None
    
        for file in input_folder.iterdir():
            verbose_(f"The input {file.name} for year {year}", verbose=verbose)
            if file.is_file() and f"({year})" in file.name:
                filename = file.name
                print(filename)
        if not filename:
            verbose_(f"The input file for year {year} has not been found", verbose=True)
            break

        file = input_folder / filename
        verbose_(f" network_file path: {file}", verbose=verbose)

        # Organize data in a dataframe
        manholes, pipes = read_input(file, verbose=verbose)
        verbose_(" {} pipes, {} manholes".format(pipes.shape[0], manholes.shape[0]), verbose=verbose)

        """ DESIGN PIPES """
        # Run design algorithm by walking the graph from the extremes to the outlet
        pipes = walk_graph(manholes, pipes, diameters=commercial_diameters, verbose=verbose)

        """ EXPORT RESULTS TO CSV """
        output_folder = "output/csv"
        output_filename = f"HD_{case_sudy_name}_({year}).csv"
        output_file = os.path.join(path, output_folder, output_filename).replace("\\", "/")
        pipes.to_csv(output_file, sep=";")

        """ EXECUTION TIME HYDRAULIC DESIGN """
        verbose_(f"\n HD time: {time.time() - st} s\n", verbose=verbose)

    else:
        """READ HYDRAULIC DESIGN RESULTS FROM CSV"""
        verbose_(f" Reading hydraulic design {year} ...", verbose=True)
        output_folder = path / Path(r"output/csv")
        filename = None
        for file in output_folder.iterdir():
            if file.is_file() and f"({year})" in file.name:
                filename = file.name
                print(filename)
        if not filename:
            verbose_(f"The input file for year {year} has not been found", verbose=True)
            break

        file = output_folder / filename
        verbose_(f" HD_file path: {file}", verbose=verbose)

        pipes = pd.read_csv(file, sep=";", header=0)
        pipes.set_index("id_up", drop=False, inplace=True)  # Set df index to pipe id
        pipes.sort_index(inplace=True)  # Sort pipes by index/id_up

    """ PRINT STATEMENTS """
    verbose_(f" ---------- HYDRAULIC DESIGN {year} ------------", verbose=verbose)
    print_full_df(pipes.head(), verbose=verbose)
    verbose_(" ------------", verbose=verbose)


"""Deterioration model"""
__run_deterioration = input("Run deterioration model: [Yes/No]: ")
if __run_deterioration.lower().startswith("y"):

    """Fit the CDF curve for the selected deterioration model"""
    param_optimal, param_covariance, failure_fit = run_deterioration_model(
        data_ages=d_ages, data_failures=d_failures, data_sim_times=d_sim_times, distribution=distribution, verbose=False
    )

    verbose_(f"param_optimal a:{param_optimal[0]}, b:{param_optimal[1]}", verbose=verbose)
    a = param_optimal[0]
    b = param_optimal[1]

    """ NETWORK DETERIORATION """
    # Random sampling
    for m in range(200):

        pipes = network_deterioration(
            pipes=pipes,
            initial_pipes_age=initial_pipes_age,
            distribution=distribution,
            a=a,
            b=b,
            year_i=year_i,
            year_0=year_0,
            year_f=year_f,
            year_step=1,
            verbose=True,
        )

        """ EXPORT RESULTS TO CSV """
        output_folder = f"output/csv/Deterioration_{years_tot_sim}"
        dir = os.path.join(path, output_folder).replace("\\", "/")
        isExist = os.path.exists(dir)
        if not isExist:
            os.makedirs(dir)

        output_filename = f"Deterioration_{case_sudy_name}_{years_tot_sim}_[{m}].csv"
        output_file = os.path.join(dir, output_filename).replace("\\", "/")
        pipes.to_csv(output_file, sep=";")

        """ EXECUTION TIME """
        verbose_(f"\n execution time [{m}]: {time.time() - st} s\n", verbose=True)

__plot_deterioration = input("Plot deterioration distributions? [Yes/No]: ")
if __plot_deterioration.lower().startswith("y"):

    df_distributions = get_distributions(
        year_i=year_i, year_f=year_f, result_dir=None, columns_needed=[f"age", f"prob_failure", f"failed"]
    )

    output_folder = f"output/figures/Deterioration_{years_tot_sim}"
    dir = os.path.join(path, output_folder).replace("\\", "/")
    isExist = os.path.exists(dir)
    if not isExist:
        os.makedirs(dir)

    for d in df_distributions.keys():

        k = d.split(sep="_")
        print(k)
        # print(df_distributions[d])

        fig1 = plot_distributions(
            df_distributions,
            type_dist=k[0],
            label=k[1],
            reference_data=[d_ages, d_failures],
            years_tot_sim=years_tot_sim,
            output_folder=dir,
        )

""" COSTS CALCULATION """
# sewer_investments = dict()
# total_costs = calculate_investments(pipes=designed_pipes)
# verbose_(f"Total costs of the network: {float(total_costs)} CHF", verbose=True)
# sewer_investments[year] = total_costs

""" EXECUTION TIME """
verbose_(f"\n Final time: {time.time() - st} s\n", verbose=True)
