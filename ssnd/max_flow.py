import timeit
from pyswmm import Links, Nodes, Simulation

depths = {}
flows = {}
diam = {}
outflow = []
start = timeit.default_timer()

with Simulation(r"C:\Users\fabrizia\Downloads\inp_generator\inp_generator\UB_7km_250_AF.inp") as sim:
    for step in sim:
        for link in Links(sim):
            if link.linkid not in flows.keys():
                flows[link.linkid] = [link.flow]
            flows[link.linkid].append(link.flow)
        print(flows)
        for node in Nodes(sim):
            if node.nodeid not in diam.keys():
                diam[node.nodeid] = [node.full_depth]
        print(diam)
        for link in Links(sim):
            if link.linkid not in depths.keys():
                depths[link.linkid] = [link.depth]
            depths[link.linkid].append(link.depth)
        print(depths)
stop = timeit.default_timer()
print("Time: ", stop - start)
