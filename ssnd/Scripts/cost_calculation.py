"""
@file
@author Natalia Duque
@section LICENSE

Sequenced Sewer Networks Design (SSND)

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import numpy as np
import pandas as pd
from ssnd.help_functions.helper_functions import add_empty_columns_df


# ----------------------------------------------
# ------------- COST FUNCTIONS -----------------
# ----------------------------------------------
def cost_pipe(diameter, avg_depth, length, cost_factor=1.2):
    """Cost Function for single sewer pipes
    [Source: Eggimann,2017]

    :param diameter: pipe diameter [m]
    :param avg_depth: pipe average depth [m]
    :param length: pipe length [m]
    :param cost_factor: cost factor default=1.2
    :return: construction cost of a single pipe
    """

    _a_ = 152.51 * diameter + 173.08
    _b_ = 760.31 * diameter - 78.21
    cost_m = _a_ * avg_depth + _b_ * cost_factor

    return cost_m * length


def cost_wwtp(param_a, param_b, population):

    if param_a is None or param_b is None:
        if population <= 500:  # small scale wwtp
            param_a = 13318.0
            param_b = 0.209
        else:  # large scale wwtp
            param_a = 7137.0
            param_b = 0.195

    return param_a * pow(population, -param_b) * population


def annual_OM_sewer(param_a=3.6, length=0.001):
    return param_a * length


def annual_OM_wwtp(param_a=380.09, param_b=0.204, population=0.001):
    return param_a * pow(population, -param_b) * population


# ------------------------------------------------
# ------------ ECONOMIC ANALYSIS -----------------
# ------------------------------------------------
def calculate_investments(pipes=None, file_HD=None):
    if pipes is None:
        # Read Input file if a database with the pipes is not given
        if file_HD is None:
            file_HD = "HD_USTER_200_374.csv"

        # Read hydraulic design
        folder = r"..\test\data\Output"
        path = os.path.join(folder, file_HD).replace("\\", "/")
        pipes = pd.read_csv(path, sep=";", header=0)
        pipes.set_index("id_up", drop=False, inplace=True)  # Set df index to pipe id
        pipes.sort_index(inplace=True)  # Sort pipes by index/id_up

    total_costs = 0.0

    pipes = add_empty_columns_df(pipes, columns=["s_cost"])
    pipe_costs = dict([(k, 0.0) for k in pipes.id_up])

    for id_up, pipe in pipes.iterrows():

        cost_p = cost_pipe(diameter=pipe.diameter, avg_depth=pipe.avg_depth, length=pipe.length, cost_factor=1.2)

        pipe.s_cost = cost_p  # TODO save directly in dataframe.. this is not sa
        pipe_costs[id_up] = cost_p  # TODO delete when the previous happens
        total_costs += pipe.s_cost

    pipes["s_cost"] = pipes["id_up"].map(pipe_costs)

    return total_costs


def economic_analysis(
    investments=None, discount_rate=0.02, planning_horizon=80, ls_sewers=80, ls_wwtp=25, year_i=None, year_f=None
):

    np.npv(rate=discount_rate, values=investments)

    return True
