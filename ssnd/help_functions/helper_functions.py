"""
@file
@author Natalia Duque, Christian Foerster
@section LICENSE

Sequenced Sewer Networks Design (SSND)

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
#region -- PYTHON IMPORTS
import os

import matplotlib.pyplot as plt
import math
from pathlib import Path
from random import randint
import numpy as np
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import matplotlib.colors as colors
# --- Spatial geometry ---
from shapely.geometry import  Polygon

#endregion

def profiler(func):
    def wrap(*args, **kwargs):
        import cProfile

        pr = cProfile.Profile()
        pr.enable()
        res = func(*args, **kwargs)
        pr.disable()
        pr.print_stats(sort="cumtime")
        return res

    return wrap


def verbose_(string, verbose=True):
    if verbose:
        print(str(string))


def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        "trunc({n},{a:.2f},{b:.2f})".format(n=cmap.name, a=minval, b=maxval), cmap(np.linspace(minval, maxval, n))
    )
    return new_cmap


def to_number(value):
    """_summary_

    Args:
        value (_type_): _description_

    Returns:
        _type_: _description_
    """    """"""
        
    """Converts a value (str) to a float or an int
    :param value: str
    :return: float or int
    """
    number = True
    try:
        float(value)
    except ValueError:
        number = False

    if number:
        if "." in repr(value):
            return float(value)
        else:
            return int(value)
    else:
        return value


def round_up(n, decimals=0):
    multiplier = 10**decimals
    return math.ceil(n * multiplier) / multiplier

# %%
def value_per_area (values, areas):
    return values/areas


def sorted_tuples(non_numeric_list:list, numeric_list:list):
    """ Sort values of two lists based on the values of one of them.
        At least one list has to be numeric.

    Args:
        non_numeric_list (list): list with non numeric values
        numeric_list (list): list with non numeric values

    Returns:
        _list_, _list_: sorted lists
    """
    non_numeric_list = ['c', 'b', 'a', 'd', 'e']
    numeric_list = [3, 2, 1, 4, 5]
    
    # sort both lists based on the numeric list
    sorted_tuples = sorted(zip(numeric_list, non_numeric_list))
    sorted_numeric = [t[0] for t in sorted_tuples]
    sorted_non_numeric = [t[1] for t in sorted_tuples]

    # print the sorted lists
    print(sorted_numeric) # output: [1, 2, 3, 4, 5]
    print(sorted_non_numeric) # output: ['a', 'b', 'c', 'd', 'e']
    
    return sorted_non_numeric, sorted_numeric

def read_input(file_blocks, file_pipes=None, verbose=False):
    """Convert input data in dataframes depending on the file format.

    :param file: Contains input data in .txt or .csv from manholes and pipes or .shp from manholes
    :param file2: Contains input data in .shp from pipes
    :param verbose: boolean to print
    :return: manholes dataframe and pipes dataframe
    """
    filename, file_extension = os.path.splitext(file_blocks)

    if file_extension == ".txt":
        manholes, pipes = network_to_dataframe(read_network_file(file_blocks), verbose)
        return manholes, pipes

    elif file_extension == ".csv":
        manholes, pipes = read_csv_to_dataframe(file_blocks, verbose)
        return manholes, pipes

    elif file_extension == ".shp":
        manholes = read_shp_to_dataframe(file_blocks, verbose)
        pipes = read_shp_to_dataframe(file_pipes, verbose)
        return manholes, pipes

    else:
        verbose_(
            "Please make sure to use .txt, .csv or .shp files! You can check the required format in the README file"
        )


def network_to_dataframe(network_dict, verbose):
    """
    This method saves the input data for the Manholes and Pipes into a data frame
    :param network_dict:   { manholes: ["id", "x", "y", "z", "outlet"],
                             pipes: ["id_up", "id_down", "type", "design_flow"]}
    :return: data frame of the network
    """
    manholes = pd.DataFrame.from_dict(network_dict["manholes"])
    manholes.set_index("id", drop=False, inplace=True)  # Set df index to manhole id
    manholes.sort_index(inplace=True)  # Sort manholes by index/id
    verbose_(manholes, verbose)

    pipes = pd.DataFrame.from_dict(network_dict["pipes"])
    pipes.set_index("id_up", drop=False, inplace=True)  # Set df index to pipe id
    pipes.sort_index(inplace=True)  # Sort pipes by index/id_up
    verbose_(pipes, verbose)
    return manholes, pipes


def network_to_geo_dataframe(network_dict, verbose):
    """
    This method saves the input data for the Manholes and Pipes into a data frame
    :param network_dict:   { manholes: ["id", "x", "y", "z", "outlet"],
                             pipes: ["id_up", "id_down", "type", "design_flow"]}
    :return: data frame of the network
    """
    manholes = pd.DataFrame.from_dict(network_dict["manholes"])
    manholes.set_index("id", drop=False, inplace=True)  # Set df index to manhole id
    manholes.sort_index(inplace=True)  # Sort manholes by index/id
    verbose_(manholes, verbose)

    pipes = pd.DataFrame.from_dict(network_dict["pipes"])
    pipes.set_index("id_up", drop=False, inplace=True)  # Set df index to pipe id
    pipes.sort_index(inplace=True)  # Sort pipes by index/id_up
    verbose_(pipes, verbose)
    return manholes, pipes

def read_network_file(
    file_path,
    sep=" ",
    intensive_check=True,
    columns={
        "manholes": ("id", "x", "y", "z", "outlet"),
        "pipes": ("id_up", "id_down", "type", "design_flow"),
    },
):
    """
    This method manages the input data for the Manholes (id, x, y, z, outlet) and Pipes (connectivity between manholes)
    :param file_path: str
    :param sep: str
    :param intensive_check: bool
    :param columns: dict
    """
    if not os.path.exists(file_path):
        # Input/Output Error if the file is not found.
        raise IOError("The file '{}' could not be found.".format(file_path))

    network = {"manholes": [], "pipes": []}

    with open(file_path, "r") as network_file:
        for line in network_file:
            if line.lower().startswith("manhole"):
                num_manholes = int(line.split(sep)[-1])
                key = "manholes"
                continue
            elif line.lower().startswith("id") and key == "manholes":
                columns["manholes"] = list(line.split(sep))
                print(columns)

            elif line.lower().startswith("section") or line.lower().startswith("pipe"):
                num_pipes = int(line.split(sep)[-1])
                key = "pipes"
                continue
            elif line.lower().startswith("id") and key == "pipes":
                columns["pipes"] = list(line.split(sep))
                print(columns)

            entry = dict()
            for k, v in zip(columns[key], line.split(sep)):
                entry[k] = to_number(v)

            network[key].append(entry)

    # check if all manholes and sections are in the input file
    if len(network["manholes"]) != num_manholes:
        # maybe create own error InputFileError or something like that
        raise ValueError(
            "The network file contains '{}' manholes but should have '{}'.".format(
                len(network["manholes"]), num_manholes
            )
        )

    if len(network["pipes"]) != num_pipes:
        # maybe create own error InputFileError or something like that
        raise ValueError(
            "The network file contains '{}' pipes but should have '{}'.".format(len(network["pipes"]), num_pipes)
        )

    if intensive_check:
        manholes = []
        manholes_in_pipes = []

        for mh in network["manholes"]:
            manholes.append(mh["id"])

        for pi in network["pipes"]:

            up = pi["id_up"]
            down = pi["id_down"]

            if up == down:
                # maybe create own error InputFileError or something like that
                raise ValueError("A pipe has the same up stream and down stream manhole id '{}'.".format(up))

            manholes_in_pipes.extend([up, down])

        # create sets of manholes and pipes
        set_mh = set(manholes)
        set_pi = set(manholes_in_pipes)

        # check if manhole is assigned more than once
        if len(manholes) != len(set_mh):

            multiple_entries = []
            for i in range(len(manholes))[:-1]:
                if manholes[i] in manholes[:i] + manholes[i + 1 :]:
                    multiple_entries.append(manholes[i])

            raise ValueError(
                "At least one manhole id occurred more than once in the network file. Candidates are: '{}'.".format(
                    ", ".join([str(e) for e in multiple_entries])
                )
            )

        # check if any manholes are not in pipes
        # missing_mh = set_mh.difference(set_pi)
        # if missing_mh != set():
        # raise ValueError("At least one manhole id is not present in the pipes section of your network file. Candidates are: '{}'.".format(
        # ", ".join([str(e) for e in missing_mh])))

        # check pipes refer to non specified manhole
        missing_mh = set_pi.difference(set_mh)
        if missing_mh != set():
            raise ValueError(
                "At least one manhole id used in the pipes section hast not been specified in the manholes section. Cadidates are: '{}'.".format(
                    ", ".join([str(e) for e in missing_mh])
                )
            )

    return network



# %%
""" READ INPUT FILE """
# Working path and input file
def read_geoDataFrame(data_path:Path, case_study:str, read_module:str, connectivity:str, name:str , year:int, iteration:int=-9999):
    """

    Args:
        input_path (Path): _description_
        case_study (str): _description_
        read_module (str): _description_
        connectivity (str): _description_
        year (int): _description_

    Returns:
        _type_: _description_
    """    
    if iteration == -9999:
        iteration= ""
    else:
        iteration = f"[{iteration}]"
    
    path_df=data_path /read_module.lower()/connectivity.lower()/fr"{connectivity.lower()}_{case_study}_{name}_({year}){iteration}/{connectivity.lower()}_{case_study}_{name}_({year}){iteration}.shp"
    gdf = gpd.read_file(path_df)
   
    return gdf


#region Create files 
def set_file_path (path:str, 
                   case_study:str="",  
                   module:str="", 
                   connectivity:str="", 
                   name:str="", 
                   year:int=None, 
                   iteration:int=None):
    """ Set file output path. It creates the directory if it does not exist"""
    
    
    path = Path(path)/module/connectivity
    path.mkdir(parents=True, exist_ok=True)
    
    if connectivity:
        connectivity= f"{connectivity}_"
    if case_study:
        case_study= f"{case_study}_"
    if name:
        name= f"{name}_"
    if year:
        year = f"({year})"
    if iteration:
        iteration = f"[{iteration}]"
    else:
        iteration = ""
    
    
    
    file_name = connectivity + case_study + name + str(year) + str(iteration)
    file_path = path/file_name
    verbose_(f" Writing file: {file_path}")
    return file_path


def xx (path, case_study, name, year, data_type):
    filename = f"{case_study}_{name}_({year}).{data_type}"
    if filename[-4:]  == ".shp":
        folder = filename[:-4]

    else:
        folder = ''

    file_path = path / folder / filename
    return file_path

def save_df_in_dict (dict:dict, df:pd.DataFrame, key:int):
    dict[key] = df
    return dict


def save_shp_file(df, output_path:Path="", module:str="", connectivity:str="",  case_study:str="", name:str="", year:int="", iteration:int="" ):

    ## create shp file
    output_path_shp =  output_path / "shp"
    file_path = set_file_path (output_path_shp, module=module, connectivity=connectivity,  case_study=case_study, name=name, year=year, iteration=iteration)
    df.to_file(file_path, crs=df.crs)
    return True

def save_csv_file(df, output_path:Path="", module:str="", connectivity:str="",  case_study:str="", name:str="", year:int="", iteration:int="" ):

    ## create csv file
    output_path_csv = Path(output_path) / "csv"
    file_path = set_file_path (output_path_csv, module=module, connectivity=connectivity,  case_study=case_study, name=name, year=year, iteration=iteration)
    df.drop('geometry',axis=1).to_csv(fr'{file_path}.csv', sep=";")
    return True


# rewrite specific columns 

def rewrite_df_column_data(file_path:Path, column_name:str, new_values:pd.Series):
    """ 
        @param file_path: path to input and output file
        @param column_name: name of the column to update
        @param new_values: new series or dictionary to update the values in the column
         
    """
    # Read the shp file into a GeoPandas DataFrame to be modified
    df = gp.read_file(file_path)

    # Update the values in the specific column
    df[f'{column_name}'] = new_values

    # Write the DataFrame back to 
    df.to_file(file_path, crs=df.crs)  # the shp file
    df.drop('geometry',axis=1).to_csv(file_path.parent.with_suffix('.csv'), sep=";") #the CSV file
    return True

#%%
def copy_columns_between_dataframes(path_to_shapefile_a, path_to_shapefile_b,path_to_updatedshapefile_b, columns_to_copy, ):
    # read in the two shapefiles as GeoDataFrames
    df_a = gpd.read_file(path_to_shapefile_a) # To be copied
    df_b = gpd.read_file(path_to_shapefile_b) # To be modified 

    

    # loop through each row in df_a and copy the specified columns to df_b
    for idx, row in df_a.iterrows():
        block_id = row['BlockID']
        if block_id in df_b['BlockID'].values:
            # if the block_id exists in df_b, copy the specified columns
            for col in columns_to_copy:
                if col not in df_b.columns:
                    # if the column does not exist in df_b, initialize it to 0
                    df_b[col] = 0
                df_b.loc[df_b['BlockID'] == block_id, col] = row[col]

    # write the updated df_b to a new shapefile
    df_b.to_file(path_to_updatedshapefile_b)
    df_b.drop('geometry',axis=1).to_csv(path_to_updatedshapefile_b.parent.with_suffix('.csv'), sep=";") #the CSV file
    return True

def read_csv_to_dataframe(file, verbose=False):
    num_manholes = int(pd.read_csv(file, sep=";", nrows=0).columns[1])
    verbose_(f"Manholes {num_manholes}", verbose)

    manholes = pd.read_csv(file, sep=";", header=1, nrows=num_manholes)
    colMh_0 = manholes.columns[0]
    manholes.set_index(colMh_0, drop=False, inplace=True)
    manholes.sort_index(inplace=True)
    verbose_(manholes, verbose)

    num_pipes = int(pd.read_csv(file, sep=";", skiprows=num_manholes + 2, nrows=0).columns[1])
    verbose_(f"Pipes {num_pipes}", verbose)
    pipes = pd.read_csv(file, sep=";", header=num_manholes + 3, nrows=num_pipes)
    colPp_0 = pipes.columns[0]
    pipes.set_index(colPp_0, drop=False, inplace=True)
    pipes.sort_index(inplace=True)

    pd.set_option("display.max_rows", pipes.shape[0])  # None or 1000
    pd.set_option("display.max_columns", pipes.shape[1])  # None or 1000
    pd.set_option("display.max_colwidth", None)  # None or 199
    pd.set_option("display.max_seq_item", None)  # None or 199
    verbose_(pipes, verbose)

    return manholes, pipes


def read_shp_to_dataframe(file_blocks, file_pipes, verbose=False):
    """Import pipe data from ArcGIS
    :param file_pipes: Shape file
    :param verbose: Boolean for printing statements
    :return: dataframe
    """

    # --------------- Export Manhole data ----------------
    # ----------------------------------------------------
    blocks = gp.read_file(file_blocks)

    # Only reserve block data where pipes exist
    # for i in range(df_blocks.shape[0]):
    #     if not pd.DataFrame(df_blocks)["HasSWW"][i]:
    #         df_blocks.drop(i, inplace=True)

    # df_blocks["Outfall"] = 0
    # df_blocks.loc[df_blocks["BlockID"] == 671, "Outfall"] = 1

    # Export blocks data
    # blocks = df_blocks.loc[:, ("BlockID", "CentreX", "CentreY", "AvgElev", "Outfall")]
    # blocks.columns = ["id", "x", "y", "z", "outfall"]
    # blocks.set_index("id", drop=False, inplace=True)
    # df_manhole.to_csv(r"C:\Users\84111\polybox\Shared\2021_ChenCheng\06_Software\Output\manhole.txt", sep = ' ', index = False, header = None)

    # --------------- Export section data ----------------
    # ----------------------------------------------------
    pipes = gp.read_file(file_pipes)
    # pipes = df_pipes.loc[:, ["UpID", "DownID", "Type"]]
    # pipes.set_index("UpID", drop=False, inplace=True)
    # pipes.sort_values("UpID", inplace=True)

    # Export pipes flow data
    # df_flow = df_blocks.reset_index(drop=True)
    # df_flow.drop(152, inplace=True)
    # pipes["Flow"] = list(df_flow["Q_out"])
    # df_section

    # ---------- Print Dataframes --------------
    pd.set_option("display.max_rows", pipes.shape[0])  # None or 1000
    pd.set_option("display.max_columns", pipes.shape[1])  # None or 1000
    pd.set_option("display.max_colwidth", None)  # None or 199
    pd.set_option("display.max_seq_item", None)  # None or 199
    verbose_(blocks, verbose)
    verbose_(pipes, verbose)
    return blocks, pipes

#endregion

def add_empty_columns_df(dataframe, columns:list=None, values:list=None):
    """Add empty columns to df
    :param dataframe:
    :param columns: List of new columns to add to the data frame
    """

    if columns is None:
        columns = [
            "length",  # [m]
            "diameter",  # [m]
            "slope",  # [-]
            "tElev_up",  # [m.a.s.l]
            "tElev_down",  # [m.a.s.l]
            "up_elevation",  # [m.a.s.l]
            "down_elevation",  # [m.a.s.l]
            "up_depth",  # [m]
            "down_depth",  # [m]
            "avg_depth",  # [m]
            "pump",  # [m]
            "drop",  # [m]
            "exact_flow",  # [m3/s]
            "filling_ratio",  # [%]
            "y_n",  # [m]
            "angle",  # [rad]
            "h_radius",  # [m]
            "area",  # [m2]
            "velocity",  # [m/s]
            "froude",  # [-]
            "tau"  # [Pa]
            # "s_cost"            # [Pa]
        ]

    for k, col in enumerate(columns):
        if values:
            dataframe[col] = values[k]
        else:
            dataframe[col] = ""

    return dataframe


def print_full_df(dataframe, verbose=True):
    if verbose:
        pd.set_option("display.max_rows", dataframe.shape[0])  # None or 1000
        pd.set_option("display.max_columns", dataframe.shape[1])  # None or 1000
        pd.set_option("display.max_colwidth", None)  # None or 199
        print(dataframe)
        pd.reset_option("display.max_columns")


def create_test_data(blocks_dict:dict, grid:int, start_id:int, module:str, connectivity:str, case_study:str, name:str):
    """Create testing data from blocks dictionary containing yearly geopandas dataframe for the block urban characteristics 
    """

    test_blocks_dict = dict()

    if grid <=0:
        grid=1
        

    for idx, year in enumerate(blocks_dict.keys()):
        
        blocks_= blocks_dict[year]

        block_start = blocks_[blocks_["BlockID"]==start_id]
        
        # get extends (vertices) from of the block to start
        g = [i for i in block_start.geometry]
        x,y = g[0].exterior.coords.xy
        all_coords = np.dstack((x,y)) ####
        # print(all_coords)
        low_left = all_coords[0][0]
        up_right = all_coords[0][2]
        # print(p0,p2, "\n")

        size_horizontal = up_right[0]-low_left[0]       # substract x coordinates
        size_vertical = up_right[1]-low_left[1]         # substract y coordinates
        # print(size_horizontal, size_vertical)
        
        # create a polygon to intersect the blocks of interest to get a grid of the specified number of blocks
        r=100
        edge_legth = size_horizontal*(grid-1)

        p0= [low_left[0] + size_horizontal/2, low_left[1] + size_horizontal/2]
        p1= [low_left[0] + size_horizontal/2, low_left[1] + size_horizontal/2 + edge_legth]
        p2= [low_left[0] + size_horizontal/2 + edge_legth, low_left[1] + size_horizontal/2 + edge_legth]
        p3= [low_left[0] + size_horizontal/2 + edge_legth, low_left[1] + size_horizontal/2 ]
        p4= p0

        new_coords = [p0,p1,p2,p3,p4]
        # print(new_coords)
        poly = Polygon(new_coords)
        # print(poly)

        intestected_blocks= blocks_[blocks_.geometry.intersects(poly)]

        test_blocks_dict[year] = intestected_blocks

        path = fr"..\tests\00_testing_data"

        file_path = set_file_path (path, module=module, connectivity=connectivity, case_study= case_study, name=name, year=year)
        intestected_blocks.to_file(file_path, crs=intestected_blocks.crs)
        
        print("\n")
    return test_blocks_dict

def check_depth(ground_elevations, invert_elevations, min_depth, max_depth):
    """This method checks the invert elevations are between the excavation limits
    :param ground_elevations: dict
    :param invert_elevations: dict
    :param min_depth: float
    :param max_depth: float
    :return: dict
    """

    check = {}
    for manhole in ground_elevations:
        depth = ground_elevations[manhole] - invert_elevations[manhole]
        check[manhole] = (depth >= min_depth) and (depth <= max_depth)

    return check


def check_pumps(pipes, pipe_slopes, pipe_lengths, manhole_depth, pumps, verbose=False):
    """
    :param pipes: dict
    :param pipe_slopes: dic
    :param pipe_lengths: dict
    :param manhole_depth: dict
    :param pumps: dict
    :return: dict
    """

    check = {}
    for start, end in pipes.items():
        slope_check = pipe_slopes[start] <= (manhole_depth[start] - manhole_depth[end]) / pipe_lengths[start]

        if not slope_check:
            slope_check = (
                abs(pipe_slopes[start] - (manhole_depth[start] - manhole_depth[end]) / pipe_lengths[start]) < 0.00001
            )

        verbose_(
            "{}, {} -> {}; Slope -> {:.5f}; Len -> {:.5f}; dH -> {:.5f}; "
            "depth_{} -> {:.5f}; depth_{} -> {:.5f}; "
            "calcSlope -> {:.5f}".format(
                slope_check,
                start,
                end,
                pipe_slopes[start],
                pipe_lengths[start],
                pipe_slopes[start] * pipe_lengths[start],
                start,
                manhole_depth[start],
                end,
                manhole_depth[end],
                (manhole_depth[start] - manhole_depth[end]) / pipe_lengths[start],
            ),
            verbose,
        )

        check[start] = True if slope_check else end in pumps

    verbose_(pumps, verbose)
    return check


def pumping_power(manholes, min_depth, pumps, design_flow, verbose=True):
    for k, p in pumps.items():
        z = manholes.z[manholes.id == k].values[0]
        min_de = z - min_depth
        # print("- {}  z: {}, min_depth {}, invert {}, new {}, pumping height: {}, d {}".format(k, z, min_de, p[0], p[1], p[2], diameters[k]))

    total_pump_height = 0.0
    t_design_flow = 0.0
    for k, p in pumps.items():
        total_pump_height += p
        t_design_flow += design_flow[k]

    power = 1000 * t_design_flow * 9.81 * total_pump_height / 100
    verbose_(
        "..Energy to pump  for {} m and {} m3/s : {} kW".format(total_pump_height, t_design_flow, power),
        verbose,
    )


def retrieve_diameters(diameters_idx_tracker, chosen_slopes, diameter_tracker, diameters):
    """Retrieve the exact diameters for each designed pipe

    :param diameters_idx_tracker: list of diameters indexes for each pipe
    :param chosen_slopes:
    :param diameter_tracker:
    :param diameters:
    :return:
    """

    # convert diameter indexes to actual diameters in [m]
    # print(diameters_idx_tracker)
    for mh, idx in diameters_idx_tracker.items():
        if idx == -1:
            diameter_tracker[mh] = 0.0
            chosen_slopes[mh] = 0.0
        else:
            diameter_tracker[mh] = diameters[idx]

    return diameter_tracker, chosen_slopes


def KL_divergence(observations_1, observations_2):
    """Calculate the KL_divergence between two distributions

    :param observations_1: list of observed data from the first distribution
    :param observations_2: list of observed data from the second distribution
    :return: Kl-divergence index
    """

    observations_1 = np.asarray(observations_1, dtype=np.float)
    observations_2 = np.asarray(observations_2, dtype=np.float)
    np.seterr(divide="ignore")

    return np.sum(observations_1 * np.where(observations_1 != 0, np.log(observations_1 / observations_2), 0))


def cm2inch(*tupl):
    """Convert cm to inch

    :param tupl: number in cm
    :return: number in inches inch
    """
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i / inch for i in tupl[0])
    else:
        return tuple(i / inch for i in tupl)


def plot_distributions_diameter(time):
    """Plot the frequency distributions

    :param observations: dict of frequency distribution for the different design methodologies
    :param labels: list of diameters/ lengths
    :return:
    """
    # print(observations.values())
    # hist = np.histogram([observations.values], bins=labels)
    # print(hist)

    #
    real = [
        0.824,
        0.02,
        0.067,
        0.031,
        0.018,
        0.012,
        0.002,
        0.003,
        0.007,
        0.008,
        0.008,
        0.011,
        0.003,
    ]
    SND = [
        0.814,
        0.04,
        0.075,
        0.024,
        0.009,
        0.007,
        0.001,
        0.004,
        0.0,
        0.003,
        0.004,
        0.031,
        0.005,
    ]
    PBP = [
        0.86251809,
        0.0,
        0.059334298,
        0.00723589,
        0.033285094,
        0.00723589,
        0.010130246,
        0.004341534,
        0.004341534,
        0.010130246,
        0.0,
        0.0,
        0.001447178,
    ]
    UWIM = [
        0.81945,
        0.0,
        0.0026,
        0.00195,
        0.0052,
        0.00714,
        0.02143,
        0.03312,
        0.04546,
        0.06365,
        0.0,
        0.0,
        0.0,
    ]
    #
    # columns = labels
    # rows = ['%d' % x for x in (100, 50, 20, 10, 5)]
    verbose_("sum {} {} {} {}".format(sum(real), sum(SND), sum(PBP), sum(UWIM)))

    data = [
        [
            0.824,
            0.02,
            0.067,
            0.031,
            0.018,
            0.012,
            0.002,
            0.003,
            0.007,
            0.008,
            0.008,
            0.011,
            0.003,
        ],
        # [.814, 0.04, .075, .024, .009, .007, 0.001, 0.004, 0.0, 0.003, 0.004, 0.031, 0.005],
        [
            0.86251809,
            0.0,
            0.059334298,
            0.00723589,
            0.033285094,
            0.00723589,
            0.010130246,
            0.004341534,
            0.004341534,
            0.010130246,
            0.0,
            0.0,
            0.001447178,
        ],
        # [.81945, 0.0, .0026, .00195, .0052, .00714, 0.02143, 0.03312, 0.04546, 0.06365, 0.0, 0.0, 0.0]
    ]

    columns = ["%.2f" % x for x in (0.225, 0.25, 0.35, 0.4, 0.5, 0.6, 0.80, 1, 1.20, 1.5, 2, 2.5, 3.0)]
    labels = ("Real Sewer", "SND", "PBP", "UWIM")
    # kl = "KL_divergence\n SND:  {:.2f}\n " \
    #      "PBP:    {:.2f}\n UWIM:  {:.2f}".format(KL_divergence(SND, real),
    #                                             KL_divergence(PBP, real),
    #                                             KL_divergence(UWIM, real))
    kl_dict = {
        "Real Infrastructure": 0,
        "SND": KL_divergence(SND, real),
        "PBP": KL_divergence(PBP, real),
        "UWIM": KL_divergence(UWIM, real),
    }

    verbose_("KL_divergence: ".format(kl_dict))

    # Set plot size in cm
    figsize = cm2inch(9, 9)
    plt.figure(figsize=figsize)  # width:18, height:9
    plt.rcParams["font.family"] = "cambria"

    # Get some pastel shades for the colors
    # colors = plt.cm.gist_gray(np.linspace(0.2, .7, len(labels)))
    # colors = plt.cm.inferno(np.linspace(0.2, .7, len(labels)))
    colors = plt.cm.twilight_shifted(np.linspace(0.2, 0.7, len(labels)))
    # colors = plt.get_cmap('tab20c').colors[:]
    n_rows = len(data)

    index = np.arange(len(columns)) - 0.5
    bar_width = 0.1

    # Plot bars and create text labels for the table
    cell_text = []
    fig, (ax, ax2) = plt.subplots(2, 1, sharex=True)
    # plt.bar(index - bar_width, data[0], bar_width, label='Real',  color=colors[0])
    # plt.bar(index - bar_width / 2, data[1], bar_width, label='SND',  color=colors[1])
    # plt.bar(index + bar_width / 2, data[2], bar_width, label='Rational',  color=colors[2])
    # plt.bar(index + bar_width, data[3], bar_width, label='UWIM',  color=colors[3])
    # y_offset = data[row]
    # print(y_offset)
    # cell_text.append(['%1.2f' % (x*100) for x in y_offset])

    x = np.arange(len(columns))
    # for row in range(n_rows):
    #
    #     ax.bar(index + row * bar_width, data[row], bar_width, label=labels[row],  color=colors[row])
    #     ax2.bar(index + row * bar_width, data[row], bar_width, label=labels[row], color=colors[row])
    #     y_offset = data[row]
    #     cell_text.append(['%1.4f' % (x) for x in y_offset])
    # r1 = plt.bar(x, real, "b-", ms=10, mfc="r", mew=2, mec="r")
    # snd1 = plt.bar(x, SND, "r-", ms=10, mfc="r", mew=2, mec="r")
    # pbp1 = plt.bar(x, PBP, "g-", ms=10, mfc="r", mew=2, mec="r")
    # uwim1 = plt.bar(x, UWIM, "c-", ms=10, mfc="r", mew=2, mec="r")

    width = 0.15
    real_ax = ax.bar(
        x - width * 1.5,
        real,
        width,
        align="center",
        label="Real Sewer",
        fill=False,
        edgecolor="black",
        hatch="///",
    )
    real_ax2 = ax2.bar(
        x - width * 1.5,
        real,
        width,
        align="center",
        label="Real Sewer",
        fill=False,
        edgecolor="black",
        hatch="///",
    )

    snd_ax = ax.bar(x - width / 2, SND, width, align="center", label="SND", color=colors[1])
    snd_ax2 = ax2.bar(x - width / 2, SND, width, align="center", label="SND", color=colors[1])

    pbp_ax = ax.bar(x + width / 2, PBP, width, align="center", label="PBP", color=colors[0])
    pbp_ax2 = ax2.bar(x + width / 2, PBP, width, align="center", label="PBP", color=colors[0])

    uwim_ax = ax.bar(x + width * 1.5, UWIM, width, align="center", label="UWIM", color=colors[2])
    uwim_ax2 = ax2.bar(x + width * 1.5, UWIM, width, align="center", label="UWIM", color=colors[2])

    # Reverse colors and text labels to display the last value at the top.
    # colors = colors[::-1]
    # cell_text.reverse()

    # Add a table at the bottom of the axes
    # the_table = plt.table(cellText=cell_text,
    #                       rowLabels=rows,
    #                       rowColours=colors,
    #                       colLabels=columns,
    #                       loc='bottom')
    # the_table.auto_set_font_size(False)
    # the_table.set_fontsize(14)

    # Adjust layout to make room for the table:
    # plt.subplots_adjust(left=0.2, bottom=0.2)

    # zoom-in / limit the view to different portions of the data
    ax.set_ylim(0.60, 0.9)  # outliers only
    ax2.set_ylim(0, 0.08)  # most of the data

    # hide the spines between ax and ax2
    ax.spines["top"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax2.spines["top"].set_visible(False)

    # ax.tick_params(axis='x',  # changes apply to the x-axis
    #                # which='both',  # both major and minor ticks are affected
    #                bottom='off',  # ticks along the bottom edge are off
    #                top='off',  # ticks along the top edge are off
    #                labeltop=False,
    #                labelbottom=False  # labels along the bottom edge are off
    # )
    ax.tick_params(bottom=False, labeltop=False, labelbottom=False)
    ax2.set_xticks(x)
    ax2.set_xticklabels(columns, x=-2 * width, rotation=0, fontsize=10)

    # In axes coordinates, which are always between 0-1,
    # spine endpoints are at these locations (0,0), (0,1),
    # (1,0), and (1,1).  Thus, we just need to put the diagonals in the
    # appropriate corners of each of our axes, and so long as we use the
    # right transform and disable clipping.

    d = 0.005  # how big to make the diagonal lines in axes coordinates

    # arguments to pass to plot, just so we don't keep repeating them
    kwargs = dict(transform=ax.transAxes, color="k", clip_on=False)
    ax.plot((-d, +d), (-d, +d), **kwargs)  # top-left diagonal
    ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

    kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
    ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
    ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal

    plt.ylabel("Proportion [%]", y=1, fontsize=16)
    # plt.yticks(values * value_increment, ['%g' % (val * 100) for val in values], fontsize=12)
    plt.xlabel("Diameter [m]", fontsize=16)

    ax.legend()

    # plt.text(x=10, y=0.18, s=kl, fontsize=14, bbox=dict(facecolor='white', alpha=0.2))

    plt.savefig(f"../Data/figures/P1_Fig4_distributions({time}).svg", dpi=900)
    plt.show()
    pass

# %%
