# Plot Maps with geopandas

"""
@file   topology_delineation.py
@author  Natalia Duque <natalia.duquevillarreal@eawag.ch>
@section LICENSE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Natalia Duque"
__copyright__ = "Copyright 2018."

#region--- PYTHON LIBRARY IMPORTS ---
#from asyncio.windows_utils import pipe
from math import *
from pathlib import Path
from random import randint
import numpy as np
import pandas as pd
import seaborn as sns
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import matplotlib.colors as colors
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
import mapclassify

# --- Spatial geometry ---
from shapely.geometry import Point, Polygon, LineString
# --- Graph theory algorithms ---

# --- URBANBEATS LIBRARY IMPORTS ---

# --- SND IMPORTS ---
from ssnd.help_functions.helper_functions import *
#endregion

#region Create color ramps

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        "trunc({n},{a:.2f},{b:.2f})".format(n=cmap.name, a=minval, b=maxval), cmap(np.linspace(minval, maxval, n))
    )
    return new_cmap


def inter_from_256(x: int):
    """define normalized color_ramp numbers"""
    return np.interp(x=x, xp=[0, 255], fp=[0, 1])


def create_categorical_color_ramp(colors_dict=None, name: str = "new_cmap"):
    """ Create a categorical color ramp
    :param colors: 2xN list [[R,G,B, alpha],[R,G,B, alpha]]"""

    if not colors_dict:
        alpha = 1
        
        # colors dictionarry   [[R, G, B, alpha]]
        colors_dict = [ 
        # Residential (RE) # 1 Ub LUC 
        [inter_from_256(114), inter_from_256(3), inter_from_256(3), alpha], # Red hex=720303 
        # Commercial (COM) # 2 Ub LUC 
        [inter_from_256(180), inter_from_256(65), inter_from_256(65), alpha], # light red hex=B44141 
        # Mixed offices and residential (ORC) # 3 Ub LUC 
        [inter_from_256(255), inter_from_256(154), inter_from_256(105), alpha], # peach (orange) hex=FF9A69 
        # Ligth industry (LI) # 4 Ub LUC 
        [inter_from_256(255), inter_from_256(196), inter_from_256(88), alpha], # Ligth orange hex=FFC458
        # Heavy industry (HI) # 5 Ub LUC 
        [inter_from_256(255), inter_from_256(175), inter_from_256(31), alpha], # orange  hex=FFAF1F 
        # Civic (CIV) # 6 Ub LUC 
        [inter_from_256(255), inter_from_256(222), inter_from_256(83), alpha], # yellow hex=FFDE53
        # Services and Utility (SVU) # 7 Ub LUC
        [inter_from_256(253), inter_from_256(234), inter_from_256(154), alpha], # lemon yellow  hex=FDEA9A 
        # Grey  # Road (RD) # 8 Ub LUC 
        [inter_from_256(193), inter_from_256(191), inter_from_256(182), alpha], # grey hex=C1BFB6
        # Transport (TR) # 9 Ub LUC 
        [inter_from_256(93), inter_from_256(89), inter_from_256(74), alpha],  # dark grey hex=5D594A
        # Parks and Gardens (PG) # 10 Ub LUC
        [inter_from_256(196), inter_from_256(207), inter_from_256(91), alpha],  # olive green hex=C4CF5B
        # [inter_from_256(111), inter_from_256(158), inter_from_256(158), alpha],   # Mint green hex=6F9E9E 
        # Reserves and Floodways (REF) # 11 Ub LUC
        [inter_from_256(40), inter_from_256(105), inter_from_256(105), alpha],  # aquamarin hex=286969
        # Undeveloped (UND) # 12 Ub LUC
        [inter_from_256(255), inter_from_256(255), inter_from_256(255), alpha],  # White hex=FFFFFF
        # Unclassified (NA) # 13 Ub LUC 
        [inter_from_256(255), inter_from_256(221), inter_from_256(182), alpha],  # beige hex=FFDDB6
        # Water (WAT) # 14 Ub LUC 
        [inter_from_256(118), inter_from_256(137), inter_from_256(167), alpha],  # Blue hex=7689A7
        # Forest (FOR) # 15 Ub LUC 
        [inter_from_256(3), inter_from_256(56), inter_from_256(56), alpha],  # emerald green hex=033838
        # Agriculture (ARG) # 16 Ub LUC 
        [inter_from_256(19), inter_from_256(82), inter_from_256(82), alpha],  # Mint green hex=135252
    
                  ]

    # RGB codes to create blue to red color ramp
    new_cmap = colors.ListedColormap(colors= colors_dict,
                                    name=name)
    return new_cmap


def create_continuous_color_ramp(colors_dict: list = None, name: str = "new_cmap"):
    """ Create a listed or continuous color ramp"""
    if not colors:
        # RGB codes to create blue to red color ramp
        colors_dict = {
            'red': ((0.0, inter_from_256(64), inter_from_256(64)),  # Blue
                    (1 / 5 * 1, inter_from_256(112), inter_from_256(112)),  # Mint green
                    (1 / 5 * 2, inter_from_256(230), inter_from_256(230)),  # Yellow
                    (1 / 5 * 3, inter_from_256(253), inter_from_256(253)),  # Light gold
                    (1 / 5 * 4, inter_from_256(244), inter_from_256(244)),  # Orange
                    (1.0, inter_from_256(169), inter_from_256(169)),  # Red
                    (1.0, inter_from_256(100), inter_from_256(100)),  # Grey
                    (1.0, inter_from_256(0), inter_from_256(0)),  # Black
                    (1.0, inter_from_256(255), inter_from_256(255))),  # White

            'green': ((0.0, inter_from_256(57), inter_from_256(57)),  # Blue
                      (1 / 5 * 1, inter_from_256(198), inter_from_256(198)),  # Mint green
                      (1 / 5 * 2, inter_from_256(241), inter_from_256(241)),  # Yellow
                      (1 / 5 * 3, inter_from_256(219), inter_from_256(219)),  # Light gold
                      (1 / 5 * 4, inter_from_256(109), inter_from_256(109)),  # Orange
                      (1.0, inter_from_256(23), inter_from_256(23)),  # Red
                      (1.0, inter_from_256(100), inter_from_256(100)),  # Grey
                      (1.0, inter_from_256(0), inter_from_256(0)),  # Black
                      (1.0, inter_from_256(255), inter_from_256(255))),  # White

            'blue': ((0.0, inter_from_256(144), inter_from_256(144)),  # Blue
                     (1 / 5 * 1, inter_from_256(162), inter_from_256(162)),  # Mint green
                     (1 / 5 * 2, inter_from_256(246), inter_from_256(146)),  # Yellow
                     (1 / 5 * 3, inter_from_256(127), inter_from_256(127)),  # Light gold
                     (1 / 5 * 4, inter_from_256(69), inter_from_256(69)),  # Orange
                     (1.0, inter_from_256(69), inter_from_256(69)),  # Red
                     (1.0, inter_from_256(100), inter_from_256(100)),  # Grey
                     (1.0, inter_from_256(0), inter_from_256(0)),  # Black
                     (1.0, inter_from_256(250), inter_from_256(255))),  # White
        }
    new_cmap = colors.LinearSegmentedColormap(name=name, segmentdata=colors_dict)

    return new_cmap
#endregion


#region Color Ramps - Topology
cmap_lake = truncate_colormap(cmap=pl.cm.Greys, minval=0.2, maxval=0.2)
cmap_river = truncate_colormap(cmap=pl.cm.Greys, minval=0.2, maxval=0.2)
cmap_earth = truncate_colormap(cmap=pl.cm.gist_earth, minval=0.2)
cmap_feature1 = truncate_colormap(cmap=pl.cm.YlOrRd, minval=0.0, maxval=0.2)
# cmap_grey = truncate_colormap(cmap=pl.cm.Greys, minval=0.2, maxval=0.5)
cmap_grey = truncate_colormap(cmap=pl.cm.Greys, minval=0.4, maxval=1)
# cmap_grey = truncate_colormap(pl.cm.Greys, minval=0.2)
cmap_sink = truncate_colormap(pl.cm.Reds, minval=0.6)

cmap_has_ww= truncate_colormap(cmap=pl.cm.YlOrRd, minval=0.2, maxval=0.5)
cmap_pop = truncate_colormap(cmap=pl.cm.Reds, minval=0.2, maxval=0.7)
cmap_luc = truncate_colormap(cmap=pl.cm.YlOrBr, minval=0.2, maxval=0.7) # [TODO]

cmap_flow = truncate_colormap(cmap=pl.cm.Blues, minval=0.4, maxval=1.0)
cmap_age = truncate_colormap(cmap=pl.cm.magma_r, minval=0.4, maxval=1.0)

#from seaborn
new_cmap =sns.color_palette("ch:start=.2,rot=-.3", as_cmap=True)
cmap_pop = sns.color_palette("dark:salmon_r", as_cmap=True)
cmap_luc = create_categorical_color_ramp(name="cmap_luc")
cmap_flare = sns.color_palette("flare", as_cmap=True)
cmap_cool_warm = sns.color_palette("coolwarm", as_cmap=True)
cmap_blue_orange = sns.diverging_palette(250, 30, l=65, center="dark", as_cmap=True)
cmap_greens_r = sns.cubehelix_palette(start=2, rot=0, dark=0, light=.95, reverse=True, as_cmap=True)
cmap_rocket = sns.color_palette("rocket", as_cmap=True)
cmap_water =sns.color_palette("ch:start=.2,rot=-.3", as_cmap=True)
#endregion

def get_categories(data_dict:dict, feature_name:str):

    # get all the categories along the years
    if data_dict is not None:
        # verbose_("Plotting Pipes...", verbose)

        # define categories along the years 
        categories = np.array([])
        
        for yr in data_dict.keys():
            data_df = data_dict[yr]
            unique_vals = pd.unique(data_df[feature_name].values.ravel('K'))  # get unique values and drop the first value which is 0
            
            if unique_vals[0] == 0.0:
                unique_vals = unique_vals[1:]

            categories = np.append(categories, unique_vals)
            categories = np.unique(categories)

    if len(categories)==0:
        categories = [0]
        
    return list(categories)

def get_min_max_values (values:list):

    vmin= 999999
    vmax= -99999
    if type(values[0]) is not str: 

        vmin = min(vmin, min(values))
        vmax = max(vmax, max(values))

    else:
        vmin = None
        vmax = None

    return vmin, vmax

def plot_blocks_and_pipes_features(blocks: pd.DataFrame, pipes=None,
                                   columns=["has_ww"], label=None, cmaps=[pl.cm.YlOrRd],
                                   columns_p=["BlockID"], labels_p=["BlockID"], cmap_p=[pl.cm.Greys_r], title="",
                                   module:str="", connectivity:str="", case_study:str="", year:int=None, 
                                   plot=True, verbose=False):
    """
    :param: blocks  : dataframe of blocks
    :param: columns : list of columns to plot in individual axes

    :return: True
    """
    blocks.replace(0.0,np.nan)

    if not plot:
        return None

    if len(cmaps) != len(columns):
        verbose_(
            "When plotting multiple features, please specify an ordered list of color maps 'cmaps' to be use for each feature. The cmaps list order should correspond to the order of columns to plot.")
        return False

    if len(columns) == 1 and all(blocks[columns[0]] == 0):
        verbose_(f"There are no blocks with the feature {columns[0]}")
        return True

    fig, ax = plt.subplots(1, 1, figsize=(20, 20))
    plt.rcParams.update({'font.size': 20})

    for i, column in enumerate(columns):

        cmap = cmaps[i]

        if all(blocks[column] == 0):
            continue

        # Binary variable
        elif any(blocks[column].replace(0, np.nan) == 1):

            blocks.replace(0, np.nan).plot(column, ax=ax, categorical=True, cmap=cmap, edgecolor='grey', alpha=0.5,
                                           legend=False,
                                           legend_kwds={"fmt": "{:.0f}", 'orientation': 'horizontal', 'shrink': 0.50,
                                                        'anchor': (0.5, 2.0)},
                                           # missing_kwds={'alpha': 0.0},
                                           )
            if label is not None:
                # Annotation
                blocks.replace(0, np.nan).apply(lambda x: ax.annotate(text=round(x[label], 4),
                                                                      xy=x.geometry.centroid.coords[0], color="black",
                                                                      ha='center', fontsize=7), axis=1)
                if "BlockID" in  blocks.columns:                                                         
                    blocks.replace(0,np.nan).apply(lambda x: ax.annotate(text=round(x["BlockID"], 5),
                                                                          xy=x.geometry.centroid.coords[0], color="black",
                                                                          ha='top', fontsize=8), axis=1)

        # Continuous variable
        elif all(blocks[column].replace(0, np.nan) != 1):

            blocks.replace(0, np.nan).plot(column, ax=ax, cmap=cmap, edgecolor='grey', alpha=0.9, legend=True,
                                           # legend_kwds={"fmt": "{:.0f}", 'orientation' : 'horizontal', 'shrink': 0.50, 'anchor':(0.5, 2.0)},
                                           # missing_kwds={'alpha': 0.0},
                                           )
            ax.set_label(label)

    # Plot Pipes
    if pipes is not None:
        for j, column in enumerate(columns_p):
            cmap = cmap_p[j]

            pipes.replace(0, np.nan).plot(column, ax=ax, cmap=cmap, linewidth=1.5, alpha=1, legend=True,
                                          # legend_kwds={"fmt": "{:.0f}", 'orientation' : 'horizontal', 'shrink': 0.50, 'anchor':(0.5, 2.0)},
                                          # missing_kwds={'alpha': 0.0},
                                          )
            # ax.set_label(labels_p)

    ax.axis("off")

    if title:
        ax.set_title(title, fontdict={'fontsize': '25', 'fontweight': '5'})

    blocks.plot(ax=ax, color="none", edgecolor='grey')

    # plt.legend()
    path = fr"C:\Users\duquevna\Dropbox\010_PhD\01_Python\SND_Manual\Data\data\{case_study}\output\figures"
    if module.lower().__contains__("test"):
        path =fr"tests\00_testing_data\{case_study}\figures"
    
    file_path = set_file_path(path, module=module, connectivity=connectivity, case_study=case_study,
                              name=f"(blocks_{columns}__pipes_{columns_p})", year=year)
    format = 'jpeg'
    fig.savefig(f"{file_path}.{format}", dpi=300, format=format,
                # metadata=None,
                bbox_inches=None, pad_inches=0.1,
                facecolor='auto', edgecolor='auto',
                backend=None,
                # **kwargs,
                )
    fig.show()

    return True


def plot_development(poly_dict: dict, lineStr_dict: dict = None, title:str="", 
                     feature_poly: str = "", units_poly: str = '', label_poly:str="", cmap_poly=pl.cm.YlOrRd,
                     feature_lineStr: str = "", units_lineStr: str = '', label_lineStr:str="", cmap_lineStr=pl.cm.Greys_r,
                     module:str="", connectivity:str="", case_study:str="",
                     backgroud_layers=[], cmap_background_layers=[], 
                     path:Path="", 
                     plot: bool = True, verbose: bool = False):
    """"Plot the delevopment over time of a specific feature
    """
    if not plot:
        return None
    
    years = list(poly_dict.keys())

    # Start building the figure for each year 
    fig, axs = plt.subplots(1, len(years), figsize=cm2inch(90, 180))
    plt.rcParams.update({'font.size': 20})
    
    plt.title(f"Development of {feature_poly} \nand infrastructure {feature_lineStr}")
    
    
    # create an empty handle
    empty_handle = Line2D([], [], linestyle='none', alpha=0)
    
    # create list for custom made handles and labels
    handles_poly=[empty_handle]
    labels_poly = [f"\n{feature_poly}"]
    
    handles_lineStr= [empty_handle]
    labels_lineStr = [f"\n{feature_lineStr}"]

    for i, year in enumerate(years):

        # The number of subplots should be the same as the amount of years to plot
        if len(years)== 1:
            ax = axs
        else:
            ax = axs[i]
        
        ax.set_title(f"{year}")
        ax.axis("off")

        ##################################################################################
        ##############################   POLYGONS  #######################################
        ##################################################################################
        if poly_dict:
            verbose_(f"\n--------{year} ---------- \nPlotting polygons feature: '{feature_poly}' ...")
            

           
            df_polygons = poly_dict[year]

            # Calculate the centroid of each polygon and create a new GeoDataFrame with the points
            df_points = df_polygons[[feature_poly, 'geometry']].copy()
            df_points['geometry'] = df_points.centroid

            df_polygons.replace(0, np.nan).plot(ax=ax, color="none", edgecolor='lightgrey')
            # df_polygons.replace(0, np.nan).plot(column="HasLake", ax=ax, cmap=truncate_colormap(cmap=pl.cm.Greys, minval=0.2, maxval=0.2), edgecolor='none')
            
            for idx, layer in enumerate(backgroud_layers):
                
                cmap_layer = cmap_background_layers[idx]
                base_map = df_polygons.replace(0, np.nan).plot(column=layer, ax=ax, alpha=0.5, 
                                                               cmap=truncate_colormap(cmap=cmap_layer, minval=0.2, maxval=0.2), 
                                                               edgecolor='none',
                                                               label = layer,
                                                               )
                
            hd, lb = ax.get_legend_handles_labels()
                
            if hd not in handles_poly or lb not in labels_poly:    
                handles_poly.extend(hd)
                labels_poly.extend(lb)
            
            
            handles_poly.append(empty_handle)
            labels_poly.append(f"\n{feature_poly}")
            
            
            if feature_poly in poly_dict[year].columns:
                
                # Define categories
                categories_poly = get_categories(poly_dict, feature_name=feature_poly)
                vmin_poly, vmax_poly = get_min_max_values(categories_poly)
                verbose_(f"{len(categories_poly)} categories for the polygons: vmin {vmin_poly} vmax {vmax_poly} ...", verbose)

                if lineStr_dict is None:
                    alpha = 1
                else:
                    alpha = 1
                
                ## Ploting QUANTITATIVE Data 
                if not type(categories_poly[0]) is str:
                    
                    # check that feature data is not empty
                    if all(df_polygons[feature_poly]==0):
                        pass
                    
                    # Binary data (0,1)
                    elif sum(categories_poly) == 1:
                        verbose_("Binary variable", verbose)
                        alpha = 1

                        df_polygons[feature_poly].replace(1, f'{feature_poly}', inplace=True)
                        df_points[feature_poly].replace(1, f'{feature_poly}', inplace=True)

                        # Create color_palette
                        color_palette_poly = {0: 'none',
                                            f"{feature_poly}": cmap_poly(0.5)} 

                        # Plot data
                        # Loop through each attribute type and plot it using the colors assigned in the dictionary
                        for __category_, point_data in df_points.groupby(f'{feature_poly}'):
                            
                            # Define the color for each group using the dictionary
                            color = color_palette_poly[__category_]
                            
                            # Plot each group using the color defined above
                            point_data.plot(color=color, edgecolor='none',
                                    ax=ax,
                                    label=__category_,
                                    alpha=alpha)
                            
                        # Loop through each attribute type and plot it using the colors assigned in the dictionary
                        for __category_, poly_data in df_polygons.groupby(f'{feature_poly}'):
                            
                            # Define the color for each group using the dictionary
                            color = color_palette_poly[__category_]
                            
                            # Plot each group using the color defined above
                            poly_data.plot(color=color, edgecolor='none',
                                            ax=ax,
                                            label=__category_,
                                            alpha=alpha)

                                                    
                        df_polygons[feature_poly].replace(f'{feature_poly}', 1, inplace=True)
                        df_points[feature_poly].replace(f'{feature_poly}', 1, inplace=True)
                    
                    # Categorical data (5 categories)
                    elif len(categories_poly)<=5:
                        
                        verbose_("Categorical variable (up to 5)", verbose)
                        color_palette_poly = dict()
                        bins = list()

                        # Create color_palette and bins to categorize
                        for _i_, cat in enumerate(categories_poly):
                            if cat == 0:
                                color_palette_poly[cat] = 'none'
                                bins.append(cat)
                            else:
                                color_palette_poly[cat] = cmap_poly(_i_/(len(categories_poly)))
                                bins.append(cat)
                        
                        # Loop through each attribute type and plot it using the colors assigned in the dictionary
                        for __category_, point_data in df_points.replace(0,np.nan).groupby(f'{feature_poly}'):
                            
                            # Define the color for each group using the dictionary
                            color = color_palette_poly[__category_]
                            
                            # Plot each group using the color defined above
                            point_data.plot(color=color, edgecolor='none',
                                    ax=ax,
                                    label=__category_,
                                    alpha=alpha,
                                    vmin=vmin_poly, vmax=vmax_poly,
                                    scheme="User_Defined",
                                    classification_kwds=dict(bins=bins))
                            
                        # Loop through each attribute type and plot it using the colors assigned in the dictionary
                        for __category_, poly_data in df_polygons.replace(0,np.nan).groupby(f'{feature_poly}'):
                            
                            # Define the color for each group using the dictionary
                            color = color_palette_poly[__category_]
                            
                            # Plot each group using the color defined above
                            poly_data.plot(color=color, edgecolor='none',
                                    ax=ax,
                                    label=__category_,
                                    alpha=alpha,
                                    vmin=vmin_poly, vmax=vmax_poly,
                                    scheme="User_Defined",
                                    classification_kwds=dict(bins=bins))
                    
                    # Continuos data categorized by Quantiles 
                    else:
                        
                        verbose_("Continuous variable", verbose)
                        
                        # create bins to categorize 
                        k_quantiles= 7
                        bins = mapclassify.Quantiles(categories_poly, k=k_quantiles).bins 
                        bins = np.append(np.array(0),bins)
                                   

                        # Create color_palette
                        color_palette_poly = dict()
                        for _i_, cat in enumerate(bins):
                            if cat == 0:
                                color_palette_poly[cat] = 'none'
                            else:                       
                                color_palette_poly[cat] = cmap_poly(_i_/(len(bins)-1))
                            

                        # Change the values in the DataFrame to have only k values = number of bins
                        points_temp = df_points[[feature_poly, 'geometry']].copy()
                    
                        for _i_, point in points_temp.iterrows():
                            val = df_points.loc[_i_, feature_poly]
                            label = bins[np.where(val <= bins)[0][0]]
                            points_temp.loc[_i_, feature_poly] = label
                    
                        
                        # Loop through each attribute type and plot it using the colors assigned in the dictionary
                        for __category_, point_data in points_temp.groupby(f'{feature_poly}'):
                            
                            # Define the color for each group using the dictionary
                        
                            label = bins[np.where(__category_ <= bins)[0][0]]
                            # print(label)

                            color = color_palette_poly[label]
                            
                            # Plot each group using the color defined above
                            point_data.plot(color= color, 
                                    edgecolor='none',
                                    ax=ax,
                                    label = f"< {round(label,4)}",
                                    alpha=alpha,
                                    vmin=vmin_poly, vmax=vmax_poly,
                                    scheme="User_Defined",
                                    classification_kwds=dict(bins=bins)
                                    )  

                        # Change the values in the DataFrame to have only k values = number of bins
                        blks_temp = df_polygons[[feature_poly, 'geometry']].copy()
                    
                        for _i_, block in blks_temp.iterrows():
                            val = df_polygons.loc[_i_, feature_poly]
                            label = bins[np.where(val <= bins)[0][0]]
                            blks_temp.loc[_i_, feature_poly] = label

                        # Loop through each attribute type and plot it using the colors assigned in the dictionary
                        for __category_, poly_data in blks_temp.groupby(f'{feature_poly}'):
                            
                            # Define the color for each group using the dictionary
                        
                            label = bins[np.where(__category_ <= bins)[0][0]]
                            # print(label)

                            color = color_palette_poly[label]
                            
                            # Plot each group using the color defined above
                            poly_data.plot(color= color, 
                                    edgecolor='none',
                                    ax=ax,
                                    label = f"< {round(label,4)}",
                                    alpha=alpha,
                                    vmin=vmin_poly, vmax=vmax_poly,
                                    scheme="User_Defined",
                                    classification_kwds=dict(bins=bins)
                                    )  

                ## Ploting QUALITATIVE Data
                else:
                    verbose_("Categorical variable: Blocks", verbose)
                    color_palette_poly = dict()
                    bins = list()
                    for _i_, cat in enumerate(categories_poly):
                        if cat == 0:
                            color_palette_poly[cat] = 'none'
                            bins.append(cat)
                        else:
                            color_palette_poly[cat] = cmap_poly(_i_/(len(categories_poly)-1))
                            bins.append(cat)

                    # Loop through each attribute type and plot it using the colors assigned in the dictionary
                    for __category_, point_data in df_points.replace(0,np.nan).groupby(f'{feature_poly}'):
                        
                        # Define the color for each group using the dictionary
                        color = color_palette_poly[__category_]
                        
                        # Plot each group using the color defined above
                        point_data.plot(color=color, edgecolor='none',
                                ax=ax,
                                label=__category_,
                                scheme="User_Defined",
                                classification_kwds=dict(bins=bins)) 

                    # Loop through each attribute type and plot it using the colors assigned in the dictionary
                    for __category_, poly_data in df_polygons.replace(0,np.nan).groupby(f'{feature_poly}'):
                        
                        # Define the color for each group using the dictionary
                        color = color_palette_poly[__category_]
                        
                        # Plot each group using the color defined above
                        poly_data.plot(color=color, edgecolor='none',
                                ax=ax,
                                label=__category_,
                                scheme="User_Defined",
                                classification_kwds=dict(bins=bins)) 
            
                
                
                # add fake handle for the labelof the group
                # fake_handle = mpatches.Patch(color='white')
                if label_poly not in labels_poly:    
                   handles_poly.append("label_poly")
                   labels_poly.append(empty_handle)
                
                # add all handles to the legend
                hd, lb = ax.get_legend_handles_labels()
                if lb not in labels_poly:    
                   handles_poly.extend(hd)
                   labels_poly.extend(lb)
                    
            else:
                verbose_(f" {feature_poly} is not in the Polygons information")

            if label_poly in poly_dict[year].columns:
                df_polygons.apply(lambda x: ax.annotate(text=round(x[label_poly],2), xy=x.geometry.centroid.coords[0], color="black", ha='center'), axis=1)

            print("HANDLES AND LABELS")
            print(handles_poly, labels_poly)
            print("----------EN OF HANDLES-----------")
        
        ##################################################################################
        ##############################   LINESTRINGS   ###################################
        ##################################################################################
        
        if lineStr_dict:
            verbose_(f"\nPlotting pipes '{feature_lineStr}' ...")

            # geoDataFrame 
            df_lineStr = lineStr_dict[year]

            if feature_lineStr in df_lineStr.columns:
                
                # Define categories
                categories_lineStr = get_categories(lineStr_dict, feature_name=feature_lineStr)
                vmin_lineStr, vmax_lineStr = get_min_max_values(categories_lineStr)
                verbose_(f"{len(categories_lineStr)} Pipes Categories : vmin {vmin_lineStr} vmax {vmax_lineStr} ...",verbose)

                str_variable_p = ""
                No_clases_p = len(categories_lineStr)

                if  type(categories_lineStr[0]) is not str:
                    
                    # Check that the column feature is not empty
                    if all(df_lineStr[feature_lineStr]==0) :
                        pass
                    
                    # Binary variables (0, 1)
                    elif sum(categories_lineStr) == 1 :
                        verbose_("Binary variable", verbose)
                        alpha = 1

                        # Rename data for the legend
                        df_lineStr[feature_lineStr].replace(1, f'{feature_lineStr}', inplace=True)
                        df_lineStr[feature_lineStr].replace(0, f'Non_{feature_lineStr}', inplace=True)
                        
                        # Create color palette dictionary
                        color_palette_pipes = {
                                               f'Non_{feature_lineStr}': "black",
                                                #    0: 'none',
                                                f"{feature_lineStr}": "coral"} 

                        # Plot data
                        # Loop through each group of values and plot it using the colors assigned in the dictionary                
                        for __category_, pipe_data in df_lineStr.groupby(f'{feature_lineStr}'):
                            
                            # Define the color for each group using the dictionary
                            color = color_palette_pipes[__category_]
                            
                            # Plot each group using the color defined above
                            pipe_data.plot(ax=ax,
                                           color=color, 
                                           edgecolor='none',                                   
                                           label=__category_)
                        
                        # Rename data to original binary values
                        df_lineStr[feature_lineStr].replace(f'{feature_lineStr}', 1, inplace=True)
                        df_lineStr[feature_lineStr].replace(f'Non_{feature_lineStr}', 0, inplace=True)


                    elif len(categories_lineStr)<=5:
                        
                        verbose_("Continuous variable (up to 5 categories): Pipes", verbose)
                        color_palette_pipes = dict()
                        bins = list()

                        # Create color palette dictionary and bins to categorize the values 
                        for _i_, cat in enumerate(categories_lineStr):
                            if cat == 0:
                                color_palette_pipes[cat] = 'none'
                                bins.append(cat)
                            else:
                                color_palette_pipes[cat] = cmap_lineStr(_i_/(len(categories_lineStr)))
                                bins.append(cat)
                        
                        # Loop through each attribute type and plot it using the colors assigned in the dictionary
                        for __category_, pipe_data in df_lineStr.groupby(f'{feature_lineStr}'):
                            
                            # Define the color for each group using the dictionary
                            color = color_palette_pipes[__category_]
                            
                            # Plot each group using the color defined above
                            pipe_data.plot(color=color, edgecolor='none',
                                    ax=ax,
                                    label=__category_,
                                    vmin=vmin_lineStr, vmax=vmax_lineStr,
                                    scheme="User_Defined",
                                    classification_kwds=dict(bins=bins))
                    else:
                        
                        verbose_("Continuous variable: Pipes", verbose)
                        
                        # Create bins to categorize the values 
                        k_quantiles= 7
                        bins = mapclassify.Quantiles(categories_lineStr, k=k_quantiles).bins
                                            
                        # Create color palette dictionary 
                        color_palette_pipes = dict()
                        for _i_, cat in enumerate(bins):
                            color_palette_pipes[cat] = cmap_lineStr(_i_/(len(bins)-1))
                            

                        # Change the values in the DataFrame to have only k values = number of bins
                        pipes_temp = df_lineStr[[feature_lineStr, 'geometry']].copy()
                        # print(pipes_temp.geometry)
                        for _i_, pipe in pipes_temp.iterrows():
                            val = df_lineStr.loc[_i_, feature_lineStr]
                            label = bins[np.where(val <= bins)[0][0]]
                            pipes_temp.loc[_i_, feature_lineStr] = label
                        # print(pipes_temp)
                        

                        # Loop through each attribute type and plot it using the colors assigned in the dictionary
                        for __category_, pipe_data in pipes_temp.groupby(f'{feature_lineStr}'):
                            
                            # Define the color for each group using the dictionary
                        
                            label = bins[np.where(__category_ <= bins)[0][0]]
                            # print(label)

                            color = color_palette_pipes[label]
                            
                            # Plot each group using the color defined above
                            pipe_data.plot(color= color, 
                                    # edgecolor='none',
                                    ax=ax,
                                    label = f"< {round(label,4)}",
                                    vmin=vmin_lineStr, vmax=vmax_lineStr,
                                    scheme="User_Defined",
                                    classification_kwds=dict(bins=bins)
                                    )                   
                else:
                    print("Categorical variable: Pipes")
                    color_palette_pipes = dict()
                    bins = list()

                    # Create color palette dictionary and bins to categorize the values                     
                    for _i_, cat in enumerate(categories_lineStr):
                        if cat == 0:
                            color_palette_pipes[cat] = 'none'
                            bins.append(cat)
                        else:
                            color_palette_pipes[cat] = cmap_lineStr(_i_/(len(categories_lineStr)-1))
                            bins.append(cat)
                        
                    # Loop through each category and plot it using the colors assigned in the dictionary
                    for __category_, pipe_data in df_lineStr.groupby(f'{feature_lineStr}'):
                        
                        # Define the color for each group using the dictionary
                        color = color_palette_pipes[__category_]
                        
                        # Plot each group using the color defined above
                        pipe_data.plot(ax=ax,
                                        color=color, edgecolor='none',
                                        label=__category_,
                                        scheme="User_Defined",
                                        classification_kwds=dict(bins=bins))

                # add fake handle for the labelof the group
                # fake_handle = mpatches.Patch(color='white')
                if label_lineStr not in labels_lineStr:    
                   handles_lineStr.append("label_linStr")
                   labels_lineStr.append(empty_handle)
                
                # add all handles to the legend
                hd, lb = ax.get_legend_handles_labels()
                if lb not in labels_lineStr:    
                   handles_lineStr.extend(hd)
                   labels_lineStr.extend(lb)
            else:
                verbose_(f" {feature_lineStr} is not in the Pipes information")
        

        # After ploting the last map draw the north arrow and legend
        if year == years[-1]:            
            # North Arrow
            x, y, arrow_length = 0.9, 1.0, 0.07
            ax.annotate('N', xy=(x+0.5, y), xytext=(x+0.5, y - arrow_length),
                        arrowprops=dict(facecolor='black', width=3, headwidth=8),
                        ha='center', va='center', fontsize=18,
                        xycoords=ax.transAxes)

            # Create the legend
            
            # example lists
            if type(any(labels_poly)) is not str: 
                handles_poly, labels_poly = sorted_tuples(non_numeric_list= handles_poly, numeric_list=labels_poly)
            
            a, b = ax.get_legend_handles_labels()
            
            print(a, b)
            
            ax.legend(handles= a,
                      labels=b,
                      bbox_to_anchor=(1.0, .5),
                      fontsize=15,
                      frameon=False,
                      loc=('center left'),
                      title="Legend")
                  
           
    file_path = set_file_path(path=path/"figures", module=module, connectivity=connectivity, case_study=case_study,
                            name=f"(blocks_{feature_poly}__pipes_{feature_lineStr})", year=year)
    format = 'svg'
    fig.savefig(f"{file_path}.{format}", dpi=600, format=format,
                # metadata=None,
                bbox_inches=None, pad_inches=0.1,
                facecolor='auto', edgecolor='auto',
                backend=None,
                # **kwargs,
                )
        

    return None


def plot_with_labels (blocks, field_a="Active", field_b="Active",  label="blk_elev"):
    fig, ax = plt.subplots(figsize = (30,30))

    blocks.plot(ax=ax, edgecolor="grey", alpha = 0.2)
    blocks.apply(lambda x: ax.annotate(text=round(x[label],2), xy=x.geometry.centroid.coords[0], color="black", ha='center', fontsize=9), axis=1)

    sinks = blocks[blocks[field_a]==1]
    if any(sinks):
        sinks.replace(0, np.nan).plot(field_a, ax=ax, cmap=truncate_colormap(pl.cm.Reds_r, minval=0.5))
    blocks.replace(0, np.nan).plot(field_b, ax=ax, cmap=truncate_colormap(pl.cm.PuBu, minval=0.6, maxval=0.9))
    # sinks.apply(lambda x: ax.annotate(text=round(x['ModAvgElev'],2), xy=x.geometry.centroid.coords[0], ha='center'), axis=1)
    ax.axis("off")
    return True


def population_growth(blocks_dict, year_i=2018, rate=0.7, convert_population=False):
    if not convert_population:
        return True

    columns_UB = ['Population', 'Total_jobs', 'Blk_WD', 'Blk_WW']
    year_i = list(blocks_dict.keys())[0]
    print(year_i)

    for yr in blocks_dict.keys():
        blocks = blocks_dict[yr]
        print(rate)
        for c in columns_UB:
            po = blocks.loc[:, c]
            print(sum(blocks.loc[:, c]))

            blocks.loc[:, c] = blocks_dict[year_i].loc[:, c] * pow((1 + rate / 100), (yr - year_i))

            print(sum(blocks.loc[:, c]))
        # blocks_dict[yr] = blocks.copy()
    return True