#!/bin/bash 
#SBATCH --time=340:00:00
#SBATCH --mem-per-cpu=64000mb
#SBATCH --job-name=comb_inp
#SBATCH --output=tryone.out
#SBATCH --error=tryone.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=natalia.duquevillarreal@eawag.ch

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

module load gcc/8.2.0
module load python/3.9.9

python main.py
