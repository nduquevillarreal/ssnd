"""
@file
@author Fabrizia Fappiano
@section LICENSE

Sequenced Sewer Networks Design (SSND)

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from pyswmm import Links, Nodes, Simulation


class swmm(object):
    def __init__(self):
        self.name = "swmm"
        self.simulated = False

        # Links information
        self.flows = {}
        self.depths = {}
        self.lengths = {}
        self.diams = {}
        self.head_loss = {}

        ## Nodes information
        self.node_floods = {}

    def simulation(self):
        with Simulation(
            r"C:\Users\fabrizia\Downloads\inp_generator\inp_generator\.57km2_Real_fixed_pump_depths_AF.inp"
        ) as sim:
            for step in sim:
                for link in Links(sim):
                    if link.linkid not in self.flows.keys():
                        self.flows[link.linkid] = [link.flow]
                        self.depths[link.linkid] = [link.depth]
                        self.head_loss[link.linkid] = [link.average_head_loss]
                    self.flows[link.linkid].append(link.flow)
                    self.depths[link.linkid].append(link.depth)
                    self.head_loss[link.linkid] = [link.average_head_loss]

                for node in Nodes(sim):
                    self.diams[node.nodeid] = [node.full_depth]
                    if node.nodeid not in self.node_floods.keys():
                        self.node_floods[node.nodeid] = [node.flooding]
                    self.node_floods[node.nodeid].append(node.flooding)

            # print(self.flows)
            # print(self.diams)
            print(self.depths)
            # print(self.head_loss)
            # print(self.lengths)

        self.simulated = True

    # def get_velocity(self):
    # vel = {}
    # for key, values in self.flows.items():
    # vel[key] = np.array(self.flows[key])/np.array(self.depths[key])

    # print("for link  {} \n {}".format((key,vel[key])))

    # velocity_df = pd.DataFrame.from_dict(vel, orient='index')

    def run_analysis(self):
        self.simulation()


s = swmm()
s.run_analysis()
