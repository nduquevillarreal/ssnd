# %% [markdown]
# # Hydraulic Design
# 
# """
# @file   md_infrastructure.py
# @author  Natalia Duque <natalia.duquevillarreal@eawag.ch>
# @section LICENSE
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# """
# 
# __author__ = "Natalia Duque"
# __copyright__ = "Copyright 2018."

# %% [markdown]
# ## Step 0. Import python packages

# %%
# region --- PYTHON LIBRARY IMPORTS ---
import time
from math import *
from pathlib import Path
from random import randint
import numpy as np
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import matplotlib.colors as colors
# --- Spatial geometry ---
from shapely.geometry import Point, Polygon, LineString

# --- SND IMPORTS ---
# from help_functions.helper_functions import verbose_, cm2inch, truncate_colormap, set_file_name
from ssnd.algorithm import *
from ssnd.defaults import *
from ssnd.help_functions.helper_functions import *
from ssnd.help_functions.plot_maps import *
from ssnd.deterioration import *
#endregion 

module="hydraulic_design"
read_module = "topology"

# %% [markdown]
# ## Step 1 Initialization

# %%
# Input parameters
read_path = DATA_DIR/case_study/"output/shp"         # read from previous modules 
output_path = DATA_DIR /case_study/"output"

# %% [markdown]
# duplicate columns with names for HD
# %%
def duplicate_columns_with_new_name(df:gpd.GeoDataFrame, columns:list, new_columns:list):
    if df is not None:
        for i,  col in enumerate(columns):
            new_col = new_columns[i]
            df[new_col] = df[col]
        
    return df.copy()

# %% [markdown]
# Create new columns in dataframe
# %%
def df_with_new_fields (df:pd.DataFrame, module:str="topology", verbose:bool=False):
    """ Initialize new columns for current module in the Dataframe     

    """

    if module.lower().__contains__("topo"):
        columns = ["dem_sink",
                "blk_elev",
                "edge",         # binary: 1 if the block is on the edge (boundary) of the case study 
                "carved",       # binary: 1 if the block has been carved
                "has_ww",       # binary: 1 if the block has sewer requirements
                "has_infra",     # binary: 1 if the block already has infrastructure
                "has_wwtp",      # binary: 1 if the block has a WWTP
                "id_up",        # int: upstream ID
                "id_down",      # int: downstream ID
                "has_outlet",   # binary: 1 if the block has a an outlet (sink) of a sewersubcatchment
                "link_type",    # binary: 1 for external pipes, 2 for internal pipes
                "type_sewer",   # str: "collection" or "trunk" pipes
                "id_basin",    # int: sewer basin No.
                "horiz_dist",   # float: length of the sewer link
                "elev_drop",    # float: elevation difference of the sewer pipe
                "terr_slope",   # float: terrain slope
                "near_wwtp",    # int: ID of nearest WWTP
                "near_river",   # int: ID of nearest River         
                "ww_inflow",    # float: Flow coming into the Block (upstrem manhole)
                "peak_flow",    # float: Daily peak flow produced in the Block
                "PE",           # Population Equivalents 
                "desg_flow",    # float: Design flow considering the design factor and the peak flow.
                "up_ids",       # str: list of IDs of upstream blocks
                "down_ids",     # str: list of IDs of downstream blocks
                "built_year"    # int: construction year
                ]

    if module.lower().__contains__("hydr"):
        columns = ["diameter",      # pipe diameter [m]
        "slope",        # pipe slope
        "sww_length",    # pipe length
        "fill_ratio",   # filling ration
        "y_max",        # maximum flow depth
        "y_n",           # normal flow depth
        "angle",        # internal angle for circular pipes
        "perimeter",    # wet perimeter
        "top_width",     # top width
        "h_radius",       # hydraulic_radius
        "area",         # wet area
        "velocity",     # flow velocity
        "shear",        # shear stress
        "froude",       # Froude number
        "exact_flow",   # current flow in the pipe
        "max_flow",     # max flow capacity of the pipe
        "min_flow",     # min flow capacity of the pipe
        ]   

        for col in columns:
            df[col] = 0.0
    
    else: 
        return df.copy()

    

    return df.copy()


# %% [markdown]
# Prepare hydraulics for each pipe diameter using maximum filling ratio 
# %%
def prepare_hydraulics_data(diameters=commercial_diameters, v_min=0.75, v_max=10.0, n=0.009, verbose=False):
    """Prepare arrays with the minimum and maximum slope based on the minimum and maximum velocities
    assuming a maximum filling ratio for the each diameter.

    :param diameters: list of commercial diameters in m
    :param v_min: minimum velocity
    :param v_max: maximum velocity
    :param n: Manning's n coefficient
    :param verbose: printing function
    :return: df_df_hydraulics data frame with hydraulic parameters

    """

    # Prepare arrays for the hydraulic calculations
    max_hydraulic_radius = []
    max_areas = []
    min_slopes = []
    max_slopes = []

    # calculating the max filling ratio and the min slopes
    for idx, diameter in enumerate(diameters):
        verbose_(idx, verbose)
        # Hydraulic parameters for the maximum filling ratio of the pipe with a specific diameter
        y = get_max_flow_depth(diameter)
        angle = get_angle(diameter, y)
        max_hydraulic_radius.append(get_hydraulic_radius(diameter, angle))
        max_areas.append(get_area(diameter, angle))
        verbose_("max_hydraulic_radius: {}".format(max_hydraulic_radius), verbose)
        verbose_("max_areas: {}".format(max_areas), verbose)

        # Calculate minimum and maximum slope for the given diameter

        min_slopes.append(round(get_slope(max_hydraulic_radius[-1], v_min, n),6)+1E-6)
        max_slopes.append(round(get_slope(max_hydraulic_radius[-1], v_max, n),6)-1E-6)
        verbose_("min_slopes: {}".format(min_slopes), verbose)
        verbose_("max_slopes: {}".format(max_slopes), verbose)

    dict = {
        "diameter": diameters,
        "min_slopes": min_slopes,
        "max_slopes": max_slopes,
        "max_hydraulic_radius": max_hydraulic_radius,
        "max_areas": max_areas,
    }

    # columns= {"min_slopes", "max_slopes", "max_hydraulic_radius", "max_areas"}
    df_hydraulics = pd.DataFrame.from_dict(dict)
    # list(df_hydraulics["max_hydraulic_radius"])
    verbose_("Hydraulic parameters for each pipe diameter under max filling ratio", verbose)
    verbose_("df_hydraulics \n".format(df_hydraulics), verbose)

    return df_hydraulics


# %% [markdown]
# Find best diameter slope
# %%
def get_diameter_for_max_filling_ratio(pipe,
                                       incoming_diameter_idx,
                                        df_hydraulics,
                                        diameters=commercial_diameters,):
       
    for idx in range(incoming_diameter_idx, len(diameters)):
        diameter: float = diameters[idx]
        
        max_hyd_rad = df_hydraulics.max_hydraulic_radius[idx]
        max_area = df_hydraulics.max_areas[idx]
        min_slope = df_hydraulics.min_slopes[idx]
        max_slope = df_hydraulics.max_slopes[idx]
        
        pipes[flow]

# %%
def recalculate_hydraulics(diameter, slope, design_discharge, n, max_fill, min_fill):
    
    dict_hydraulics = {"fill_ratio":0.0, 
                       "y_n":0.0, 
                       "angle":0.0, 
                       "h_radius":0.0, 
                       "area":0.0, 
                       "velocity":0.0, 
                       "froude":0.0, 
                       "tau":0.0, 
                       "exact_flow":0.0,
                       "max_flow":0.0,
                       "min_flow":0.0}
    
    
    # calculate the real flow depth
    y_n, exact_flow = get_normal_depth(diameter, design_discharge, slope, n)
    fill_ratio = y_n/diameter
    # recalculate geometric parameters
    angle = get_angle(diameter, y_n)
    h_radius = get_hydraulic_radius(diameter, angle)
    area = get_area(diameter, angle)

    # recalculate hydraulic parameters
    velocity = get_velocity(slope, h_radius, n)
    tau = get_tau(slope, h_radius)
    top_width = get_top_width(diameter, angle)
    froude = get_froude(area, velocity, top_width)
    
    # save in dict    
    dict_hydraulics["fill_ratio"] = np.round(fill_ratio, decimals=2)
    dict_hydraulics["y_n"] = np.round(y_n, decimals=4)
    dict_hydraulics["angle"] = np.round(angle, decimals=4)
    dict_hydraulics["h_radius"] = np.round(h_radius, decimals=4)
    dict_hydraulics["area"] = np.round(area, decimals=4)
    dict_hydraulics["velocity"] = np.round(velocity, decimals=4)
    dict_hydraulics["froude"] = np.round(froude, decimals=4)
    dict_hydraulics["tau"] = tau
    dict_hydraulics["exact_flow"] = np.round(exact_flow, decimals=4)
    
    
    
    # recalculate geometric parameters
    yn_max = diameter*max_fill
    angle = get_angle(diameter, yn_max)
    h_radius = get_hydraulic_radius(diameter, angle)
    area = get_area(diameter, angle)
    # recalculate hydraulic parameters
    velocity = get_velocity(slope, h_radius, n)
    max_flow = velocity*area
    dict_hydraulics["max_flow"] = np.round(max_flow, decimals=4)
    
    
    # recalculate geometric parameters
    yn_min = diameter*min_fill
    angle = get_angle(diameter, yn_min)
    h_radius = get_hydraulic_radius(diameter, angle)
    area = get_area(diameter, angle)
    # recalculate hydraulic parameters
    velocity = get_velocity(slope, h_radius, n)
    min_flow = velocity*area
    dict_hydraulics["min_flow"] = np.round(min_flow, decimals=4)
    
    return  dict_hydraulics


# %%
def find_best_diameter_slope(
    pipes,
    blocks,
    df_hydraulics,
    id_up,
    design_discharge,
    ideal_slope,
    incoming_diameter_idx=0,
    v_min=0.75,
    v_max=10.0,
    n=0.009,  # for PVC [Source: Butler and Davies(2011), Urban Drainage]
    roughness=0.00003,  # for PVC [Source: Butler and Davies(2011), Urban Drainage]
    min_discharge=0.000001,
    min_slope_limit=0.0001,
    max_slope_limit=0.1,
    delta_slope=0.0001,
    undercapacity_fill=1, 
    overcapacity_fill=0,
    diameters=commercial_diameters,
    verbose=False,
    count=0,
):
    """Find a diameter-slope feasible combination for each pipe

    :param pipes: dataframe with pipes information [id_up, id_down, type, design-flow]
    :param id_up: ID of the pipe, given by the ID of its upstream manhole
    :param design_discharge: design flow of the pipe to be designed

    :param v_min: minimum flow velocity limit default: 0.75
    :param v_max: maximum flow velocity limit default: 10.0
    :param min_slope_limit: minimum slope limit default: 0.001
    :param max_slope_limit: maximum slope limit default: 0.10
    :param delta_slope: slope change default:0.0001
    :param diameters: list of commercial diameters
                      default:[0.225, 0.25, 0.35, 0.4, 0.5, 0.6, 0.80, 1.0, 1.20, 1.5, 2.0, 2.5, 3.0]
    :param verbose: Boolean, print statements if True

    :return slope: design slope for the pipe
    :return diameter: selected pipe diameter in meters [m]
    """
    checked_hydraulics = False
    
    # for pipes with almost no discharge
    if design_discharge < min_discharge:
        design_discharge = min_discharge * 10  # discharge for a household with 10 people

    # set start slope values at the minimum slope for the following iterations
    if min_slope_limit <= ideal_slope <= max_slope_limit:
        start_slope = ideal_slope
    else:
        start_slope = df_hydraulics.min_slopes[0]

    velocity = v_min

    # max_slope is the minimum slope between the slope limit defined in defaults (e.g. 10%)
    # and the max_slope for the pipe diameter
    max_s = max_slope_limit
    
    block_idx = blocks[blocks["BlockID"]==id_up].index[0] 
    pipe_idx = pipes[pipes["id_up"]==id_up].index[0]
    
    for slope in np.arange(start_slope, max_s, delta_slope):
            
        # loop over diameters starting from the diameter of the previous manhole that is given as input.
        for idx in range(incoming_diameter_idx, len(diameters)):
            diameter: float = diameters[idx]
            

            max_hyd_rad = df_hydraulics.max_hydraulic_radius[idx]
            max_area = df_hydraulics.max_areas[idx]
            min_slope = df_hydraulics.min_slopes[idx]
            max_slope = df_hydraulics.max_slopes[idx]

            # set start slope values at the minimum slope for the following iterations
            if min_slope <= slope <= max_slope:
                # make sure discharge fits through the pipe
                velocity = get_velocity(slope, max_hyd_rad, n)
                if velocity > v_max:
                    continue
            
                max_discharge = max_area * velocity

                if design_discharge < max_discharge:

                    # calculate the real flow depth
                    y_n, exact_flow = get_normal_depth(diameter, design_discharge, slope, n)
                    fill_ratio = y_n / diameter
                    # recalculate geometric parameters
                    angle = get_angle(diameter, y_n)
                    h_radius = get_hydraulic_radius(diameter, angle)
                    area = get_area(diameter, angle)

                    # recalculate hydraulic parameters
                    velocity = get_velocity(slope, h_radius, n)
                    tau = get_tau(slope, h_radius)
                    top_width = get_top_width(diameter, angle)
                    froude = get_froude(area, velocity, top_width)

                    # check hydraulic constraints
                    checked_hydraulics = check_constraints(diameter, velocity, tau, y_n, froude, roughness)

                    if checked_hydraulics:
                        verbose_(f" FOUND IT {id_up} {diameter} {slope}", verbose=verbose)
                        # Add values for the hydraulic design to data frame
                                            
                        dict_hydraulics = recalculate_hydraulics(diameter, slope, design_discharge, n,undercapacity_fill, overcapacity_fill)
    
                        pipes.loc[pipe_idx, "diameter"] = diameter
                        pipes.loc[pipe_idx, "slope"] = slope
                        pipes.loc[pipe_idx, "fill_ratio"] = dict_hydraulics["fill_ratio"]
                        pipes.loc[pipe_idx, "y_n"] = dict_hydraulics["y_n"]
                        pipes.loc[pipe_idx, "angle"] = dict_hydraulics["angle"]
                        pipes.loc[pipe_idx, "h_radius"] = dict_hydraulics["h_radius"]
                        pipes.loc[pipe_idx, "area"] = dict_hydraulics["area"]
                        pipes.loc[pipe_idx, "velocity"] = dict_hydraulics["velocity"]
                        pipes.loc[pipe_idx, "froude"] = dict_hydraulics["froude"]
                        pipes.loc[pipe_idx, "tau"] = dict_hydraulics["tau"]
                        pipes.loc[pipe_idx, "exact_flow"] = dict_hydraulics["exact_flow"]
                        pipes.loc[pipe_idx, "max_flow"] = dict_hydraulics["max_flow"]
                        pipes.loc[pipe_idx, "min_flow"] = dict_hydraulics["min_flow"]
                        
                        blocks.loc[block_idx, "diameter"] = diameter
                        blocks.loc[block_idx, "slope"] = slope
                        blocks.loc[block_idx, "fill_ratio"] = dict_hydraulics["fill_ratio"]
                        blocks.loc[block_idx, "y_n"] = dict_hydraulics["y_n"]
                        blocks.loc[block_idx, "angle"] = dict_hydraulics["angle"]
                        blocks.loc[block_idx, "h_radius"] = dict_hydraulics["h_radius"]
                        blocks.loc[block_idx, "area"] = dict_hydraulics["area"]
                        blocks.loc[block_idx, "velocity"] = dict_hydraulics["velocity"]
                        blocks.loc[block_idx, "froude"] = dict_hydraulics["froude"]
                        blocks.loc[block_idx, "tau"] = dict_hydraulics["tau"]
                        blocks.loc[block_idx, "exact_flow"] = dict_hydraulics["exact_flow"]
                        blocks.loc[block_idx, "max_flow"] = dict_hydraulics["max_flow"]
                        blocks.loc[block_idx, "min_flow"] = dict_hydraulics["min_flow"]
                        
                        
                        return slope, diameter
                
            else:
                continue
                           
    
    while not checked_hydraulics:
        slope: float = ideal_slope
        diameter: float = diameters[incoming_diameter_idx]
        
        max_hyd_rad = df_hydraulics.max_hydraulic_radius[incoming_diameter_idx]
        max_area = df_hydraulics.max_areas[incoming_diameter_idx]
        min_slope = df_hydraulics.min_slopes[incoming_diameter_idx]
        max_slope = df_hydraulics.max_slopes[incoming_diameter_idx]
        
        # make sure discharge fits through the pipe
        max_discharge = max_area * get_velocity(slope, max_hyd_rad, n)

        if design_discharge < max_discharge:

            # calculate the real flow depth
            y_n, exact_flow = get_normal_depth(diameter, design_discharge, slope, n)
            fill_ratio = y_n / diameter
            # recalculate geometric parameters
            angle = get_angle(diameter, y_n)
            h_radius = get_hydraulic_radius(diameter, angle)
            area = get_area(diameter, angle)

            # recalculate hydraulic parameters
            velocity = get_velocity(slope, h_radius, n)
            tau = get_tau(slope, h_radius)
            top_width = get_top_width(diameter, angle)
            froude = get_froude(area, velocity, top_width)

            # check hydraulic constraints
            checked_hydraulics = True

            if checked_hydraulics:
                verbose_(f" FOUND IT {id_up} {diameter} {slope}", verbose=verbose)
                # Add values for the hydraulic design to data frame               
                dict_hydraulics = recalculate_hydraulics(diameter, slope, design_discharge, n,undercapacity_fill, overcapacity_fill)
    
                pipes.loc[pipe_idx, "diameter"] = diameter
                pipes.loc[pipe_idx, "slope"] = slope
                pipes.loc[pipe_idx, "fill_ratio"] = dict_hydraulics["fill_ratio"]
                pipes.loc[pipe_idx, "y_n"] = dict_hydraulics["y_n"]
                pipes.loc[pipe_idx, "angle"] = dict_hydraulics["angle"]
                pipes.loc[pipe_idx, "h_radius"] = dict_hydraulics["h_radius"]
                pipes.loc[pipe_idx, "area"] = dict_hydraulics["area"]
                pipes.loc[pipe_idx, "velocity"] = dict_hydraulics["velocity"]
                pipes.loc[pipe_idx, "froude"] = dict_hydraulics["froude"]
                pipes.loc[pipe_idx, "tau"] = dict_hydraulics["tau"]
                pipes.loc[pipe_idx, "exact_flow"] = dict_hydraulics["exact_flow"]
                pipes.loc[pipe_idx, "max_flow"] = dict_hydraulics["max_flow"]
                pipes.loc[pipe_idx, "min_flow"] = dict_hydraulics["min_flow"]
                
                blocks.loc[block_idx, "diameter"] = diameter
                blocks.loc[block_idx, "slope"] = slope
                blocks.loc[block_idx, "fill_ratio"] = dict_hydraulics["fill_ratio"]
                blocks.loc[block_idx, "y_n"] = dict_hydraulics["y_n"]
                blocks.loc[block_idx, "angle"] = dict_hydraulics["angle"]
                blocks.loc[block_idx, "h_radius"] = dict_hydraulics["h_radius"]
                blocks.loc[block_idx, "area"] = dict_hydraulics["area"]
                blocks.loc[block_idx, "velocity"] = dict_hydraulics["velocity"]
                blocks.loc[block_idx, "froude"] = dict_hydraulics["froude"]
                blocks.loc[block_idx, "tau"] = dict_hydraulics["tau"]
                blocks.loc[block_idx, "exact_flow"] = dict_hydraulics["exact_flow"]
                blocks.loc[block_idx, "max_flow"] = dict_hydraulics["max_flow"]
                blocks.loc[block_idx, "min_flow"] = dict_hydraulics["min_flow"]
                
                return slope, diameter
        else:
            incoming_diameter_idx +=1
            
            if incoming_diameter_idx == len(diameters):
                break
         
        
        
        
    return slope, diameter

# %% [markdown]
# ## Step 2. Run Hydraulic Design
# %% [markdown]
# Walk Graph
# %%
def walk_graph(
    *,
    blocks,
    pipes,
    material="pvc",
    min_depth=1.0,
    max_depth=10.0,
    type_external=1,
    v_min=0.75,
    v_max=10.0,
    min_slope_limit=0.001,
    max_slope_limit=0.10,
    delta_slope=0.0001,
    undercapacity_fill=1,
    overcapacity_fill=0,
    diameters_list=commercial_diameters,
    verbose=False,
):
    """Main design algorithm that 'walks' the graph from the extremes to the outlet.


    :param manholes: dataframe with manholes information [id, x, y, z, outlet]
    :param pipes: dataframe with pipes information [id_up, id_down, type, desg_flow]
    :param material: pipe material default:"pvc"
    :param min_depth: minimum excavation depth :default:1.0
    :param max_depth: maximum excavation depth :default:1.0
    :param type_external: numerical value to describe outer-manholes default:1
    :param v_min: minimum flow velocity limit default: 0.75
    :param v_max: maximum flow velocity limit default: 10.0
    :param min_slope_limit: minimum slope limit default: 0.001
    :param max_slope_limit: maximum slope limit default: 0.10
    :param delta_slope: slope change default:0.0001
    :param diameters: list of commercial diameters
                      default:[0.225, 0.25, 0.35, 0.4, 0.5, 0.6, 0.80, 1.0, 1.20, 1.5, 2.0, 2.5, 3.0]
    :param verbose: Boolean, print statements if True
    :return: pipes: dataframe with the design of the pipes in the network
            df_hydraulics: dataframe with the hydraulic parameters of each designed pipe
    """

    # ------------------
    # --- INPUT DATA ---
    # ------------------
    verbose_("\n----------------------------", verbose=True)
    verbose_("ORGANIZING INPUT DATA", verbose=True)
    # verbose_("pipes \n {}".format(pipes), verbose=True)
    # verbose_("manholes \n {}".format(blocks), verbose=True)

    # dict -> design discharge for each pipe
    design_discharge = dict(zip(pipes.id_up, pipes.desg_flow))

    # dict -> map nodes for fast access {edge_id_up: [id_up, id_down]}
    edges = dict([(pipes.id_up[k], [pipes.id_up[k], pipes.id_down[k]]) for k in pipes.index])
    verbose_("edges: {}".format(edges), verbose)

    # list -> of outlet manholes
    outlet_manholes = blocks.id_up[blocks.outlet == 1].values
    verbose_("Outlet: {}".format(outlet_manholes), verbose)

    
    # list - of manholes (i.e. all vertices)
    vertices = blocks.id_up[blocks.has_infra >= 1].values  # List

    # list -> of external manholes (i.e. upstream-most manholes)
    external_manholes = pipes.id_up[pipes.pipe_loc == type_external].values  # List
    verbose_("external mh: {}".format(external_manholes), verbose)

    # dict -> map repeated downstream IDs and its frequency (No. of incoming pipes) per manhole
    internal_mh, count_mh_in = np.unique(pipes.id_down.values, return_counts=True)
    count_pipes_in = dict(zip(internal_mh, count_mh_in))
    verbose_("internal_mh {} ".format(internal_mh), verbose)
    verbose_("count_pipes_in {}".format(count_pipes_in), verbose)

    # dict -> get elevation dictionary
    terrain_elevations = dict(zip(blocks.id_up, blocks.z))
    if 0.0 in terrain_elevations.keys():
        del terrain_elevations[0.0]
    verbose_("terrain_elevation: {}".format(terrain_elevations), verbose)

    # get terrain slopes and xy - distances
    # xy_distances, terrain_slopes = calculate_terrain_slope_xy_distance(blocks, pipes)

    xy_distances = dict(zip(pipes.id_up, pipes.horiz_dist))
    terrain_slopes = dict(zip(pipes.id_up, pipes.terr_slope))
    verbose_("terrain_elevation: {}".format(terrain_elevations), verbose)
    verbose_("xy_distance: {}".format(xy_distances), verbose)

    # create a dictionary of nodes and their incoming manholes
    upstream_manholes = {}
    for edge in edges.values():
        upstream_manholes.setdefault(edge[1], []).append(edge[0])  # {downstream_id: [incoming upstream_ids]}
    verbose_("upstream_manholes: {}".format(upstream_manholes), verbose)
    verbose_("----------------------------\n", verbose=True)
    # ---

    # --------------------------------------------------------
    # --- PREPARE EMPTY DICTIONARIES TO STORE CALCULATIONS ---
    # --------------------------------------------------------

    verbose_("\n----------------------------", verbose=True)
    verbose_("PREPARING EMPTY DICTIONARIES", verbose=True)
    # prepare dict to store exact design flow {id: flow} [Float64]
    desg_flows = dict([(k, 0.0) for k in vertices])
    # verbose_("desg_flows: {}".format(desg_flows), verbose)

    # dict with pump info {key: mh, value: (previous bottom elevation, new bottom elevation)}
    pumps = dict([(k, 0.0) for k in vertices])
    verbose_("pumps: {}".format(pumps), verbose)

    # dict with deepened manholes because of steep terrain {mh: (min_depth, new_depth)}
    drops = dict([(k, 0.0) for k in vertices])
    verbose_("drops: {}".format(drops), verbose)

    # dict to keep track of the chosen slopes. { manhole_up: slope}
    chosen_slopes = dict([(k, 0.0) for k in vertices])
    verbose_("chosen_slopes: {}".format(chosen_slopes), verbose)

    # prepare count dictionary of total incoming pipes to a manhole {id: pipes_in}[Int],
    # to compare with 'count_pipes_in'
    pipes_in = dict([(k, 0) for k in vertices])
    verbose_("pipes_in: {}".format(pipes_in), verbose)

    # prepare diameter ID tracker
    diameters_idx_tracker = dict([(k, 0) for k in vertices])
    verbose_("diameters_idx_tracker: {}".format(diameters_idx_tracker), verbose)

    # prepare dict to store chosen diameters in m {id: diameter} [Float64]
    diameters_tracker = dict([(k, 0.0) for k in vertices])
    pipe_lengths = dict([(k, 0.0) for k in vertices])
    verbose_("diameter_tracker: {}".format(diameters_tracker), verbose)

    # upstream and downstream invert elevations of each pipe (Pipe ID : mh_up)
    up_elevations = dict([(k, 0.0) for k in vertices])  # {Up_ID : up_elevation}
    up_depths = dict([(k, 0.0) for k in vertices])  # {Up_ID : up_depth}
    verbose_("'-> up_elevations: {}".format(up_elevations), verbose)

    from_which_up_mh = dict([(i, []) for i in vertices])  # {Up_ID : [up_mh]}  could have multiple upstream manholes
    
    incoming_down_elevations = dict([(i, {}) for i in vertices])  # {Down_ID : {from_which_up: Down_elevations}} # 
    incoming_down_depths = dict([(i, {}) for i in vertices])  # {Down_ID : {from_which_up: Down_elevations}}


    down_elevations = dict([(k, 0.0) for k in vertices])  # {Up_ID : down_elevation}
    down_depths = dict([(k, 0.0) for k in vertices])  # {Up_ID : down_depth}
    verbose_("'-> down_elevations: {}".format(incoming_down_elevations), verbose)

    avg_depths = dict([(k, 0.0) for k in vertices])  # {Up_ID : down_depth}

    verbose_("---------------------------- \n", verbose=True)
    # ---

    # ---------------------------------------------------------------------------------------------------------------------
    # --------------------------------------------------- START ALGORITHM -------------------------------------------------
    # ---------------------------------------------------------------------------------------------------------------------
    verbose_("\n----------------------------", verbose=True)
    verbose_("START ALGORITHM", verbose=True)
    # add break criteria for reaching last mh:
    for mh_out in outlet_manholes:
        pipes_in[mh_out] = -1
        verbose_("pipes_in: {}".format(pipes_in), verbose)

        # assign biggest diameter to the last manhole (this does not assign the diameter to the last pipe)
        # needed for breaking criteria when more than one pipe is coming into the outlet manhole
        diameters_idx_tracker[mh_out] = len(diameters_list) - 1  # last diameter (idx starts in zero)
        diameters_tracker[mh_out] = diameters_list[-1]   # last diameter (idx starts in zero)
        verbose_("diameter_tracker: {}".format(diameters_idx_tracker), verbose)

    # calculate static hydraulic parameters for each pipe diameter
    roughness, n = get_roughness(material=material)
    df_hydraulics = prepare_hydraulics_data(diameters_list, v_min, v_max, n, verbose=verbose)
    verbose_("df_hydraulics \n", verbose=verbose)
    print_full_df(df_hydraulics)

    __chosen_diameter = 0.0  # temporal variable

    # --------------------------------------------------
    # --- Start each iteration from an outer manhole ---
    # --------------------------------------------------
    __iter = 1
    count_nn = 0
    for external_mh in external_manholes:
        verbose_("\n----------------------------", verbose=verbose)
        verbose_("ITERATION {}/{}".format(__iter, len(external_manholes)), verbose=verbose)

        id_mh_down = edges[external_mh][1]  # get the downstream manhole
        pipes_in[id_mh_down] += 1  # counts the number of pipes coming into a downstream manhole

        verbose_("\n** mh_up {}, mh_down {} **".format(external_mh, id_mh_down), verbose)

        # set ideal slope to the highest between the terrain slope and the min_slope_limit
        # slope can not be zero
        ideal_slope = max(terrain_slopes[external_mh], min_slope_limit)
        if ideal_slope < min_slope_limit:  # flat or negative slope
            verbose_(
                f"\n !! ERROR !! pipe {external_mh}: ideal_slope {ideal_slope} < min_slope_limit   \n",
                verbose=True,
            )
            ideal_slope = min_slope_limit

        # desg_flows[external_mh] = design_discharge[external_mh]

        verbose_(
            "# Finding best diameter-slope for {} -> {}".format(external_mh, id_mh_down),
            verbose,
        )
        
        #region find_slope-diameter
        count_nn += 1  # TODO Temporal for debugging.. delete
        slope, diameter = find_best_diameter_slope(
            pipes=pipes,
            blocks=blocks,
            df_hydraulics=df_hydraulics,
            id_up=external_mh,
            design_discharge=design_discharge[external_mh],
            ideal_slope=ideal_slope,
            incoming_diameter_idx=diameters_idx_tracker[external_mh],
            v_min=v_min,
            v_max=v_max,
            n=n,
            min_slope_limit=min_slope_limit,
            max_slope_limit=max_slope_limit,
            delta_slope=delta_slope,
            undercapacity_fill=undercapacity_fill, 
            overcapacity_fill=overcapacity_fill,
            roughness=roughness,
            diameters=diameters_list,
            verbose=False,
            count=count_nn,
        )

        # check for min slope limit
        if type(slope) == "list":
            verbose_(f"slope is a list --> {slope}", verbose=verbose)
        
        

        delta_h = slope * xy_distances[external_mh]  # upstream vs downstream elevation difference in m

        pipe_lengths[external_mh] = np.sqrt(np.square(delta_h) + np.square(xy_distances[external_mh]))

        # save data in dictionaries
        chosen_slopes[external_mh] = slope  # chosen slope for the pipe with id mh_up
        # diameters_idx_tracker[
        #     external_mh
        # ] = dia_idx  # index of the chosen diameter from the list of commercial diameters
        __chosen_diameter = diameter  # diameter in m
        diameters_tracker[external_mh] = diameter

        verbose_(
            "  diameter {} , slope {}, delta_h {}".format(__chosen_diameter, slope, delta_h),
            verbose,
        )

        # Save invert elevations. 
        # For outer pipes start at the minimum excavation elevation (include pipe diameter)
        if external_mh in up_elevations.keys():
            if up_elevations[external_mh]:  # if there are values in the list
                verbose_(
                    "  # There are already up_elevations[{}]: {}".format(external_mh, up_elevations[external_mh]),
                    verbose,
                )

            up_elevations[external_mh] = terrain_elevations[external_mh] - min_depth - __chosen_diameter  # absolute elevation upstream [m.a.s.l.]
            up_depths[external_mh] = terrain_elevations[external_mh] - up_elevations[external_mh]         # excavation depth upstream [m] 

        if id_mh_down in incoming_down_elevations.keys():
            
            #saves the upstream manhole from with the pipe comes
            from_which_up_mh[id_mh_down].append(external_mh)   
            
            # list of elevations coming into a manhole (mh_down). Saves the corresponding upstream manhole for each elevation
            incoming_down_elevations[id_mh_down][external_mh] = up_elevations[external_mh] - delta_h         # absolute elevation downstream [m.a.s.l.]

            incoming_down_depths[id_mh_down][external_mh] = terrain_elevations[id_mh_down] - incoming_down_elevations[id_mh_down][external_mh] # excavation depth downstream [m] 
            

            # final downstream elevation and depth for the current mh_up in evaluation
            down_elevations[external_mh] = incoming_down_elevations[id_mh_down][external_mh]                  # absolute elevation downstream [m.a.s.l.]
            down_depths[external_mh] = incoming_down_depths[id_mh_down][external_mh]                          # excavation depth downstream [m] 
        #endregion 
        

        # CHECK EXCAVATION LIMITS
        """ DROPS """
        # Check at the UPSTREAM manhole
        # if the pipe slope is too shallow:
        # not reaching the minimal excavation depth of the upstream manhole
        minimum_elevation_upstream = terrain_elevations[external_mh] - min_depth - __chosen_diameter  # absolute elevation upstream [m.a.s.l.]
        elevation_upstream = up_elevations[external_mh]

        if elevation_upstream > minimum_elevation_upstream:
            verbose_(
                "NEEDS A DROP: Invert elevation does not meet min excavation limit",
                verbose,
            )
        
            drop = elevation_upstream - minimum_elevation_upstream                                # distance [m]

            # Save the drop heigth (elevation difference)
            drops[external_mh] = drop
            
            verbose_("  Drop at mh_up {} : {}".format(external_mh, drop), verbose)

            # update elevations
            up_elevations[external_mh] -= drop                                                            # absolute elevation upstream [m.a.s.l.]
            up_depths[external_mh] = terrain_elevations[external_mh] - up_elevations[external_mh]         # excavation depth upstream [m] 
            # up_depths2[external_mh] = terrain_elevations[external_mh] - up_elevations[external_mh]         # excavation depth upstream [m] 

            incoming_down_elevations[id_mh_down][external_mh] -= drop
            incoming_down_depths[id_mh_down][external_mh] = terrain_elevations[id_mh_down] - incoming_down_elevations[id_mh_down][external_mh] # excavation depth downstream [m] 
            
            down_elevations[id_mh_down] = incoming_down_elevations[id_mh_down][external_mh]
            down_depths[external_mh] = incoming_down_depths[id_mh_down][external_mh]                          # excavation depth downstream [m] 

            # correcting pumps and drops
            if drops[external_mh] > pumps[external_mh] > 0: 
                pumps[external_mh] = 0                # the pipe downstream was lowered, so there is no need of pumping
            elif pumps[external_mh] > drops[external_mh] :
                pumps[external_mh] -= drops[external_mh]            # reduce the pumping heigh just the the starting elevation of the next pipe
                drops[external_mh] = 0  
        


        # Check at the DOWNSTREAM manhole
        # if the pipe slope is too shallow:
        # not reaching the minimal excavation depth of the downstream manhole
        # down_invert < terrain - (min_exc + diameter)

        minimum_down_elevation = terrain_elevations[id_mh_down] - min_depth - __chosen_diameter
        elevation_downstream_dict  = incoming_down_elevations[id_mh_down][external_mh]
        elevation_downstream  = down_elevations[external_mh]

        if elevation_downstream > minimum_down_elevation:
            verbose_(
                "NEEDS A DROP: Invert elevation does not meet min excavation limit",
                verbose,
            )

            drop = incoming_down_elevations[id_mh_down][external_mh] - minimum_down_elevation

                # Save the drop heigth (elevation difference)
            drops[external_mh] = drop  # {mh_up: (drop, old_elev, new_elev)}
            
            
            # make the drop from the upstream manhole 
            # update upstream elevation upstream manhole 
            up_elevations[external_mh] -= drop                                                            # absolute elevation upstream [m.a.s.l.]
            up_depths[external_mh] = terrain_elevations[external_mh] - up_elevations[external_mh]         # excavation depth upstream [m] 


            # update the dictionaries downstream manhole
            incoming_down_elevations[id_mh_down][external_mh] -= drop
            incoming_down_depths[id_mh_down][external_mh] = terrain_elevations[id_mh_down] - incoming_down_elevations[id_mh_down][external_mh] # excavation depth downstream [m] 
            
            down_elevations[external_mh] = incoming_down_elevations[id_mh_down][external_mh]
            down_depths[external_mh] = incoming_down_depths[id_mh_down][external_mh]

            # correcting pumps and drops
            if drops[external_mh] > pumps[external_mh] > 0: 
                pumps[external_mh] = 0                # the pipe downstream was lowered, so there is no need of pumping
            elif pumps[external_mh] > drops[external_mh] :
                pumps[external_mh] -= drops[external_mh]            # reduce the pumping heigh just the the starting elevation of the next pipe
                drops[external_mh] = 0  
        

            verbose_("  Drop at mh_down {} : {}".format(external_mh, drop), verbose)

        
        """ PUMPS """
        # Check at the DOWNSTREAM manhole
        # When reaching the max excavation limit at the downstream manhole, no more space for downstream pipes
        lower_downstream_elevation = min(incoming_down_elevations[id_mh_down].values())              # absolute elevation upstream [m.a.s.l.]
        
        # If the downstream elevation is deeper than the max elevation limit
        if lower_downstream_elevation <= terrain_elevations[id_mh_down] - max_depth:
            verbose_(
                "NEEDS A PUMP: Invert elevation reached max excavation limit",
                verbose,
            )

            # Elevate the elevation at the downstream to be set as a 
            # STARTING ELEVATION FOR THE DESIGN OF THE FOLLOWING PIPE

            # the DOWNSTREAM elevation pipe at the minimum excavation limit
            verbose_(
                "   Elevate the up_elevation of mh_down {} to the min_exc_elev".format(id_mh_down),
                verbose,
            )

            minimum_down_elevation = terrain_elevations[id_mh_down] - min_depth - __chosen_diameter        # absolute elevation downstream [m.a.s.l.]
            pumping_h = minimum_down_elevation - lower_downstream_elevation                            # distance [m]
            
            pumps[id_mh_down] = pumping_h
            verbose_(" PUMP at mh {} : {}".format(id_mh_down, pumps[id_mh_down]), verbose)
            
            # only update the final elevation for the mh_up of the following pipe (current mh_down) and 
            # not the individual list of downstream elevations arriving to manhole down because those pipes still arrive there
            # the difference would be for the starting point of the next downstream pipe

            up_elevations[id_mh_down] = terrain_elevations[id_mh_down] - min_depth - __chosen_diameter  
            up_depths[id_mh_down] = min_depth + __chosen_diameter 


            # correcting pumps and drops
            if drops[id_mh_down] > pumps[id_mh_down] > 0: 
                pumps[id_mh_down] = 0                # the pipe downstream was lowered, so there is no need of pumping
            elif pumps[id_mh_down] > drops[id_mh_down] :
                pumps[id_mh_down] -= drops[id_mh_down]            # reduce the pumping heigh just the the starting elevation of the next pipe
                drops[id_mh_down] = 0  
        

            

        # Check at the UPSTREAM manhole
        # if upstream elevation is deeper than the maximum excavation limit
        if up_elevations[external_mh] < terrain_elevations[external_mh] - max_depth:
            verbose_(f"Do not Pump, there is a drop at mh_up {external_mh} : {drops[external_mh]}", verbose=verbose)
            

        # verbose_("elevation {} -> {}".format(external_mh, mh_down), verbose)
        # verbose_("  upstream elevations {}".format(up_elevations), verbose)
        # verbose_("  downstream elevations {}".format(incoming_down_elevations), verbose)
        verbose_(
                        "  Invert elev up {}, down {}".format(
                            up_elevations[external_mh], down_elevations[external_mh]
                        ),
                        verbose,
                    )



        # SET MINIMUM DIMAETER FOR NEXT DOWNSTREAM PIPE
        """ MINIMUM DIAMETER """            
        # set at least same diameter for mh_down as mh_up
        # if the incoming pipe has a larger diameter compared to previous diameters evaluated coming to this manhole, assign the current larger diameter.
        if diameters_tracker[external_mh] > diameters_tracker[id_mh_down]:
            __min_idx_diameter = diameters_tracker[external_mh]  
            diameters_tracker[id_mh_down] = __min_idx_diameter
            diameters_idx_tracker[id_mh_down] = diameters_list.index(__min_idx_diameter)

            # verbose_("  diameters {}".format(diameters_tracker), verbose)
        

        ## ---------------------------------------------------------------------------------------------------------------------------##
        ##                                  FINISHED EXTERNAL PIPE DESIGN
        ## ---------------------------------------------------------------------------------------------------------------------------##


        # keep moving along the path until you reach a node that has multiple inflowing pipes
        verbose_(
            "\n## Keep moving along the path until you reach a node that has multiple inflowing pipes",
            verbose,
        )
        verbose_(f"ITERATION {__iter} MANHOLE_UP {id_mh_down}", verbose)


        # Continue design downstream from mh_down when all upstream pipes have been designed
        while pipes_in[id_mh_down] == count_pipes_in[id_mh_down]:

            previous_down = id_mh_down  # save temporarily

            # update mh_IDs for new pipe
            id_mh_up = id_mh_down  # new mh_up is the downstream mh from the upstream pipe
            id_mh_down = edges[id_mh_up][1]  # retrieve downstream manhole of the edge
            pipes_in[id_mh_down] += 1  # count incoming pipe
            verbose_(f"\n** mh_up {id_mh_up}, mh_down {id_mh_down} **", verbose)

            # Save invert elevations. For inner pipes consider upstream elevation for the previous pipe
            if id_mh_up not in up_elevations.keys():
                verbose_(
                    f"\n!! WARNING !! mh {id_mh_up} not in upstream elevations\n",
                    verbose,
                )
                verbose_(f"upstream manholes {upstream_manholes}", verbose)

            # set new elevation where the lowest mh_up ends
            verbose_(
                f"  >> set new elevation where the lowest mh_up ends:\n"
                "   >> to_evaluate: {up_elevations[mh_up]} , previous: {down_elevations[previous_down][-1]}",
                verbose,
            )

            if up_elevations[id_mh_up]:
                # If there is already an up_elevation saved, do not update the elevation
                # means that a pump was included from the previous pipe
                verbose_(" PUMP at mh {} : {} ".format(id_mh_up, pumps[id_mh_up]), verbose)
            else:
                # set new elevation at the lowest mh_up ends
                up_elevations[id_mh_up] = min(incoming_down_elevations[previous_down].values())
                up_depths[id_mh_up] = terrain_elevations[id_mh_up] - up_elevations[id_mh_up]

            # -------- from above
            #region find the diameter and slope of the pipe
            verbose_(
                "# Finding best diameter-slope for {} -> {}".format(id_mh_up, id_mh_down),
                verbose=verbose,
            )
            
            
            count_nn += 1
            slope, diameter = find_best_diameter_slope(
                pipes=pipes,
                blocks=blocks,
                df_hydraulics=df_hydraulics,
                id_up=id_mh_up,
                design_discharge=design_discharge[id_mh_up],
                ideal_slope=ideal_slope,
                incoming_diameter_idx=diameters_idx_tracker[id_mh_up],
                v_min=v_min,
                v_max=v_max,
                n=n,
                min_slope_limit=min_slope_limit,
                max_slope_limit=max_slope_limit,
                delta_slope=delta_slope,
                roughness=roughness,
                diameters=diameters_list,
                verbose=False,
                count=count_nn,
            )
            # if design  is close to zero!
            if slope < min_slope_limit:
                verbose_(
                    "\n!! WARNING MIN_SLOPE !! slope {}, diameter {}\n".format(slope, diameter),
                    verbose,
                )

            delta_h = slope * xy_distances[id_mh_up]  # upstream vs downstream elevation difference in m
            # verbose_("\nmh_up {}, mh_down {}".format(mh_up, mh_down), verbose)

            pipe_lengths[id_mh_up] = np.sqrt(np.square(delta_h) + np.square(xy_distances[id_mh_up]))

            verbose_(
                "  diameter {} , slope {}, delta_h {}".format(diameter, slope, delta_h),
                verbose,
            )

            if id_mh_down not in incoming_down_elevations.keys():
                verbose_(
                    "\n!! WARNING !! mh {} not in downstream elevations\n".format(id_mh_down),
                    verbose,
                )
            from_which_up_mh[id_mh_down].append(id_mh_up)
            # dict key [current_id][coming from mh_up]
            incoming_down_elevations[id_mh_down][id_mh_up] = up_elevations[id_mh_up] - delta_h
            incoming_down_depths[id_mh_down][id_mh_up] = (
                terrain_elevations[id_mh_down] - incoming_down_elevations[id_mh_down][id_mh_up]
            )

            down_elevations[id_mh_up] = incoming_down_elevations[id_mh_down][id_mh_up]
            down_depths[id_mh_up] = incoming_down_depths[id_mh_down][id_mh_up]

            verbose_(
                "  invert elev up {}, down {}".format(up_elevations[id_mh_up], incoming_down_elevations[id_mh_down][id_mh_up]),
                verbose,
            )

            # save data in dictionaries
            chosen_slopes[id_mh_up] = slope  # chosen slope for the pipe with id mh_up
            diameters_tracker[id_mh_up] = diameter  # index of the chosen diameter from the list of commercial diameters
            __chosen_diameter = diameter  # diameter in m for calculations
            diameters_idx_tracker[id_mh_up] = diameters_list.index(diameter)  # save in diameter_tracker dictionary
            # verbose_("  diameters {}".format(diameters_tracker), verbose)


            # set at least same diameter for mh_down as mh_up
            if id_mh_down not in outlet_manholes:
                           
                if diameters_tracker[id_mh_up] > diameters_tracker[id_mh_down]:
                    __min_idx_diameter = diameters_tracker[id_mh_up]  # incoming diameter
                    diameters_tracker[id_mh_down] = __min_idx_diameter
                    diameters_idx_tracker[id_mh_down] = diameters_list.index(__min_idx_diameter)

                    verbose_("  set at least same diameter for mh_down as mh_up", verbose)
                    verbose_("  diameters {}".format(diameters_tracker), verbose)
            #endregion
            
            # CHECK EXCAVATION LIMITS
            """ DROPS """
            # Check at the UPSTREAM manhole
            # if the pipe slope is too shallow:
            # not reaching the minimal excavation depth of the upstream manhole
            if up_elevations[id_mh_up] > terrain_elevations[id_mh_up] - min_depth - __chosen_diameter:
                verbose_(
                    "NEEDS A DROP: Invert elevation does not meet min excavation limit",
                    verbose,
                )
                
                minimum_elevation_upstream = terrain_elevations[id_mh_up] - min_depth - __chosen_diameter  # absolute elevation upstream [m.a.s.l.]
                drop = up_elevations[id_mh_up] - minimum_elevation_upstream                                # distance [m]

                # Save the drop heigth (elevation difference)
                drops[id_mh_up] = drop
               
                verbose_("  Drop at mh_up {} : {}".format(id_mh_up, drop), verbose)

                # update elevations
                up_elevations[id_mh_up] -= drop                                                                  # absolute elevation upstream [m.a.s.l.]
                up_depths[id_mh_up] = terrain_elevations[id_mh_up] - up_elevations[id_mh_up]         # excavation depth upstream [m] 


                incoming_down_elevations[id_mh_down][id_mh_up] -= drop
                incoming_down_depths[id_mh_down][id_mh_up] = terrain_elevations[id_mh_down] - incoming_down_elevations[id_mh_down][id_mh_up] # excavation depth downstream [m] 
                
                down_elevations[id_mh_up] = incoming_down_elevations[id_mh_down][id_mh_up]
                down_depths[id_mh_up] = incoming_down_depths[id_mh_down][id_mh_up]
               
                # correcting pumps and drops
                if drops[id_mh_up] > pumps[id_mh_up] > 0: 
                    pumps[id_mh_up] = 0                # the pipe downstream was lowered, so there is no need of pumping
                elif pumps[id_mh_up] > drops[id_mh_up] :
                    pumps[id_mh_up] -= drops[id_mh_up]            # reduce the pumping heigh just the the starting elevation of the next pipe
                    drops[id_mh_up] = 0  
          


            # Check at the DOWNSTREAM manhole
            # if the pipe slope is too shallow,  with steep topography
            # ending up above the minimal elevation at the downstream manhole
            # down_invert < terrain - (min_exc + diameter)
            if incoming_down_elevations[id_mh_down][id_mh_up] > terrain_elevations[id_mh_down] - min_depth - __chosen_diameter:
                verbose_(
                    "NEEDS A DROP: Invert elevation does not meet min excavation limit",
                    verbose,
                )
                minimum_down_elevation = terrain_elevations[id_mh_down] - min_depth - __chosen_diameter
                drop = incoming_down_elevations[id_mh_down][id_mh_up] - minimum_down_elevation

                # Save the drop heigth (elevation difference)
                drops[id_mh_up] = drop  # {mh_up: (drop, old_elev, new_elev)}
               
                # make the drop from the upstream manhole 
                # update upstream elevation upstream manhole 
                up_elevations[id_mh_up] -= drop
                up_depths[id_mh_up] = terrain_elevations[id_mh_up] - up_elevations[id_mh_up] 

                # update the dictionaries downstream manhole
                incoming_down_elevations[id_mh_down][id_mh_up] -= drop
                incoming_down_depths[id_mh_down][id_mh_up] = terrain_elevations[id_mh_down] - incoming_down_elevations[id_mh_down][id_mh_up] # excavation depth downstream [m] 
                
                down_elevations[id_mh_up] = incoming_down_elevations[id_mh_down][id_mh_up]
                down_depths[id_mh_up] = incoming_down_depths[id_mh_down][id_mh_up]

                # correcting pumps and drops
                if drops[id_mh_up] > pumps[id_mh_up] > 0: 
                    pumps[id_mh_up] = 0                # the pipe downstream was lowered, so there is no need of pumping
                elif pumps[id_mh_up] > drops[id_mh_up] :
                    pumps[id_mh_up] -= drops[id_mh_up]            # reduce the pumping heigh just the the starting elevation of the next pipe
                    drops[id_mh_up] = 0  
          
                verbose_("  Drop at mh_down {} : {}".format(id_mh_up, drop), verbose)

            
            """ PUMPS """
            # Check at the DOWNSTREAM manhole
            # When reaching the max excavation limit at the downstream manhole, no more space for downstream pipes
            lower_downstream_elevation = min(incoming_down_elevations[id_mh_down].values())              # absolute elevation upstream [m.a.s.l.]
           
            # If the downstream elevation is deeper than the max elevation limit
            if lower_downstream_elevation <= terrain_elevations[id_mh_down] - max_depth:
                verbose_(
                    "NEEDS A PUMP: Invert elevation reached max excavation limit",
                    verbose,
                )

                # Elevate the elevation at the downstream to be set as a 
                # STARTING ELEVATION FOR THE DESIGN OF THE FOLLOWING PIPE

                # the DOWNSTREAM elevation pipe at the minimum excavation limit
                verbose_(
                    "   Elevate the up_elevation of mh_down {} to the min_exc_elev".format(id_mh_down),
                    verbose,
                )

                minimum_down_elevation = terrain_elevations[id_mh_down] - min_depth - __chosen_diameter        # absolute elevation downstream [m.a.s.l.]
                pumping_h = minimum_down_elevation - lower_downstream_elevation                            # distance [m]
                
                pumps[id_mh_down] = pumping_h
                verbose_(" PUMP at mh {} : {}".format(id_mh_down, pumps[id_mh_down]), verbose)
                
                # only update the final elevation for the mh_up of the following pipe (current mh_down) and 
                # not the individual list of downstream elevations arriving to manhole down because those pipes still arrive there
                # the difference would be for the starting point of the next downstream pipe

                up_elevations[id_mh_down] = terrain_elevations[id_mh_down] - min_depth - __chosen_diameter  
                up_depths[id_mh_down] = min_depth + __chosen_diameter 

                # correcting pumps and drops
                if drops[id_mh_down] > pumps[id_mh_down] > 0: 
                    pumps[id_mh_down] = 0                # the pipe downstream was lowered, so there is no need of pumping
                elif pumps[id_mh_down] > drops[id_mh_down] :
                    pumps[id_mh_down] -= drops[id_mh_down]            # reduce the pumping heigh just the the starting elevation of the next pipe
                    drops[id_mh_down] = 0  
            

            # Check at the UPSTREAM manhole
            # if upstream elevation is deeper than the maximum excavation limit
            if up_elevations[id_mh_up] < terrain_elevations[id_mh_up] - max_depth:
                verbose_(f"Do not pump, there is a drop at mh_up {id_mh_up} : {drops[id_mh_up]}", verbose=verbose)
                

            # verbose_("elevation {} -> {}".format(mh_up, mh_down), verbose)
            # verbose_("  upstream elevations {}".format(up_elevations), verbose)
            # verbose_("  downstream elevations {}".format(incoming_down_elevations), verbose)

            # -------------------


        # Iteration finished from an outer manhole, start from another outer manhole in the next iteration
        __iter += 1


    for k in up_depths.keys():
        # if k in down_depths.keys():
        a=np.array(up_depths[k])
        b=np.array(down_depths[k])
        avg_depths[k] = (a+b)/2 

    # update dataframe. add columns with the new pipes design
    # Index with the IDs of mh_up
    
    pipes["tElev_up"] = pipes["id_up"].map(terrain_elevations)
    pipes["tElev_down"] = pipes["id_down"].map(terrain_elevations)
    pipes["terrain_slope"] = pipes["id_up"].map(terrain_slopes)

    pipes["diameter"] = pipes["id_up"].map(diameters_tracker)
    pipes["sww_length"] = pipes["id_up"].map(pipe_lengths)
    pipes["slope"] = pipes["id_up"].map(chosen_slopes)

    pipes["up_elevation"] = pipes["id_up"].map(up_elevations)
    pipes["down_elevation"] = pipes["id_up"].map(down_elevations)
    pipes["up_depth"] = pipes["id_up"].map(up_depths)
    pipes["down_depth"] = pipes["id_up"].map(down_depths)
    pipes["avg_depth"] = pipes["id_up"].map(avg_depths)
    pipes["pump"] = pipes["id_up"].map(pumps)
    pipes["drop"] = pipes["id_up"].map(drops)
    # pipes["from_up_mh"] = pipes["id_down"].map(from_which_up_mh)# Index with the IDs of mh_down


    # Update Blocks
    
    blocks["tElev_up"] = blocks["id_up"].map(terrain_elevations)
    blocks["tElev_down"] = blocks["id_down"].map(terrain_elevations)
    blocks["terrain_slope"] = blocks["id_up"].map(terrain_slopes)
    
    blocks["diameter"] = blocks["id_up"].map(diameters_tracker)
    blocks["sww_length"] = blocks["id_up"].map(pipe_lengths)
    blocks["slope"] = blocks["id_up"].map(chosen_slopes)

    blocks["up_elevation"] = blocks["id_up"].map(up_elevations)
    blocks["down_elevation"] = blocks["id_up"].map(down_elevations)
    blocks["up_depth"] = blocks["id_up"].map(up_depths)
    blocks["down_depth"] = blocks["id_up"].map(down_depths)
    blocks["avg_depth"] = blocks["id_up"].map(avg_depths)
    blocks["pump"] = blocks["id_up"].map(pumps)
    blocks["drop"] = blocks["id_up"].map(drops)
    # blocks["from_up_mh"] = blocks["id_down"].map(from_which_up_mh)# Index with the IDs of mh_down
    

    return blocks, pipes

# %% [markdown]
# Run algorithm over time
# %%
def run_hydraulic_design(*, path:str,
                output_path:str,
                year_i:int, 
                year_f:int, 
                year_step:int,                
                case_study:str,
                module:str,
                connectivity:str,
                undercapacity_fill:float, 
                overcapacity_fill:float,
                verbose:bool=False,
                notify:bool=False,
                plot:bool=False):
    
    blocks_dict = dict()
    pipes_dict = dict()
                    
    for year in range(year_i, year_f+1, year_step):

        # Start counting the execution time
        st = float(time.time())

        # Read input 
        path_blocks=path/read_module/connectivity/fr"{connectivity.lower()}_{case_study}_blocks_({year})/{connectivity.lower()}_{case_study}_blocks_({year}).shp"
        print(path_blocks)
        blocks = gpd.read_file(path_blocks)
        blocks_dict[year]=blocks
        
        path_pipes=path/read_module/connectivity/f"{connectivity.lower()}_{case_study}_pipes_({year})/{connectivity.lower()}_{case_study}_pipes_({year}).shp"
        pipes = gpd.read_file(path_pipes)
        print(path_pipes)
        pipes_dict[year]=pipes

        # Duplicate columns for correct input 
        columns = ['BlockID', 'CentreX', 'CentreY', 'blk_elev', 'has_wwtp', 'id_up', 'id_down', 'pipe_loc', 'desg_flow' ]
        new_columns = ['id', 'x', 'y', 'z', 'outlet', 'id_up', 'id_down', 'type', 'desg_flow']
        blocks = duplicate_columns_with_new_name(df=blocks, columns=columns, new_columns=new_columns)
        # display(blocks.loc[blocks["has_infra"]>=1,new_columns])

        pipes = duplicate_columns_with_new_name(df=pipes, columns=columns, new_columns=new_columns)
        # display(pipes[new_columns])

        # Create new columns
        blocks = df_with_new_fields (df=blocks, module=module, verbose=verbose)
        # display(blocks[blocks.columns[-16:]])

        pipes = df_with_new_fields (df=pipes, module=module, verbose=verbose)
        # display(pipes[pipes.columns[-16:]])
        """ EXECUTION TIME """
        verbose_(f"\n organising data time: {time.time() - st} s\n", notify)

        # Walk_graph

        # Start counting the execution time
        st = float(time.time())
        blocks, pipes = walk_graph(blocks=blocks,
            pipes=pipes,
            material=material,
            min_depth=min_depth,
            max_depth=max_depth,
            type_external=type_external,
            v_min=v_min,
            v_max=v_max,
            min_slope_limit=min_slope_limit,
            max_slope_limit=max_slope_limit,
            delta_slope=delta_slope,
            undercapacity_fill=undercapacity_fill,
            overcapacity_fill=overcapacity_fill,
            diameters_list=commercial_diameters,
            verbose=verbose)
        
        

        verbose_(f"\n time: {time.time() - st} s\n", notify)
        
        verbose_(f"____write output_____", notify)
        st = float(time.time())
            
        # Save hydraulic design
        # Step X
        ## X.1 
        save_df_in_dict(dict=pipes_dict, df=pipes, key=year)
        save_df_in_dict(dict=blocks_dict, df=blocks, key=year)

        ## X.2 create shapefile
        save_shp_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks, year=year, iteration="")
        save_shp_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes, year=year, iteration="")


        ## X.3 create csv file
        save_csv_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks, year=year, iteration="")
        save_csv_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes, year=year, iteration="")

        """ EXECUTION TIME """
        verbose_(f"\n time: {time.time() - st} s\n", notify)
        verbose_(f"_______________End of {year} simulation________________ ", notify)
        
        
        # Save previous design
        l_blk_diff_blkIDs=[]
        if year==year_i:
            # save the same design
            # X.2 create shapefile
            save_shp_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks+"_old", year=year, iteration="")
            save_shp_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes+"_old", year=year, iteration="")

            ## X.3 create csv file
            save_csv_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks+"_old", year=year, iteration="")
            save_csv_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes+"_old", year=year, iteration="")
            pass
        
        elif year > year_i:
            # save previous design to future states and recalculate hydraulics 
            previous_pipes = pipes_dict[year-year_step]
            previous_pipes_IDs = list(previous_pipes["BlockID"].values)
            current_pipes_IDs = list(pipes["BlockID"].values)
            
            bool_b_in_a = np.in1d(current_pipes_IDs, previous_pipes_IDs)  # check which pipes form B are already in A
            pipes_old = [current_pipes_IDs[i] for i in range(len(current_pipes_IDs)) if bool_b_in_a[i]]
            print(pipes_old)
            
            for i, values in pipes.loc[bool_b_in_a, ["BlockID","diameter", "slope", "desg_flow"]].iterrows():
                current_blkID = values[0]
                current_diameter = values[1]
                current_slope = values[2]
                current_design_flow = values[3]
                
                previous_diameter = previous_pipes.loc[previous_pipes["BlockID"]==current_blkID, "diameter"].values
                previous_slope = previous_pipes.loc[previous_pipes["BlockID"]==current_blkID, "slope"].values
                idx_corresponding_block = blocks[blocks["BlockID"]==current_blkID].index
                
                if current_diameter != previous_diameter or current_slope != previous_slope:
                    l_blk_diff_blkIDs.append(current_blkID)
                    
                    roughness, n = get_roughness(material=material)
                    dict_hydraulics = recalculate_hydraulics(previous_diameter, previous_slope, current_design_flow, n,undercapacity_fill, overcapacity_fill)
    
                    pipes.loc[i, "diameter"] = previous_diameter
                    pipes.loc[i, "slope"] = previous_slope
                    pipes.loc[i, "fill_ratio"] = dict_hydraulics["fill_ratio"]
                    pipes.loc[i, "y_n"] = dict_hydraulics["y_n"]
                    pipes.loc[i, "angle"] = dict_hydraulics["angle"]
                    pipes.loc[i, "h_radius"] = dict_hydraulics["h_radius"]
                    pipes.loc[i, "area"] = dict_hydraulics["area"]
                    pipes.loc[i, "velocity"] = dict_hydraulics["velocity"]
                    pipes.loc[i, "froude"] = dict_hydraulics["froude"]
                    pipes.loc[i, "tau"] = dict_hydraulics["tau"]
                    pipes.loc[i, "exact_flow"] = dict_hydraulics["exact_flow"]
                    pipes.loc[i, "max_flow"] = dict_hydraulics["max_flow"]
                    pipes.loc[i, "min_flow"] = dict_hydraulics["min_flow"]
                    
                    blocks.loc[idx_corresponding_block, "diameter"] = previous_diameter
                    blocks.loc[idx_corresponding_block, "slope"] = previous_slope
                    blocks.loc[idx_corresponding_block, "fill_ratio"] = dict_hydraulics["fill_ratio"]
                    blocks.loc[idx_corresponding_block, "y_n"] = dict_hydraulics["y_n"]
                    blocks.loc[idx_corresponding_block, "angle"] = dict_hydraulics["angle"]
                    blocks.loc[idx_corresponding_block, "h_radius"] = dict_hydraulics["h_radius"]
                    blocks.loc[idx_corresponding_block, "area"] = dict_hydraulics["area"]
                    blocks.loc[idx_corresponding_block, "velocity"] = dict_hydraulics["velocity"]
                    blocks.loc[idx_corresponding_block, "froude"] = dict_hydraulics["froude"]
                    blocks.loc[idx_corresponding_block, "tau"] = dict_hydraulics["tau"]
                    blocks.loc[idx_corresponding_block, "exact_flow"] = dict_hydraulics["exact_flow"]
                    blocks.loc[idx_corresponding_block, "max_flow"] = dict_hydraulics["max_flow"]
                    blocks.loc[idx_corresponding_block, "min_flow"] = dict_hydraulics["min_flow"]
                    
            pipes_dict[year] = pipes
            blocks_dict[year] = blocks
            # X.2 create shapefile
            save_shp_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks+"_old", year=year, iteration="")
            save_shp_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes+"_old", year=year, iteration="")

            ## X.3 create csv file
            save_csv_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks+"_old", year=year, iteration="")
            save_csv_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes+"_old", year=year, iteration="")
            
        if len(l_blk_diff_blkIDs)==0:
            print(f"No differences among the pipes desing between {year_i} and {year}")
        else:
            print(f"Differences among the pipes desing between {year_i} and {year}")
            print(l_blk_diff_blkIDs) 
            
    """ EXECUTION TIME """
    verbose_(f"\n time: {time.time() - st} s\n", notify)
    verbose_(f"_______________End of {year} simulation________________ ", notify)     

    return blocks_dict, pipes_dict
