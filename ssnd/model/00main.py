# %% [markdown]
# # TURN-Sewers
# ## Transitions in Urban Networks - Towards decentralised sewer systems
# 
# """
# @file   00_main.py
# @author  Natalia Duque <natalia.duquevillarreal@eawag.ch>
# @section LICENSE
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# """
# 
# __author__ = "Natalia Duque"
# __copyright__ = "Copyright 2018."

# %%
# region --- PYTHON LIBRARY IMPORTS ---

# ignore developer warnings
import warnings
warnings.simplefilter("ignore", category=FutureWarning)

# get current directory
import os
print(os.getcwd())
import sys
sys.path.append('../..')

# imports for the module
import time
from math import *
from pathlib import Path
from random import randint, seed
import numpy as np
import pandas as pd
import seaborn as sns
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import matplotlib.colors as colors
# --- Spatial geometry ---
from shapely.geometry import Point, Polygon, LineString
# --- Graph theory algorithms ---
import networkx as nx
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree

# --- SSND IMPORTS ---
from ssnd.defaults import *
from ssnd.help_functions.helper_functions import *
from ssnd.help_functions.plot_maps import *

from ssnd.model.a_topology import run_topology
from ssnd.model.b_hydraulic_design import run_hydraulic_design
from ssnd.model.c_deterioration_model import run_deterioration, g_distributions, plot_distributions


pd.options.mode.chained_assignment = None  # default='warn'


bool_run_step_by_step = False

verbose = False
notify = True
plot = True
# endregion

# %%
"""USER DEFINED VARIABLES"""
#region Initialize 
test=True
connectivity="decentralised"

# CASE STUDIES
# case_study = "USTER_200"
# case_study = "Uster_200_08flow"
case_study = "Uster_200_200L"
# case_study = "USTER_200_x10Pop"

# TEST CASE STUDIES
grid = 10
test_case_study = f"Case_{grid}x{grid}_20xFlow"
# test_case_study = f"Case_{grid}_grid"

input_path = DATA_DIR/case_study/"input/shp"
read_path = DATA_DIR/case_study/"output/shp" # read from previous modules 
output_path = DATA_DIR /case_study/"output"

# For the test case study
if test: 
        case_study = test_case_study
        name = f"{grid}_blocks"
        input_path = TEST_DATA_DIR/case_study/"input"/"shp"
        read_path = TEST_DATA_DIR/case_study/"output/shp"
        output_path = TEST_DATA_DIR/case_study/"output" 

# Save old hydraulic design with another name
name_blocks_notRedesigned= "blocks_old"
name_pipes_notRedesigned ="pipes_old"

# Set the random seed
random_seed=24
np.random.seed(random_seed)
#endregion


print(input_path)
input_path.exists()

# %%[markdown]
## Topology delineation
# %%
module = "topology"       

# %%
"""RUNNING EXISTING TOPOLOGY"""
tt = float(time.time())
blocks_dict, pipes_dict = run_topology(
        path = input_path,
        connectivity=connectivity,
        module=module,
        case_study= case_study, 
        name_blocks=name_blocks,
        name_pipes=name_pipes,
        output_path=output_path,
        year_i = year_i, 
        year_f = year_f, 
        year_step = year_step,
        existing_treatment_id=existing_treatment_id,
        peak_factor=peak_factor,
        return_factor=return_factor,
        L_person_day=L_person_day,  
        scale_factor_PE = scale_factor_PE,
        growth_rate_PE = growth_rate_PE,       
        drop=drop,        
        verbose=verbose,
        notify = notify,
        plot = plot)

total_time = time.time() - tt

# %%
"""READ EXISTING TOPOLOGY"""
path=read_path
blocks_dict=dict()
pipes_dict=dict()
for year in range(year_i, year_f+1, year_step):
        # Read input 
        path_blocks=path/module/connectivity/fr"{connectivity.lower()}_{case_study}_{name_blocks}_({year})/{connectivity.lower()}_{case_study}_{name_blocks}_({year}).shp"
        print(path_blocks)
        blocks = gpd.read_file(path_blocks)
        blocks_dict[year]=blocks
        
        path_pipes=path/module/connectivity/f"{connectivity.lower()}_{case_study}_{name_pipes}_({year})/{connectivity.lower()}_{case_study}_{name_pipes}_({year}).shp"
        pipes = gpd.read_file(path_pipes)
        print(path_pipes)
        pipes_dict[year]=pipes
# %%
sum(blocks_dict[2018]["Population"])


# %%[markdown]
## Hydraulic Design
# %% module = "hydraulic_design"
read_module = "topology"
module = "hydraulic_design"
connectivity="centralised"

# %% run_hydraulic_design

tt = float(time.time())
blocks_dict, pipes_dict = run_hydraulic_design(path=read_path,
                output_path=output_path,
                year_i=year_i, 
                year_f=year_f, 
                year_step=year_step,                
                case_study=case_study,
                module=module,
                connectivity=connectivity,
                undercapacity_fill=1, 
                overcapacity_fill=0,
                verbose=False,
                notify=True,
                plot=False)

total_time = time.time() - tt


# %% READ hydraulic_design
path=read_path
blocks_dict=dict()
pipes_dict=dict()
name_blocks_notRedesigned= name_blocks_notRedesigned
name_pipes_notRedesigned =name_pipes_notRedesigned
name_blocks_notRedesigned= "blocks"
name_pipes_notRedesigned ="pipes"

for year in range(year_i, year_f+1, year_step):

        # Read input
        path_blocks=path/module/connectivity/fr"{connectivity.lower()}_{case_study}_{name_blocks_notRedesigned}_({year})/{connectivity.lower()}_{case_study}_{name_blocks_notRedesigned}_({year}).shp"
        print(path_blocks)
        blocks = gpd.read_file(path_blocks)
        blocks_dict[year]=blocks
        
        path_pipes=path/module/connectivity/f"{connectivity.lower()}_{case_study}_{name_pipes_notRedesigned}_({year})/{connectivity.lower()}_{case_study}_{name_pipes_notRedesigned}_({year}).shp"
        pipes = gpd.read_file(path_pipes)
        print(path_pipes)
        pipes_dict[year]=pipes
# %%
np.unique(pipes_dict[2038]["diameter"], return_counts=True)
# %%
pipes_dict[2018]["diameter"]
# %%
# Check filling ration is changing
l_blk_diff_filing_ratio=[]
curr_year =2023
for i, d in blocks_dict[2018][["BlockID","fill_ratio"]].iterrows():
        p_id = int(d[0])
        p_diam = d[1]
        ids = list(blocks_dict[curr_year]["BlockID"].values)
        # print(ids)
        if d[0] in ids:
                c_diam = blocks_dict[curr_year].loc[blocks_dict[curr_year]["BlockID"]==p_id, "fill_ratio"].values
                if c_diam != p_diam:
                        print(p_diam, c_diam)
                        l_blk_diff_filing_ratio.append(p_id)

if len(l_blk_diff_filing_ratio)==0:
        print("There are no differences")
else:
        print(l_blk_diff_filing_ratio)
# %%
np.unique(blocks_dict[2038]["fill_ratio"], return_counts=True)

# %%
np.unique(blocks_dict[2018]["pump"], return_counts=True)

# %%
np.unique(blocks_dict[2018]["drop"], return_counts=True)

# %%
np.unique(blocks_dict[2023]["desg_flow"], return_counts=True)

# %%[markdown]
## Deterioration Model
# %%
module="deterioration"
read_module = "hydraulic_design"
connectivity="centralised"
# %% run_deterioration
tt = float(time.time())

blocks_dict, pipes_dict, times_dict = run_deterioration(iterations=1,
            year_i=year_i,
            year_f=year_f, 
            year_step=year_step,  
            years_warm_up=years_warm_up,
            planning_horizon=planning_horizon,                  
            d_ages=d_ages,    
            d_failures=d_failures,
            ls_wwtp=ls_wwtp,
            ls_wwtp_small=ls_wwtp_small,
            wwtp_stochastic_failure=False,
            d_ages_wwtp=d_ages_wwtp,
            d_failures_wwtp=d_failures_wwtp,
            input_path=read_path,
            output_path=output_path,            
            case_study=case_study,
            connectivity=connectivity,
            read_module=read_module,
            name_blocks=name_blocks,
            name_pipes=name_pipes,
            verbose=verbose,
            notify=notify)

total_time = time.time() - tt


#%% 
# COPY Deterioration for simulation years into the not_redesigned blocks
path=read_path
blocks_dict=dict()
pipes_dict=dict()
name_blocks_ = name_blocks
name_pipes_ = name_pipes

blocks_dict_notRedesigned=dict()
pipes_dict_notRedesigned=dict()
name_blocks_notRedesigned=name_blocks_notRedesigned
name_pipes_notRedesigned=name_pipes_notRedesigned

iteration = 1

for year in range(year_o, year_f+1, year_step):

        
        # Copy information on the blocks
        path_blocks=path/module/connectivity/fr"{connectivity.lower()}_{case_study}_{name_blocks_}_({year})[{iteration}]/{connectivity.lower()}_{case_study}_{name_blocks_}_({year})[{iteration}].shp"
        blocks = gpd.read_file(path_blocks)
        blocks_dict[year]=blocks
        
        path_blocks_notRedesigned=path/module/connectivity/fr"{connectivity.lower()}_{case_study}_{name_blocks_notRedesigned}_({year})[{iteration}]/{connectivity.lower()}_{case_study}_{name_blocks_notRedesigned}_({year})[{iteration}].shp"
        blocks_notRedesigned = gpd.read_file(path_blocks_notRedesigned)
        
        blocks_notRedesigned.loc[:,["age", "p_fail", "failed", "age_wwtp", "p_fail_wwt","fail_wwtp"] ]= blocks.loc[:,["age", "p_fail", "failed", "age_wwtp", "p_fail_wwt","fail_wwtp"] ]
        blocks_dict_notRedesigned[year]=blocks_notRedesigned
        
        # Copy information on the pipes
        path_pipes=path/module/connectivity/f"{connectivity.lower()}_{case_study}_{name_pipes_}_({year})[{iteration}]/{connectivity.lower()}_{case_study}_{name_pipes_}_({year})[{iteration}].shp"
        pipes = gpd.read_file(path_pipes)
        pipes_dict[year]=pipes
        
        path_pipes_notRedesigned=path/module/connectivity/fr"{connectivity.lower()}_{case_study}_{name_pipes_notRedesigned}_({year})[{iteration}]/{connectivity.lower()}_{case_study}_{name_pipes_notRedesigned}_({year})[{iteration}].shp"
        pipes_notRedesigned = gpd.read_file(path_pipes_notRedesigned)
        
        pipes_notRedesigned.loc[:,["age", "p_fail", "failed", "age_wwtp", "p_fail_wwt","fail_wwtp"] ]= pipes.loc[:,["age", "p_fail", "failed", "age_wwtp", "p_fail_wwt","fail_wwtp"] ]
        pipes_dict_notRedesigned[year]=pipes_notRedesigned
        
        # 9.2 create shapefile
        save_shp_file(df=blocks_notRedesigned, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks_notRedesigned, year=year, iteration=iteration)
        save_shp_file(df=pipes_notRedesigned, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes_notRedesigned, year=year, iteration=iteration)
        # print(output_path, module, connectivity,case_study, name_blocks_notRedesigned,year, iteration)

        # 9.3 create csv file            
        save_csv_file(df=blocks_notRedesigned, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks_notRedesigned, year=year, iteration=iteration)
        save_csv_file(df=pipes_notRedesigned, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes_notRedesigned, year=year, iteration=iteration)

        
        

       
#%% Read Deterioration for simulation years

path=read_path
blocks_dict=dict()
pipes_dict=dict()

name_blocks_ = name_blocks
name_pipes_ = name_pipes
# name_blocks_ = name_blocks_notRedesigned
# name_pipes_ = name_pipes_notRedesigned
iteration = 1

for year in range(year_i, year_f+1, year_step):

        # Read input x
        
        path_blocks=path/module/connectivity/fr"{connectivity.lower()}_{case_study}_{name_blocks_}_({year})[{iteration}]/{connectivity.lower()}_{case_study}_{name_blocks_}_({year})[{iteration}].shp"
        print(path_blocks)
        blocks = gpd.read_file(path_blocks)
        blocks_dict[year]=blocks
        
        
        path_pipes=path/module/connectivity/f"{connectivity.lower()}_{case_study}_{name_pipes_}_({year})[{iteration}]/{connectivity.lower()}_{case_study}_{name_pipes_}_({year})[{iteration}].shp"
        pipes = gpd.read_file(path_pipes)
        print(path_pipes)
        pipes_dict[year]=pipes

#%% # ploting results of test case
cmap_fail= sns.color_palette("dark:salmon_r", as_cmap=True)
plot_development(poly_dict=blocks_dict, 
                        lineStr_dict=pipes_dict, 
                        feature_poly="fail_wwtp", units_poly = "[delta]", cmap_poly= pl.cm.hot,
                        feature_lineStr="failed", units_lineStr = "[-]", cmap_lineStr=cmap_fail,
                        module=module, connectivity=connectivity, case_study=case_study, plot=plot, 
                        backgroud_layers=["has_ww", "has_wwtp"], cmap_background_layers=[pl.cm.Greys, pl.cm.gist_gray],
                        path=output_path,
                        verbose=False,
                        )

# %%
if plot:
        result_dir = TEST_DATA_DIR /case_study/ "output/csv"/ module/connectivity
        print(result_dir)

# %%
if plot:
        tot_simulation_year = year_ph-year_o
        # create df to plot the probability distributions.
        age_range = range( 0, tot_simulation_year, year_step)
        print(age_range)

# %%
if plot:
        df_distributions= g_distributions(result_dir, 
                                          age_range, 
                                          iterations= 1,
                                          connectivity=connectivity,
                                          case_study=case_study, 
                                          name= name_pipes)
        print(df_distributions)
        
        plot_distributions(df_distributions, type_dist="frequency", label="failure", color="blue", reference_data=[d_ages,d_failures], distribution="Weibull", years_tot_sim=0, verbose=False)

        plot_distributions(df_distributions, type_dist="density", label="failure", color="blue", reference_data=[d_ages,d_failures], distribution="Weibull", years_tot_sim=0, verbose=False)

        plot_distributions(df_distributions, type_dist="cumulative", label="failure", color="coral", reference_data=[d_ages, d_failures], distribution="Weibull", years_tot_sim=0, verbose=False)




# %%[markdown]
## Replacement Strategies

# %%
# Calculate Over-/Under-capacity 

# %%
list_over_cap_centr=[0]*len(blocks_dict.keys())
list_under_cap_centr=[0]*len(blocks_dict.keys())

list_over_cap_decentr=[0]*len(blocks_dict.keys())
list_under_cap_decentr=[0]*len(blocks_dict.keys())

# %%
list_over_cap = []
list_under_cap = []
max_flow = []
total_cap=[]
for year in range(year_i, year_f+1, year_step):

        # Read input x
        
        blocks=blocks_dict[year]
        
        
        blocks.loc[:, "capGap_max"] = (1 - blocks.loc[:, "exact_flow"]/blocks.loc[:, "max_flow"])
        blocks.loc[:, "capGap_min"] = (1 - blocks.loc[:, "exact_flow"]/blocks.loc[:, "max_flow"])
        
        Q_max = sum(blocks.loc[:, "max_flow"])
        # blocks.loc[:, "total_cap_over"] = sum(blocks.loc[:, "min_flow"])
        
        blocks.loc[:, "under_cap"] = 0.0
        blocks.loc[blocks["capGap_max"]<0, "under_cap"] = (blocks.loc[:, "capGap_max"]*blocks.loc[:, "max_flow"])/Q_max
        blocks.loc[:, "over_cap"] = 0.0
        blocks.loc[blocks["capGap_max"]>0, "over_cap"] = (blocks.loc[:, "capGap_max"]*blocks.loc[:, "max_flow"])/Q_max
        
        blocks.loc[:, "total_cap"]= (blocks.loc[:, "under_cap"]+blocks.loc[:, "over_cap"])
        
        max_flow.append(np.average(blocks["max_flow"].values))
        total_cap.append(np.average(blocks["total_cap"].values))
        list_under_cap.append(sum(blocks["under_cap"]))
        list_over_cap.append(sum(blocks["over_cap"]))

if connectivity.lower().startswith("c"):
        list_over_cap_centr = list_over_cap
        list_under_cap_centr =list_under_cap
else:
        list_over_cap_decentr = list_over_cap
        list_under_cap_decentr =list_under_cap
#%%

import seaborn as sns
import matplotlib.pyplot as plt

# Sample data
x = [0, 5, 10, 15, 20]
y1 = list_over_cap_centr
y2 = list_under_cap_centr
y3 = list_over_cap_decentr
y4 = list_under_cap_decentr

# Combine data into a single DataFrame
data = {"x": x, "y1": y1, "y2": y2, "y3": y3, "y4": y4}
df = pd.DataFrame(data)

# Plot 

sns.set(style="whitegrid")
plt.plot(x, y1, '-o', color='#522766', alpha=1, markersize=15, linewidth=3, markerfacecolor='white', label="C_Over-capacity")
plt.plot(x, y2, '-o', color='#522766',alpha=1, markersize=15, linewidth=3, markerfacecolor="#ded3e3", label="C_Under-capacity")
plt.plot(x, y3,'-d', color='#c563f2', alpha=1, markersize=10, linewidth=3, markerfacecolor='white', label="D_Over-capacity")
plt.plot(x, y4, '-d', color='#c563f2', alpha=1, markersize=10, linewidth=3, markerfacecolor='#dcacf2', label="D_Under-capacity")

# Add labels and title
plt.yticks(fontsize=14)
plt.xticks(x, fontsize=14)
plt.xlabel("years", fontsize=16)
plt.ylabel("Network's capacity [-]", fontsize=16)

# plt.title("Scatter Plot with Multiple Sets of Data")

# Display the plot
plt.grid(alpha=0.2)
plt.legend(loc='center left',ncol=2, bbox_to_anchor=(0, -0.3), fontsize=12)
plt.show()

#%%



#%%
# Update undercapacity in pipes 
from ssnd.model.c_deterioration_model import update_pipes_information
n_columns = ["capGap_max", "capGap_min", "under_cap", "over_cap","total_cap"]
pipes_dict = update_pipes_information(pipes_dict,
                                        blocks_dict,
                                        year_o=year_i, 
                                        year_ph=year_f,
                                        columns=n_columns)
# %%
print(*pipes_dict[2018].columns)
# %%
from ssnd.help_functions.plot_maps import truncate_colormap
plot_development(poly_dict=blocks_dict, 
                        lineStr_dict=pipes_dict, 
                        feature_poly="fail_wwtp", units_poly = "[delta]", cmap_poly= pl.cm.hot,
                        feature_lineStr="over_cap", units_lineStr = "[-]", cmap_lineStr=truncate_colormap(pl.cm.Purples,minval=0.4),
                        label_lineStr="Over-capacity [-]",
                        module=module, connectivity=connectivity, case_study=case_study, plot=plot, 
                        backgroud_layers=["has_ww", "has_wwtp"], cmap_background_layers=[pl.cm.Greys, pl.cm.gist_gray],
                        path=output_path,
                        verbose=False,
                        )


# %%
# Save output files
module="replacement"
for year in pipes_dict.keys():

    # Restart counting the execution time for each iteration
    st = float(time.time())
    verbose_(f"_______________save output {year} ________________ ", notify)

    blocks=blocks_dict[year]
    pipes=pipes_dict[year]

    # Step 9
    verbose_("\nStep 9 - Save output files ... ", notify)
    
    # check the coordinates reference system
    if not blocks.crs == pipes.crs:
        pipes.to_crs(blocks.crs)  # re-project to the same crs as the blocks

    ## 9.2 create shapefile
    save_shp_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks_, year=year, iteration=iteration)
    save_shp_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes_, year=year, iteration=iteration)


    ## 9.3 create csv file
    save_csv_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks_, year=year, iteration=iteration)
    save_csv_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes_, year=year, iteration=iteration)

    """ EXECUTION TIME """
    verbose_(f"\n time: {time.time() - st} s\n", notify)
    verbose_(f"_______________________________ ", notify)



# %%
for year in range(year_i, year_f+1, year_step):

        # Read input x
        
        blocks=blocks_dict[year]
        
        
        max_PE = max(blocks.loc[:, "PE_total"])
        pipes_vulnerability=  (blocks.loc[:, "p_fail"]*blocks.loc[:, "PE_total"])/max_PE
        wwtp_vulnerability = (blocks.loc[:, "p_fail_wwt"]*blocks.loc[:, "PE_total"])/max_PE
        blocks.loc[:, "vulnerblty"] = pipes_vulnerability + wwtp_vulnerability
        
        blocks_dict[year]=blocks
#%%        
blocks_dict[2038].loc[blocks_dict[2038]["has_wwtp"]>0, ["age", "age_wwtp", "p_fail", "p_fail_wwt", "vulnerblty"]]
# %%
sum(blocks_dict[2038].loc[blocks_dict[2038]["has_infra"]>0, "vulnerblty"])
# %%
np.average(blocks_dict[2038].loc[blocks_dict[2038]["has_infra"]>0, "age"])
# %%
np.average(blocks_dict[2038].loc[blocks_dict[2038]["has_infra"]>0, "age_wwtp"])
# %%
