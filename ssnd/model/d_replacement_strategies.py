# %% [markdown]
# # REPLACEMENT STRATEGIES

# %%
# region --- PYTHON LIBRARY IMPORTS ---
import time
from math import *
from pathlib import Path
from random import randint
import numpy as np
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import matplotlib.colors as colors
from scipy.optimize import curve_fit
%matplotlib inline
# --- Spatial geometry ---
from shapely.geometry import Point, Polygon, LineString

# --- SND IMPORTS ---
from ssnd.defaults import *
from ssnd.help_functions.helper_functions import *
#endregion 


    
module = "replacement_strategy"
read_module = "deterioration"

bool_run_step_by_step=True
if bool_run_step_by_step:
    # case_study = "Uster_200_200L"
    grid=10
    case_study = f"Case_{grid}x{grid}_20xFlow"
    connectivity = "decentralised"

    verbose = True  # set to TRUE for debugging
    notify = True   # set to TRUE to track the progress

    read_path = TEST_DATA_DIR/case_study/"output/shp"
    output_path = TEST_DATA_DIR /case_study/"output"

    blocks_dict=dict()
    pipes_dict = dict()


# %% [markdown]
# Read Input files

# %%
""" READ INPUT FILE """
# %%
iteration = 1
name_blocks_notRedesigned= "blocks_NotRedesigned"
name_pipes_notRedesigned ="pipes_NotRedesigned"
for year in range(year_i, year_ph+1, year_step):
     
    blocks = read_geoDataFrame(read_path, case_study, read_module=read_module, connectivity=connectivity, name= name_blocks_notRedesigned,year=year, iteration=iteration)
    blocks_dict[year] = blocks.copy()
    
    pipes = read_geoDataFrame(read_path, case_study, read_module=read_module, connectivity=connectivity,name=name_pipes_notRedesigned, year=year, iteration=iteration)
    pipes_dict[year]  = pipes.copy()

# %%
blocks_dict.keys()

# %% [markdown]
# # Run replacement strategies 

# %% [markdown]
# S1: Replace all undercapacity = Upsize and replaced failed pipes
# 
# S2: Replace all failed pipes. Upsize failed
# 
# S3: Replace all failed pipes.  No Upsizing

# %%
replacement_strategy = "S1"

# %%
year=2018

# %%
years_designed = list(range(year_i, year_f+1,year_step))
blocks = blocks_dict[year]
pipes = pipes_dict[year]

# %%
pipes

# %% [markdown]
# Calculate capacity gap 

        



# %% [markdown]
# Define whether the pipe was upsized 

# %%
# identify upsized pipes
for year in pipes_dict.keys():

    pipes = pipes_dict[year]

    pipes.loc[:, "upsized"] = 0 

    if year != year_o:

        previous_pipes= pipes_dict[year-year_step]

        for idx, pipe in pipes.iterrows():

            id_pipe = pipes.loc[idx, "BlockID"] 
            
            if id_pipe  in previous_pipes["BlockID"].values:
                idx_previous = previous_pipes[previous_pipes["BlockID"]==id_pipe].index[0]
                
                id_previous = previous_pipes.loc[idx_previous, "BlockID"]
                # print(id_pipe, pipes.loc[idx, "diameter"] , "      ", id_previous, previous_pipes.loc[idx_previous, "diameter"] )
                
                if pipes.loc[idx, "diameter"] > previous_pipes.loc[idx_previous, "diameter"] :
                    
                    # print(id_pipe, pipes.loc[idx, "diameter"] , "      ", id_previous, previous_pipes.loc[idx_previous, "diameter"] )
                    pipes.loc[idx, "upsized"]=1 
                
                if pipes.loc[idx, "fill_ratio"] > 0.8 :
                    
                    # print(id_pipe, pipes.loc[idx, "diameter"] , "      ", id_previous, previous_pipes.loc[idx_previous, "diameter"] )
                    pipes.loc[idx, "upsized"]=1 
            
        
    print(year, len(pipes.loc[pipes["upsized"]==1]))
        # if year == year_o +40:
        # break

# %%
pd.set_option("display.max_columns",335)
pipes_dict[2028][["BlockID", "failed","upsized"]].transpose()

# %%
columns_hd = ["diameter",      # pipe diameter [m]
            "slope",        # pipe slope
            "sww_length",    # pipe length
            # "fill_ratio",   # filling ration
            # "y_max",        # maximum flow depth
            # "y_n",           # normal flow depth
            # "angle",        # internal angle for circular pipes
            # "perimeter",    # wet perimeter
            # "top_width",     # top width
            # "h_radius",       # hydraulic_radius
            # "area",         # wet area
            # "velocity",     # flow velocity
            # "shear",        # shear stress
            # "froude",       # Froude number
            # "exact_flow",     # current flow in the pipe
            ] 
n_columns_hd = [f"{col}_S3" for col in columns_hd]

print("year" , "S1", "S2", "S3")
times =[]
for year in pipes_dict.keys():

    # Start counting the execution time
    
    st_annual = float(time.time())

    blocks = blocks_dict[year]
    pipes = pipes_dict[year]
   

    pipes.loc[:, "replace_S1"] = 0
    pipes.loc[:, "replace_S2"] = 0
    pipes.loc[:, "replace_S3"] = 0
    pipes.loc[:, n_columns_hd] = 0


    count1=0
    count2=0
    count3=0
    for idx, p in pipes.iterrows():
        if pipes.loc[idx, "failed"] or pipes.loc[idx, "upsized"]:
            count1+=1
            pipes.loc[idx, "replace_S1"] = 1

        if pipes.loc[idx, "failed"]: 
            count2+=1
            pipes.loc[idx, "replace_S2"] = 1

            if pipes.loc[idx, "upsized"]:
                # The diameter is already correct for the current year
                pass 
        
        if pipes.loc[idx, "failed"]:
            count3+=1
            pipes.loc[idx, "replace_S3"] = 1
            if pipes.loc[idx, "upsized"]:
                built_year = pipes.loc[idx, "built_year"].values
                pipes.loc[idx,n_columns_hd] = pipes_dict[built_year].loc[idx,columns_hd]  # this is actually saving the original dimension of the pipe whe it was built 
    
    pipes_dict[year] = pipes.copy()

    initial_time = time.time() - st_annual
    verbose_(f"\n time: {initial_time} s\n", notify)
    times.append(initial_time)

    print(year , count1, count2, count2)
    
    

# %%
times

# %%
pipes_dict[2028][pipes_dict[2028]["upsized"]>=1]

# %% [markdown]
# Update information in the blocks

# %%
#update blocks info with deterioration pipes
for year in pipes_dict.keys():
    blocks =  blocks_dict[year] 
    pipes = pipes_dict[year]    
    for idx, pipe in pipes.iterrows():
        pipe_id = pipes.loc[idx, "BlockID"]
        
        # get the corresponding block for the current pipe
        block =  blocks.loc[blocks["BlockID"] == pipe_id, "BlockID"]
        print(block)
        block_id = block.values
        block_idx = block.index
        # print(pipe_id, block_id, block_idx)

        if pipe_id == block_id:
            blocks.loc[block_idx, "upsized"] = pipes.loc[idx, "upsized"]
            blocks.loc[block_idx, "replace_S1"] = pipes.loc[idx, "replace_S1"]
            blocks.loc[block_idx, "replace_S2"] = pipes.loc[idx, "replace_S2"]    
            blocks.loc[block_idx, "replace_S3"] = pipes.loc[idx, "replace_S3"]
            blocks.loc[block_idx, n_columns_hd] = pipes.loc[idx, n_columns_hd]
        else:
            verbose_("IDs of Pipe and Block and different. Check! ")
        
        blocks_dict[year] = blocks.copy()

# %%
blocks_dict[2028]

# %%
bool1 = blocks_dict[2028]["upsized"]>0
bool2 = blocks_dict[2028]["failed"]>0

bool_ = np.logical_or(bool1, bool2)
blocks_dict[2028].loc[bool_,("BlockID", "PE", "diameter", "has_wwtp", "failed", "upsized", "replace_S1", "replace_S2", "replace_S3")]

# %%
bool1 = pipes_dict[2028]["upsized"]>0
bool2 = pipes_dict[2028]["failed"]>0
bool_ = np.logical_or(bool1, bool2)
pipes_dict[2028].loc[bool_, ("BlockID", "PE", "diameter", "has_wwtp", "failed", "upsized", "replace_S1", "replace_S2", "replace_S3")]

# %%
pipes_dict[2028]

# %% [markdown]
# Write output files

# %%
# Save output files
for year in pipes_dict.keys():

    # Restart counting the execution time for each iteration
    st = float(time.time())
    verbose_(f"_______________save output {year} ________________ ", notify)

    blocks=blocks_dict[year]
    pipes=pipes_dict[year]

    # Step 9
    verbose_("\nStep 9 - Save output files ... ", notify)
    
    # check the coordinates reference system
    if not blocks.crs == pipes.crs:
        pipes.to_crs(blocks.crs)  # re-project to the same crs as the blocks

    ## 9.2 create shapefile
    save_shp_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks, year=year, iteration=iteration)
    save_shp_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes, year=year, iteration=iteration)


    ## 9.3 create csv file
    save_csv_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks, year=year, iteration=iteration)
    save_csv_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes, year=year, iteration=iteration)

    """ EXECUTION TIME """
    verbose_(f"\n time: {time.time() - st} s\n", notify)
    verbose_(f"_______________________________ ", notify)


# %% [markdown]
# # Run test case

# %%
plot=True
test_blocks_dict= dict()
test_pipes_dict=dict()

grid=10
case_study = f"Case_{grid}_grid"
input_path = TEST_DATA_DIR/case_study/"output/shp"
output_path = TEST_DATA_DIR /case_study/"output"
connectivity= "decentralised"

# %%
iteration = 1
for year in range(year_i, year_f+1, year_step):
     
    blocks, pipes = read_blocks_and_pipes(input_path, case_study, read_module=read_module, connectivity=connectivity, year=year, iteration=1)
    test_blocks_dict[year] = blocks.copy()
    test_pipes_dict[year]  = pipes.copy()

# %%
years_designed = list(range(year_i, year_f+1,year_step))
years_designed

# %%
test_pipes_dict.keys()

# %%


# %%
# identify upsized pipes
for year in test_pipes_dict.keys():

    pipes = test_pipes_dict[year]

    pipes.loc[:, "upsized"] = 0 

    if year != year_i:

        previous_pipes= test_pipes_dict[year-year_step]

        for idx, pipe in pipes.iterrows():

            id_pipe = pipes.loc[idx, "BlockID"] 
            
            if id_pipe  in previous_pipes["BlockID"].values:
                idx_previous = previous_pipes[previous_pipes["BlockID"]==id_pipe].index[0]
                
                id_previous = previous_pipes.loc[idx_previous, "BlockID"]
                # print(id_pipe, pipes.loc[idx, "diameter"] , "      ", id_previous, previous_pipes.loc[idx_previous, "diameter"] )
                
                if pipes.loc[idx, "diameter"] > previous_pipes.loc[idx_previous, "diameter"] :
                    
                    # print(id_pipe, pipes.loc[idx, "diameter"] , "      ", id_previous, previous_pipes.loc[idx_previous, "diameter"] )
                    pipes.loc[idx, "upsized"]=1   
            
        
    print(year, len(pipes.loc[pipes["upsized"]==1]))
        # if year == year_o +40:
        # break

# %%
columns_hd = ["diameter",      # pipe diameter [m]
            "slope",        # pipe slope
            "sww_length",    # pipe length
            # "fill_ratio",   # filling ration
            # "y_max",        # maximum flow depth
            # "y_n",           # normal flow depth
            # "angle",        # internal angle for circular pipes
            # "perimeter",    # wet perimeter
            # "top_width",     # top width
            # "h_radius",       # hydraulic_radius
            # "area",         # wet area
            # "velocity",     # flow velocity
            # "shear",        # shear stress
            # "froude",       # Froude number
            # "exact_flow",     # current flow in the pipe
            ] 
n_columns_hd = [f"{col}_S3" for col in columns_hd]

print("year" , "S1", "S2", "S3")
times =[]
for year in test_pipes_dict.keys():

    # Start counting the execution time
    
    st_annual = float(time.time())

    blocks = test_blocks_dict[year]
    pipes = test_pipes_dict[year]
   

    pipes.loc[:, "replace_S1"] = 0
    pipes.loc[:, "replace_S2"] = 0
    pipes.loc[:, "replace_S3"] = 0
    pipes.loc[:, n_columns_hd] = 0


    count1=0
    count2=0
    count3=0
    for idx, p in pipes.iterrows():
        if pipes.loc[idx, "failed"] or pipes.loc[idx, "upsized"]:
            count1+=1
            pipes.loc[idx, "replace_S1"] = 1

        if pipes.loc[idx, "failed"]: 
            count2+=1
            pipes.loc[idx, "replace_S2"] = 1

            if pipes.loc[idx, "upsized"]:
                # The diameter is already correct for the current year
                pass 
        
        if pipes.loc[idx, "failed"]:
            count3+=1
            pipes.loc[idx, "replace_S3"] = 1
            if pipes.loc[idx, "upsized"]:
                built_year = pipes.loc[idx, "built_year"].values
                pipes.loc[idx,n_columns_hd] = test_pipes_dict[built_year].loc[idx,columns_hd]  # this is actually saving the original dimension of the pipe whe it was built 
    
    test_pipes_dict[year] = pipes.copy()

    initial_time = time.time() - st_annual
    verbose_(f"\n time: {initial_time} s\n", notify)
    times.append(initial_time)

    print(year , count1, count2, count2)
    
    

# %%
#update blocks info with deterioration pipes
for year in test_pipes_dict.keys():
    blocks =  test_blocks_dict[year] 
    pipes = test_pipes_dict[year]    
    for idx, pipe in pipes.iterrows():
        pipe_id = pipes.loc[idx, "BlockID"]
        
        # get the corresponding block for the current pipe
        block =  blocks.loc[blocks["BlockID"] == pipe_id, "BlockID"]
        print(block)
        block_id = block.values
        block_idx = block.index
        # print(pipe_id, block_id, block_idx)

        if pipe_id == block_id:
            blocks.loc[block_idx, "upsized"] = pipes.loc[idx, "upsized"]
            blocks.loc[block_idx, "replace_S1"] = pipes.loc[idx, "replace_S1"]
            blocks.loc[block_idx, "replace_S2"] = pipes.loc[idx, "replace_S2"]    
            blocks.loc[block_idx, "replace_S3"] = pipes.loc[idx, "replace_S3"]
            blocks.loc[block_idx, n_columns_hd] = pipes.loc[idx, n_columns_hd]
        else:
            verbose_("IDs of Pipe and Block and different. Check! ")
        
        test_blocks_dict[year] = blocks.copy()

# %%
year_= 2018
bool1 = test_pipes_dict[year_]["upsized"]>0
bool2 = test_pipes_dict[year_]["failed"]>0
bool_ = np.logical_or(bool1, bool2)
test_pipes_dict[year_].loc[bool_, ("BlockID", "PE", "diameter", "fill_ratio",  "has_wwtp", "failed", "upsized", "replace_S1", "replace_S2", "replace_S3")]

# %%
# Save output files
for year in test_pipes_dict.keys():

    # Restart counting the execution time for each iteration
    st = float(time.time())
    verbose_(f"_______________save output {year} ________________ ", notify)

    blocks=test_blocks_dict[year]
    pipes=test_pipes_dict[year]

    # Step 9
    verbose_("\nStep 9 - Save output files ... ", notify)
    
    # check the coordinates reference system
    if not blocks.crs == pipes.crs:
        pipes.to_crs(blocks.crs)  # re-project to the same crs as the blocks

    ## 9.2 create shapefile
    save_shp_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks, year=year, iteration=iteration)
    save_shp_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes, year=year, iteration=iteration)


    ## 9.3 create csv file
    save_csv_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_blocks, year=year, iteration=iteration)
    save_csv_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_pipes, year=year, iteration=iteration)

    """ EXECUTION TIME """
    verbose_(f"\n time: {time.time() - st} s\n", notify)
    verbose_(f"_______________________________ ", notify)



# %%



