# %% [markdown]
# # Deterioration model 

# %%
# region --- PYTHON LIBRARY IMPORTS ---
import sys
sys.path.append('../..')

import time
from math import *
from pathlib import Path
from random import randint, seed
import numpy as np
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import matplotlib.colors as colors
from scipy.optimize import curve_fit
# --- Spatial geometry ---
from shapely.geometry import Point, Polygon, LineString

# --- SND IMPORTS ---
# from help_functions.helper_functions import verbose_, cm2inch, truncate_colormap, set_file_name
# from algorithm import *
from ssnd.defaults import *
from ssnd.help_functions.helper_functions import *
from ssnd.help_functions.plot_maps import *
# from deterioration import *
#endregion 


module="deterioration"
read_module = "hydraulic_design"

verbose = False  # set to TRUE for debugging
notify = True   # set to TRUE to track the progress


input_path = DATA_DIR/case_study/"output/shp"
output_path = DATA_DIR /case_study/"output"

blocks_dict=dict()
blocks_dict = dict()

# %%
bool_run_step_by_step= False


# %% [markdown]
# ## Functions

# %%
def weibull_cumulative_function(x:np.array, a:float, b:float):
    """ The Weibull distribution is a continuous probability distribution.
        [Source: (Scheidegger (2013))]

    Args:
        x (np.array): _description_
        a (float): scale
        b (float): shape

    Returns:
        _type_: _description_
    """       

    """ 

    :param x: array of pipe ages (variable)
    :param a: scale parameter (sets the growth rate)
    :param b: shape parameter (sets the displacement along the x-axis (translates the graph to the left or right)
    :return:
    """
    # For x ≥ 0, and F(x; k; λ) = 0 for x < 0.

    return np.where(x > 0, 1 - np.exp(-(x / a) ** b), 0)

# %%
def weibull_density_function(x, a, b):
    """_summary_

    Args:
        x (_type_): _description_
        a (_type_): scale
        b (_type_): shape

    Returns:
        _type_: _description_
    """
    k = (1/a)**b 
    return np.where(x > 0, k*b*(x**(b-1))*(np.exp(-k*(x**b))), 0)

# %%
def cond_prob_failure_age (previous_age, current_age, a, b):
    """_summary_

    Args:
        previous_age (_type_): _description_
        current_age (_type_): _description_
        a (_type_): _description_
        b (_type_): _description_

    Returns:
        _type_: _description_
    """
    F0 = weibull_cumulative_function(previous_age, a, b)
    F1 = weibull_cumulative_function(current_age, a, b)
    p_cond_failure = (F1 - F0) / (1 - F0)
    return p_cond_failure

# %%
def weibull_survival_density_function(x, a, b):
    k = (1/a)**b 
    return np.where(x > 0, 1-(k*b*(x**(b-1))*(np.exp(-k*(x**b)))), 1)

# %%
def weibull_survival_function(x, a, b):
 
    return np.where(x > 0, np.exp(-(x / a) ** b), 1.0)

# %% [markdown]
# ## Define deterioration model


# %% [markdown]
# 
# %%
def get_df_from_previous_time_step(df_dict, current_year, time_step=1, verbose:bool=False, notify:bool=True):
    """ Get a dataframe from the previous time step

    Args:
        df_dict (_type_): _description_
        current_year (_type_): _description_
        time_step (int, optional): _description_. Defaults to 1.
        verbose (bool, optional): _description_. Defaults to False.
        notify (bool, optional): _description_. Defaults to True.

    Returns:
        _type_: _description_
    """
       
    previous_year = current_year - time_step    
    
    if previous_year in df_dict.keys(): 
        verbose_(f"{current_year}: getting information from previous year {previous_year}", verbose=verbose)   
        previous_df =df_dict[previous_year]
    
    # if the previous year falls outside the range
    elif current_year in df_dict.keys():    
        verbose_(f"{current_year}: getting information from current year {current_year}", verbose=verbose)
        previous_df =df_dict[current_year] # return current year
    
    return previous_df


# %% [markdown]
# pipes deterioration function

# %%
def pipes_deterioration(pipes_dict:dict, year_o:int, year_ph:int, year_step:int, a: float, b:float, times:list=None, verbose:bool=False):
    """_summary_

    Args:
        pipes_dict (dict): _description_
        year_o (int): _description_
        year_ph (int): _description_
        year_step (int): _description_
        a (float): _description_
        b (float): _description_
        times (list, optional): _description_. Defaults to None.
        verbose (bool, optional): _description_. Defaults to False.

    Returns:
        _type_: _description_
    """    
    

    if not times:
        times = []


    # Set the random seed
    # random_seed=10
    # seed(random_seed)
    
    for year in range(year_o, year_ph+1, year_step):
        # Start counting the execution time    
        st_ = float(time.time())
        
        pipes = pipes_dict[year]

        # warm-up starts at year_o with age=0
        if year == year_o: 
          
            previous_age=0   
            
            # # Set the random seed
            # random_seed=10
            # seed(random_seed)
            for idx, pipe in pipes.iterrows():
                
                age = pipes.loc[idx, "age"]
                p_cond_failure = cond_prob_failure_age (previous_age=previous_age, current_age=age, a=a, b=b) 
                pipes.loc[idx, "p_fail"] = float(p_cond_failure)
                
                # probability of failura given by a binomial distribution and the conditional probalitity 
                # of failure for each pipe p, with n is the number of trials (n=1)
                failure = np.random.binomial(n=1, p=p_cond_failure, size=None)   
                pipes.loc[idx, "failed"] = failure

            pipes_dict[year] = pipes
        
        else:
            
            pipes_old = get_df_from_previous_time_step(pipes_dict, year, time_step=year_step, verbose=verbose, notify=notify)

            # # Set the random seed
            # random_seed=10
            # seed(random_seed)
            for idx, pipe in pipes.iterrows():
                
                id_ = pipes.loc[idx, "BlockID"]

                # new pipe build in the current year with age=0
                if id_ not in list(pipes_old["BlockID"]):
                    current_age = 0 + year_step           # add the time step
                    pipes.loc[idx, "age"] =  current_age  # recently built
                    p_cond_failure = cond_prob_failure_age (previous_age=0, current_age=current_age, a=a, b=b)
                    pipes.loc[idx, "p_fail"] = float(p_cond_failure)
                    failure=np.random.binomial(n=1, p=p_cond_failure, size=None)
                    pipes.loc[idx, "failed"] = failure
                
                # existing pipe
                else: 
                    idx_old_pipe = pipes_old[pipes_old["BlockID"]==id_].index[0]
                    
                    previous_age = pipes_old.loc[idx_old_pipe, "age"]
                    
                    if pipes_old.loc[idx_old_pipe, "failed"] == 1:
                        current_age = 0 + year_step           # add the time step
                        pipes.loc[idx, "age"] = current_age  
                        
                        p_cond_failure = cond_prob_failure_age (previous_age=0, current_age=current_age, a=a, b=b)
                        pipes.loc[idx, "p_fail"] = float(p_cond_failure)

                        pipes.loc[idx, "failed"] = np.random.binomial(n=1, p=p_cond_failure, size=None)

                    else:
                        previous_age = pipes_old.loc[idx_old_pipe, "age"]
                        current_age = previous_age + year_step # add the time step
                        pipes.loc[idx, "age"] = current_age  

                        p_cond_failure = cond_prob_failure_age (previous_age=previous_age, current_age=current_age, a=a, b=b)
                        pipes.loc[idx, "p_fail"] = float(p_cond_failure)

                        pipes.loc[idx, "failed"] = np.random.binomial(n=1, p=p_cond_failure, size=None)       
            
            pipes_dict[year] = pipes.copy()
        
            # """ EXECUTION TIME """
            # annual_time = time.time() - st_
            # verbose_(f"\n annual time: {annual_time} s\n", notify)
            # times.append(annual_time)
        
    return pipes_dict, times


def wwtp_deterioration_lifespan(blocks_dict:dict, year_o:int, year_ph:int, year_step:int, ls_wwtp: float, ls_wwtp_small:float, times:list=None, verbose:bool=False):
    """_summary_

    Args:
        blocks_dict (dict): _description_
        year_o (int): _description_
        year_ph (int): _description_
        year_step (int): _description_
        a (float): _description_
        b (float): _description_
        times (list, optional): _description_. Defaults to None.
        verbose (bool, optional): _description_. Defaults to False.

    Returns:
        _type_: _description_
    """    
    

    if not times:
        times = []

    for year in range(year_o, year_ph+1, year_step):
        # Start counting the execution time    
        st_ = float(time.time())
        
        blocks = blocks_dict[year]
        blocks_with_wwtp_large = blocks.loc[blocks_dict[year]["has_wwtp"]==1, :]
        blocks_with_wwtp_small = blocks.loc[blocks_dict[year]["has_wwtp"]==2, :]
        
        # warm-up starts at year_o with age=0
        if year == year_o: 
          
            previous_age=0   
            
            for idx, block in blocks_with_wwtp_large.iterrows():
                
                current_age = blocks.loc[idx, "age_wwtp"]
                if current_age >= ls_wwtp+1:                                
                    blocks.loc[idx, "p_fail_wwt"] = 1.0 
                    blocks.loc[idx, "fail_wwtp"] = 1
            
            for idx, block in blocks_with_wwtp_small.iterrows():
                
                current_age = blocks.loc[idx, "age_wwtp"]
                if current_age >= ls_wwtp_small+1:                                
                    blocks.loc[idx, "p_fail_wwt"] = 1.0 
                    blocks.loc[idx, "fail_wwtp"] = 1

            blocks_dict[year] = blocks
        
        else:
            
            blocks_old = get_df_from_previous_time_step(blocks_dict, year, time_step=year_step, verbose=verbose, notify=notify)
            # blocks_with_wwtp_large = blocks.loc[blocks_dict[year]["has_wwtp"]==1, :]
            # blocks_with_wwtp_small = blocks.loc[blocks_dict[year]["has_wwtp"]==2, :]
            
            for idx, block in blocks_with_wwtp_large.iterrows():
                
                id_ = blocks.loc[idx, "BlockID"]

                # new wwtp build in the current year with age=0
                if id_ not in list(blocks_old["BlockID"]):
                    current_age = 0 + year_step           # add the time step
                    blocks.loc[idx, "age_wwtp"] =  current_age  # recently built
                    if current_age >= ls_wwtp+1: 
                        blocks.loc[idx, "p_fail_wwt"] = 1.0
                        blocks.loc[idx, "fail_wwtp"] = 1
                    else:                
                        blocks.loc[idx, "p_fail_wwp"] = 0.0
                        blocks.loc[idx, "fail_wwtp"] = 0.0  
                
                # existing wwtp
                else: 
                    idx_old_pipe = blocks_old[blocks_old["BlockID"]==id_].index[0]
                    
                    previous_age = blocks_old.loc[idx_old_pipe, "age_wwtp"]
                    
                    # If the wwtp failed in the previous time step
                    if blocks_old.loc[idx_old_pipe, "fail_wwtp"] == 1:
                        current_age = 0 + year_step           # add the time step
                        blocks.loc[idx, "age_wwtp"] = current_age  
                        if current_age >= ls_wwtp+1: 
                            blocks.loc[idx, "p_fail_wwt"] = 1.0
                            blocks.loc[idx, "fail_wwtp"] = 1
                        else:                
                            blocks.loc[idx, "p_fail_wwp"] = 0.0
                            blocks.loc[idx, "fail_wwtp"] = 0.0  

                     # If the wwtp did NOT fail in the previous time step
                    else:
                        previous_age = blocks_old.loc[idx_old_pipe, "age_wwtp"]
                        current_age = previous_age + year_step # add the time step
                        blocks.loc[idx, "age_wwtp"] = current_age  
                        if current_age >= ls_wwtp+1: 
                            blocks.loc[idx, "p_fail_wwt"] = 1.0
                            blocks.loc[idx, "fail_wwtp"] = 1
                        else:                
                            blocks.loc[idx, "p_fail_wwp"] = 0.0
                            blocks.loc[idx, "fail_wwtp"] = 0.0  
                
            for idx, block in blocks_with_wwtp_small.iterrows():
            
                id_ = blocks.loc[idx, "BlockID"]

                # new wwtp build in the current year with age=0
                if id_ not in list(blocks_old["BlockID"]):
                    current_age = 0 + year_step           # add the time step
                    blocks.loc[idx, "age_wwtp"] =  current_age  # recently built
                    if current_age >= ls_wwtp_small+1: 
                        blocks.loc[idx, "p_fail_wwt"] = 1.0
                        blocks.loc[idx, "fail_wwtp"] = 1
                    else:                
                        blocks.loc[idx, "p_fail_wwp"] = 0.0
                        blocks.loc[idx, "fail_wwtp"] = 0.0  
                
                # existing wwtp
                else: 
                    idx_old_pipe = blocks_old[blocks_old["BlockID"]==id_].index[0]
                    
                    previous_age = blocks_old.loc[idx_old_pipe, "age_wwtp"]
                    
                    # If the wwtp failed in the previous time step
                    if blocks_old.loc[idx_old_pipe, "fail_wwtp"] == 1:
                        current_age = 0 + year_step           # add the time step
                        blocks.loc[idx, "age_wwtp"] = current_age  
                        if current_age >= ls_wwtp_small+1: 
                            blocks.loc[idx, "p_fail_wwt"] = 1.0
                            blocks.loc[idx, "fail_wwtp"] = 1
                        else:                
                            blocks.loc[idx, "p_fail_wwp"] = 0.0
                            blocks.loc[idx, "fail_wwtp"] = 0.0  

                    # If the wwtp did NOT fail in the previous time step
                    else:
                        previous_age = blocks_old.loc[idx_old_pipe, "age_wwtp"]
                        current_age = previous_age + year_step # add the time step
                        blocks.loc[idx, "age_wwtp"] = current_age  
                        if current_age >= ls_wwtp_small+1: 
                            blocks.loc[idx, "p_fail_wwt"] = 1.0
                            blocks.loc[idx, "fail_wwtp"] = 1
                        else:                
                            blocks.loc[idx, "p_fail_wwp"] = 0.0
                            blocks.loc[idx, "fail_wwtp"] = 0.0    
            
            blocks_dict[year] = blocks
        
            # """ EXECUTION TIME """
            # annual_time = time.time() - st_
            # verbose_(f"\n annual time: {annual_time} s\n", notify)
            # times.append(annual_time)
        
    return blocks_dict, times

# %%
def wwtp_deterioration_stochastic(blocks_dict:dict, year_o:int, year_ph:int, year_step:int, a: float, b:float, times:list=None, verbose:bool=False):
    """_summary_

    Args:
        blocks_dict (dict): _description_
        year_o (int): _description_
        year_ph (int): _description_
        year_step (int): _description_
        a (float): _description_
        b (float): _description_
        times (list, optional): _description_. Defaults to None.
        verbose (bool, optional): _description_. Defaults to False.

    Returns:
        _type_: _description_
    """    
    

    if not times:
        times = []

    # # Set the random seed
    # seed(random_seed)
    
    for year in range(year_o, year_ph+1, year_step):
        # Start counting the execution time    
        st_ = float(time.time())
        
        blocks = blocks_dict[year]
        blocks_with_wwtp = blocks.loc[blocks_dict[year]["has_wwtp"]>0, :]
        
        # warm-up starts at year_o with age=0
        if year == year_o: 
          
            previous_age=0   
            
            for idx, block in blocks_with_wwtp.iterrows():
                
                age = blocks.loc[idx, "age_wwtp"]
                p_cond_failure = cond_prob_failure_age (previous_age=previous_age, current_age=age, a=a, b=b) 
                blocks.loc[idx, "p_fail_wwt"] = float(p_cond_failure)
                
                # probability of failura given by a binomial distribution and the conditional probalitity 
                # of failure for each pipe p, with n is the number of trials (n=1)
                failure = np.random.binomial(n=1, p=p_cond_failure, size=None)   
                blocks.loc[idx, "fail_wwtp"] = failure

            blocks_dict[year] = blocks
        
        else:
            
            blocks_old = get_df_from_previous_time_step(blocks_dict, year, time_step=year_step, verbose=verbose, notify=notify)
            blocks_with_wwtp = blocks.loc[blocks_dict[year]["has_wwtp"]>0, :]
            
            for idx, block in blocks_with_wwtp.iterrows():
                
                id_ = blocks.loc[idx, "BlockID"]

                # new wwtp build in the current year with age=0
                if id_ not in list(blocks_old["BlockID"]):
                    current_age = 0 + year_step           # add the time step
                    blocks.loc[idx, "age_wwtp"] =  current_age  # recently built
                    p_cond_failure = cond_prob_failure_age (previous_age=0, current_age=current_age, a=a, b=b)
                    blocks.loc[idx, "p_fail_wwt"] = float(p_cond_failure)
                    failure=np.random.binomial(n=1, p=p_cond_failure, size=None)
                    blocks.loc[idx, "fail_wwtp"] = failure
                
                # existing wwtp
                else: 
                    idx_old_pipe = blocks_old[blocks_old["BlockID"]==id_].index[0]
                    
                    previous_age = blocks_old.loc[idx_old_pipe, "age_wwtp"]
                    
                    if blocks_old.loc[idx_old_pipe, "fail_wwtp"] == 1:
                        current_age = 0 + year_step           # add the time step
                        blocks.loc[idx, "age_wwtp"] = current_age  
                        
                        p_cond_failure = cond_prob_failure_age (previous_age=0, current_age=current_age, a=a, b=b)
                        blocks.loc[idx, "p_fail_wwt"] = float(p_cond_failure)

                        blocks.loc[idx, "fail_wwtp"] = np.random.binomial(n=1, p=p_cond_failure, size=None)

                    else:
                        previous_age = blocks_old.loc[idx_old_pipe, "age_wwtp"]
                        current_age = previous_age + year_step # add the time step
                        blocks.loc[idx, "age_wwtp"] = current_age  

                        p_cond_failure = cond_prob_failure_age (previous_age=previous_age, current_age=current_age, a=a, b=b)
                        blocks.loc[idx, "p_fail_wwp"] = float(p_cond_failure)

                        blocks.loc[idx, "fail_wwtp"] = np.random.binomial(n=1, p=p_cond_failure, size=None)       
            
            blocks_dict[year] = blocks.copy()
        
            # """ EXECUTION TIME """
            # annual_time = time.time() - st_
            # verbose_(f"\n annual time: {annual_time} s\n", notify)
            # times.append(annual_time)
        
    return blocks_dict, times

# %% [markdown]
# Update blocks information
# %%
def update_blocks_information(blocks_dict:gpd.GeoDataFrame,
                            pipes_dict:gpd.GeoDataFrame,
                            year_o:int=year_o, 
                            year_ph:int=year_ph,
                            columns:list=None):
    """_summary_
        #update blocks info with deterioration pipes

    Args:
        blocks_dict (gpd.GeoDataFrame): _description_
        pipes_dict (gpd.GeoDataFrame): _description_
        year_o (int, optional): _description_. Defaults to year_o.
        year_ph (int, optional): _description_. Defaults to year_ph.
        columns (list, optional): _description_. Defaults to None.

    Returns:
        _type_: _description_
    """
    for year in range(year_o, year_ph+1, year_step):
        blocks =  blocks_dict[year] 
        pipes = pipes_dict[year]    
        for idx, pipe in pipes.iterrows():
            pipe_id = pipes.loc[idx, "BlockID"]
            
            # get the corresponding block for the current pipe
            block =  blocks.loc[blocks["BlockID"] == pipe_id, "BlockID"]
            block_id = block.values[0]
            block_idx = block.index[0]
            # print(pipe_id, block_id, block_idx)

            if pipe_id == block_id:
                for c in columns:
                    blocks.loc[block_idx, c] = pipes.loc[idx, c]
            else:
                verbose_("IDs of Pipe and Block and different. Check! ")
            
        blocks_dict[year] = blocks.copy()
    return blocks_dict

# %%
def update_pipes_information(pipes_dict:gpd.GeoDataFrame,
                            blocks_dict:gpd.GeoDataFrame,
                            year_o:int=year_o, 
                            year_ph:int=year_ph,
                            columns:list=None):
    """_summary_
        #update blocks info with deterioration pipes

    Args:
        blocks_dict (gpd.GeoDataFrame): _description_
        pipes_dict (gpd.GeoDataFrame): _description_
        year_o (int, optional): _description_. Defaults to year_o.
        year_ph (int, optional): _description_. Defaults to year_ph.
        columns (list, optional): _description_. Defaults to None.

    Returns:
        _type_: _description_
    """
    for year in range(year_o, year_ph+1, year_step):
        pipes =  pipes_dict[year] 
        blocks = blocks_dict[year]    
        for idx, blk in blocks.iterrows():
            blk_ID = blocks.loc[idx, "BlockID"]
            
            # get the corresponding block for the current pipe
            if blk_ID in pipes["BlockID"].values:
                pp =  pipes.loc[pipes["BlockID"] == blk_ID, "BlockID"]
                pipe_id = pp.values[0]
                pipe_idx = pp.index[0]
                # print(pipe_id, block_id, block_idx)

                if blk_ID == pipe_id:
                    for c in columns:
                        pipes.loc[pipe_idx, c] = blocks.loc[idx, c]
                else:
                    verbose_("IDs of Pipe and Block and different. Check! ")
            
        pipes_dict[year] = pipes.copy()
    return pipes_dict

# %% [markdown]
# Plot distributions
# %%
def plot_distributions(dataframe, type_dist="frequency", label="failure", color="blue", 
                       reference_data=[[1, 55, 75, 100, 135],[0, 0.25, 0.50, 0.75, 0.99]], 
                       distribution="Weibull", 
                       years_tot_sim=0, 
                       verbose=False):

    """ Build FREQUENCIES the plot"""
    min_values = []
    max_values = []
    avg_values = []
    std_values = []
    
    key=type_dist + "_" + label
    
    for idx, f in dataframe[key].iteritems():
        min_values.append(min(dataframe[key][idx]))
        max_values.append(max(dataframe[key][idx]))
        avg_values.append(np.mean(dataframe[key][idx]))
        std_values.append(np.std(dataframe[key][idx]))
    totals = dataframe[key].sum(axis=1)
    
    lower_bound=[]
    upper_bound=[]
    for item1, item2 in zip(avg_values, std_values):
        lower_bound.append(item1 - item2)
        upper_bound.append(item1 + item2)

   
    "Fit input distribution"            
    __d_ages = np.array(reference_data[0])   # Reference quantile values from master thesis of Arreaza Bauer (2011)
    __d_failures = np.array(reference_data[1])  # Reference quantiles

    param_optimal, param_covariance = curve_fit(f=weibull_cumulative_function, xdata=__d_ages, ydata=__d_failures)
    verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)

    a = param_optimal[0]  # first parameter from the fitting process
    b = param_optimal[1]  # second parameter from the fitting process


    # Build the plot
    
    fig1, ax = plt.subplots(figsize=(cm2inch((9, 9))), dpi=300)
    fig1.set_facecolor("white")
    
    plt.rcParams['font.size'] = 8
    
    x= dataframe[key].columns
    y= avg_values
    ax.bar(x, y, align='center', width=0.8*year_step, alpha=0.3, color="grey", label=label.capitalize())
    
    ax.set_xlabel('Age [years]')
    # x_pos = np.arange(len(x), step=np.round(len(x)/5,0))
    x_labes = max(dataframe[key].columns[-1]+year_step, max(__d_ages))
    x_pos = np.arange(x_labes+5,step=np.round(x_labes/5,0))
    ax.set_xticks(x_pos)
    
    if type_dist.lower().startswith('f'):
        ax.set_title('Frequency distribution')
        ax.set_ylabel(f'Frequency of {label} [# pipes]')
        
    elif type_dist.lower().startswith('d'):
        ax.set_title('Probability density function')
        ax.set_ylabel(f'Probability of failed pipes [-]')  
        
        if "fail" in label.lower():
            "Plot input proability density distribution"
            failure_fit = dict([(i, weibull_density_function(i, a, b)) for i in range(max(__d_ages)+1)])         
            ax.plot(failure_fit.keys(), failure_fit.values(), c='black', linestyle='-', linewidth=0.5, mfc='none', label=f"Reference curve [a:{round(a,2)} b:{round(b,2)}]")
        if "surv" in label.lower():
            "Plot input proability density distribution"
            survival_fit = dict([(i, weibull_survival_density_function(i, a, b)) for i in range(max(__d_ages)+1)])         
            ax.plot(survival_fit.keys(), survival_fit.values(), c='black', linestyle='-', linewidth=0.5, mfc='none', label=f"Reference curve [a:{round(a,2)} b:{round(b,2)}]")
        
    elif type_dist.lower().startswith('c'):
        
        if "fail" in label.lower():
            verbose_("FAILURE PlOT", verbose=verbose)
            ax.set_title('Cumulative density function')
            ax.set_ylabel('Cumulative probability of failed pipes[-]')
                        
                    
            "Plot input cumulative distribution"
            failure_fit = dict([(i, weibull_cumulative_function(i, a, b)) for i in range(max(__d_ages)+1)])
            ax.plot(__d_ages, __d_failures, c='black', marker='s', markersize=4, linestyle="", linewidth=0.5, mfc='none', label="Reference points")       
            ax.plot(failure_fit.keys(), failure_fit.values(), c='black', linestyle='-', linewidth=0.5, mfc='none', label=f"Reference curve [a:{round(a,2)} b:{round(b,2)}]")
            
            "plot fitted curve based on histogram"
            x = list(dataframe[key].columns)
            y = avg_values           
            param_optimal, param_covariance = curve_fit(f=weibull_cumulative_function, xdata=x, ydata=y, p0=[a, b], maxfev=1000)
            verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)

            a = param_optimal[0]  # first parameter from the fitting process
            b = param_optimal[1]  # second parameter from the fitting process
        
            failure_curve = [weibull_cumulative_function(k, a, b) for k in x]  
            ax.plot(x, failure_curve, c='slategrey', alpha=1, linestyle='-', linewidth=0.5, label=f"Fitted curve [a:{round(a,2)} b:{round(b,2)}]")
            failure_fit2 = dict([(i, weibull_cumulative_function(i, param_optimal[0], param_optimal[1])) for i in x])
            
            
            #x_labes = max(len(dataframe[key].columns), max(__d_ages))
            #x_pos = np.arange(x_labes+5,step=np.round(x_labes/5,0))
#             x_pos = np.arange(max_age+5, step=np.round((max_age)/5, 0))
            #ax.set_xticks(x_pos)
#             ax.set_xticklabels(dataframe[key].columns)
            
        if "surv" in label.lower():
            verbose_("SURVIVAL PlOT", verbose=verbose)
            ax.set_title('Survival curve')
            ax.set_ylabel(f'Probability of survival of pipes [-]')
            
            "Plot input cumulative distribution"
            survival_fit = dict([(i, weibull_survival_function(i, a, b)) for i in range(max(__d_ages)+1)])                    
            ax.plot(survival_fit.keys(), survival_fit.values(), c='black', linestyle='-', linewidth=0.5, mfc='none', label=f"Reference curve [a:{round(a,2)} b:{round(b,2)}]")
            
            "plot fitted curve based on histogram"
            x = list(dataframe[key].columns)
            y = avg_values
            
            param_optimal, param_covariance = curve_fit(f=weibull_survival_function, xdata=x, ydata=y, p0=[a, b], maxfev=1000)
            verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)
            a = param_optimal[0]  # first parameter from the fitting process
            b = param_optimal[1]  # second parameter from the fitting process
        
            survival_curve = [weibull_survival_function(k, a, b) for k in x]
           
            ax.plot(x, survival_curve, c='slategrey', alpha=1, linestyle='-', linewidth=0.5, label=f"Fitted curve [a:{round(a,2)} b:{round(b,2)}]")
            
            #x_labes = max(len(dataframe[key].columns), max(__d_ages))
            #x_pos = np.arange(x_labes+5,step=np.round(x_labes/5,0))
#             x_pos = np.arange(max_age+5, step=np.round((max_age)/5, 0))
            #ax.set_xticks(x_pos)
#             ax.set_xticklabels(dataframe[key].columns)
            
    else:
            verbose_("type_dist should be 'frequency', 'density', 'cumulative'")
    
    
    
    
    
#     z = np.polyfit(x, y, 2)
#     p = np.poly1d(z)
#     plt.plot(x,p(x),"r--")
    ax.plot(x, y, color=color, linewidth=0.5, label="Average")
    ax.fill_between(x, lower_bound, upper_bound, alpha=0.3, linewidth=0, color=color, label="Standard deviation")
    
    
    
    ax.legend(loc="best",fontsize=5)
    ax.grid(True, alpha=0.2)
    # Save the figure and show
    plt.tight_layout()
    
    plt.show()
    # fig1.savefig(fr"C:\Users\duquevna\Dropbox\010_PhD\01_Python\SND_Manual\test/figures/P2_({dataframe[key].shape[0]}-{dataframe[key].shape[1]})[{years_tot_sim}]_{type_dist}_{label}.png", dpi=900)

    return True



# %% [markdown]
# ## Run deterioration
# %%
def run_deterioration(iterations=1,
                year_i = year_i,
                year_f = year_f, 
                year_step=year_step,  
                years_warm_up = years_warm_up,
                planning_horizon = planning_horizon,                  
                d_ages=d_ages,    
                d_failures=d_failures,
                ls_wwtp=ls_wwtp,
                ls_wwtp_small=ls_wwtp_small,
                wwtp_stochastic_failure=False,
                d_ages_wwtp=d_ages_wwtp,
                d_failures_wwtp=d_failures_wwtp,
                input_path=input_path,
                output_path=output_path,
                case_study=case_study,
                connectivity=connectivity,
                read_module="hydraulic_design",
                name_blocks=name_blocks,
                name_pipes=name_pipes,
                verbose=verbose,
                notify=notify):
    """"""
    
    blocks_dict = dict()
    pipes_dict = dict()
    times_dict = dict()
    

    year_o=year_i - years_warm_up
    year_ph = year_i + planning_horizon
    total_simulation_time = year_ph - year_o

    "FIT PARAMETERS"
    #region 
    # Fit pipe parameters for failure distribution based on input data
    __d_ages = np.array(d_ages)   # Reference quantile values from master thesis of Arreaza Bauer (2011)
    __d_failures = np.array(d_failures)  # Reference quantiles
    
   
    param_optimal, param_covariance = curve_fit(f=weibull_cumulative_function, xdata=__d_ages, ydata=__d_failures)
    verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)

    # Parameters for deterioration model
    a = param_optimal[0]
    b = param_optimal [1]
    #endregion
        
    #region 
    # Fit WWTP parameters for failure distribution based on input data
   
    if wwtp_stochastic_failure:
        __d_ages_wwtp = np.array(d_ages_wwtp)   # Reference quantile values from master thesis of Arreaza Bauer (2011)
        __d_failures_wwtp = np.array(d_failures_wwtp)  # Reference quantiles

        param_optimal, param_covariance = curve_fit(f=weibull_cumulative_function, xdata=__d_ages_wwtp, ydata=__d_failures_wwtp)
        verbose_(f"{distribution}: parameters {param_optimal}; covariance {param_covariance}", verbose=verbose)

        # Parameters for deterioration model
        a_wwtp = param_optimal[0]
        b_wwtp = param_optimal [1]
    #endregion
    
    # random_seed=10
    # seed(random_seed)
    "SIMULATION RUNS"
    for iteration in range(1,iterations+1): 
        
        verbose_(f"iteration {iteration}", notify)
        times = []
        years_designed = list(range(year_i, year_f+1,year_step))
        verbose_(f"designed years {years_designed}",verbose=verbose)
        
        # Start counting the execution time
        st = float(time.time())
        
        #region "GET NETWORK'S DATA"
        for year in range(year_o, year_ph+1, year_step):

            if year == year_o:
                blocks =  read_geoDataFrame(input_path, case_study, read_module=read_module, connectivity=connectivity, name=name_blocks, year=year_i)
                pipes =  read_geoDataFrame(input_path, case_study, read_module=read_module, connectivity=connectivity, name=name_pipes, year=year_i)
            if year in years_designed:
                blocks = read_geoDataFrame(input_path, case_study, read_module=read_module, connectivity=connectivity, name=name_blocks, year=year)
                pipes = read_geoDataFrame(input_path, case_study, read_module=read_module, connectivity=connectivity, name=name_pipes, year=year)
            
            # create new columns for deterioration module
            n_columns = ["age", "p_fail", "failed", "age_wwtp", "p_fail_wwt","fail_wwtp"] 
            n_values = [int(0), float(0.0), int(0), int(0), float(0.0), int(0)]
            blocks = add_empty_columns_df(blocks, columns=n_columns, values=n_values)
            pipes = add_empty_columns_df(pipes, columns=n_columns, values=n_values)
                            
            blocks_dict[year] = blocks.copy()
            pipes_dict[year] = pipes.copy()

            # verbose_(f"{year} has {len(pipes)} pipes", verbose=verbose)

        #endregion

        #region Output NAMES : Distinguish runs with wwtp_stochastic_failure in existing or generated deterioration files 
        if wwtp_stochastic_failure:
            name_deterioration_blocks= name_blocks + "_wwtpStochasticFailure_"
            name_deterioration_pipes= name_pipes + "_wwtpStochasticFailure_"
        else:
            name_deterioration_blocks= name_blocks
            name_deterioration_pipes= name_pipes    
        #endregion
        
        #region "COPY FAILURE INFORMATION FROM CENTRALISED/DECENTRALISED SIMULATION IF EXISTING"
        
        path_to_decentralised_df=input_path /module.lower()/"decentralised"/fr"decentralised_{case_study}_{name_deterioration_blocks}_({year})[{iteration}]/decentralised_{case_study}_{name_deterioration_blocks}_({year})[{iteration}].shp"
        print("path_to_decentralised:")
        print(path_to_decentralised_df)
        path_to_centralised_df=input_path /module.lower()/"centralised"/fr"centralised_{case_study}_{name_deterioration_blocks}_({year})[{iteration}]/centralised_{case_study}_{name_deterioration_blocks}_({year})[{iteration}].shp"
        print("path_to_centralised:")
        print(path_to_centralised_df)
        
        if connectivity == "centralised" and path_to_decentralised_df.exists():
            # copy the failure from decentralised case to the centralised case
            verbose_(f"{iteration} copying failure from decentralised case")
            
        
            "COPY FAILURE DATA FROM CENTRALISED APPROACH IF EXISTING"
            for year in range(year_o, year_ph+1, year_step):
                
                # Dataframes to copy for the current iteration from decentralised to centralised
                blocks_to_copy = read_geoDataFrame(input_path, case_study, read_module=module, connectivity="decentralised", name=name_deterioration_blocks, year=year, iteration=iteration)
                pipes_to_copy = read_geoDataFrame(input_path, case_study, read_module=module, connectivity="decentralised", name=name_deterioration_pipes, year=year, iteration=iteration)
                
                # Select blocks with failures in WWTP and copy to the corresponding pipe
                blocks_selection_to_copy = blocks_to_copy[blocks_to_copy["has_wwtp"]>0]
                # blocks_selection_to_copy = blocks_to_copy[blocks_to_copy["fail_wwtp"]==1]
                print("No. WWTP in decentralised case" , len (blocks_selection_to_copy), " in ", year)
                                
                for row, blk_to_copy in blocks_selection_to_copy.iterrows():
                    blk_id_to_copy = blocks_selection_to_copy.loc[row, "BlockID"]
                    
                    # Update blocks failure and age from (de)centralised wwtps
                    blocks = blocks_dict[year]                    
                    block_selected_idx = blocks[blocks["BlockID"]==blk_id_to_copy].index[0]
                    
                    # if the current block has a pipe in the centralised case
                    if blocks.loc[block_selected_idx, "has_wwtp"]<= 0: 
                        blocks.loc[block_selected_idx,"age"]= blocks_selection_to_copy.loc[row,"age_wwtp"]
                        blocks.loc[block_selected_idx,"p_fail"]= blocks_selection_to_copy.loc[row,"p_fail_wwt"]
                        blocks.loc[block_selected_idx,"failed"]= blocks_selection_to_copy.loc[row,"fail_wwtp"]
                    
                    # if the current block has a wwtp
                    else:
                        blocks.loc[block_selected_idx,"age_wwtp"]= blocks_selection_to_copy.loc[row,"age_wwtp"]
                        blocks.loc[block_selected_idx,"p_fail_wwt"]= blocks_selection_to_copy.loc[row,"p_fail_wwt"]
                        blocks.loc[block_selected_idx,"fail_wwtp"]= blocks_selection_to_copy.loc[row,"fail_wwtp"]
                        
                
                # Select blocks with failures in pipes and copy to the corresponding pipe
                pipes_selection_to_copy = pipes_to_copy[pipes_to_copy["has_infra"]>0]
                # pipes_selection_to_copy = pipes_to_copy[pipes_to_copy["failed"]==1]
                print("No. pipes in decentralised case" , len (pipes_selection_to_copy), " in ", year)
                
                
                for row, pipe_to_copy in pipes_selection_to_copy.iterrows():
                    blk_id_to_copy = pipes_selection_to_copy.loc[row, "BlockID"]
                    
                    # Update blocks failure and age
                    blocks = blocks_dict[year]              
                    block_selected_idx = blocks[blocks["BlockID"]==blk_id_to_copy].index[0]

                    blocks.loc[block_selected_idx,"age"] = pipes_selection_to_copy.loc[row,"age"]
                    blocks.loc[block_selected_idx,"p_fail"]= pipes_selection_to_copy.loc[row,"p_fail"]
                    blocks.loc[block_selected_idx,"failed"]= pipes_selection_to_copy.loc[row,"failed"]
                    a = blocks.loc[block_selected_idx,"age"] 
                    p = blocks.loc[block_selected_idx,"p_fail"]
                    f = blocks.loc[block_selected_idx,"failed"]
                    print(block_selected_idx, blk_id_to_copy,a,p,f)
                    
                    # Update pipes failure and age
                    pipes = pipes_dict[year]              
                    pipe_selected_idx = pipes[pipes["BlockID"]==blk_id_to_copy].index[0]
                    
                    pipes.loc[pipe_selected_idx,"age"]= pipes_selection_to_copy.loc[row,"age"]
                    pipes.loc[pipe_selected_idx,"p_fail"]= pipes_selection_to_copy.loc[row,"p_fail"]
                    pipes.loc[pipe_selected_idx,"failed"]= pipes_selection_to_copy.loc[row,"failed"]
                    a = pipes.loc[pipe_selected_idx,"age"]
                    p = pipes.loc[pipe_selected_idx,"p_fail"]
                    f = pipes.loc[pipe_selected_idx,"failed"]
                    print(pipe_selected_idx, blk_id_to_copy,a,p,f)
                    
                    
                # Save in dictionary od dataframes                 
                blocks_dict[year] = blocks.copy()
                pipes_dict[year] = pipes.copy()
            
            
            
        elif connectivity == "decentralised" and path_to_centralised_df.exists():
            # copy the failure from centralised case to the decentralised case
            verbose_(f"{iteration} copying failure from centralised case")
            
            "COPY FAILURE DATA FROM CENTRALISED APPROACH IF EXISTING"
            for year in range(year_o, year_ph+1, year_step):
                
                # Dataframes to copy for the current iteration from centralised to decentralised
                blocks_to_copy = read_geoDataFrame(input_path, case_study, read_module=module, connectivity="centralised", name=name_deterioration_blocks, year=year, iteration=iteration)
                pipes_to_copy = read_geoDataFrame(input_path, case_study, read_module=module, connectivity="centralised", name=name_deterioration_pipes, year=year, iteration=iteration)
                
                # Select blocks with failures in C-WWTP and copy to the corresponding WWTP
                blocks_selection_to_copy = blocks_to_copy[blocks_to_copy["has_wwtp"]>0]
                print("No. WWTP in centralised case" , len (blocks_selection_to_copy), " in ", year)
                                
                for row, blk_to_copy in blocks_selection_to_copy.iterrows():
                    blk_id_to_copy = blocks_selection_to_copy.loc[row, "BlockID"]
                    
                    # Update blocks failure and age from (de)centralised wwtps
                    blocks = blocks_dict[year]                    
                    block_selected_idx = blocks[blocks["BlockID"]==blk_id_to_copy].index[0]
                    
                    # if the current block has a pipe
                    if blocks.loc[block_selected_idx, "has_wwtp"]<= 0: 
                        
                        blocks.loc[block_selected_idx,"age_wwtp"]= blocks_selection_to_copy.loc[row,"age_wwtp"]
                        blocks.loc[block_selected_idx,"p_fail_wwt"]= blocks_selection_to_copy.loc[row,"p_fail_wwt"]
                        blocks.loc[block_selected_idx,"fail_wwtp"]= blocks_selection_to_copy.loc[row,"fail_wwtp"]
                        
                        # a = blocks.loc[block_selected_idx,"age"]
                        # p = blocks.loc[block_selected_idx,"p_fail"]
                        # f = blocks.loc[block_selected_idx,"failed"]
                        # print(block_selected_idx, blk_id_to_copy,a,p,f)
                    
                    # if the current block has a wwtp
                    else:
                        blocks.loc[block_selected_idx,"age_wwtp"]= blocks_selection_to_copy.loc[row,"age_wwtp"]
                        blocks.loc[block_selected_idx,"p_fail_wwt"]= blocks_selection_to_copy.loc[row,"p_fail_wwt"]
                        blocks.loc[block_selected_idx,"fail_wwtp"]= blocks_selection_to_copy.loc[row,"fail_wwtp"]
                        
                
                # Select blocks with failures in pipes and copy to the corresponding pipe/WWTP
                pipes_selection_to_copy = pipes_to_copy[pipes_to_copy["has_infra"]>0]
                # pipes_selection_to_copy = pipes_to_copy[pipes_to_copy["failed"]==1]
                print("No. pipes in centralised case" , len (pipes_selection_to_copy), " in ", year)
                
                
                for row, pipe_to_copy in pipes_selection_to_copy.iterrows():
                    blk_id_to_copy = pipes_selection_to_copy.loc[row, "BlockID"]
                    
                    # Update blocks failure and age
                    blocks = blocks_dict[year]              
                    block_selected_idx = blocks[blocks["BlockID"]==blk_id_to_copy].index[0]

                    # if the current block has a pipe
                    if blocks.loc[block_selected_idx, "has_wwtp"]<= 0: 
                        blocks.loc[block_selected_idx,"age"] = pipes_selection_to_copy.loc[row,"age"]
                        blocks.loc[block_selected_idx,"p_fail"]= pipes_selection_to_copy.loc[row,"p_fail"]
                        blocks.loc[block_selected_idx,"failed"]= pipes_selection_to_copy.loc[row,"failed"]
                        a = blocks.loc[block_selected_idx,"age"] 
                        p = blocks.loc[block_selected_idx,"p_fail"]
                        f = blocks.loc[block_selected_idx,"failed"]
                        print(block_selected_idx, blk_id_to_copy,a,p,f)
                        
                        # Update pipes failure and age
                        pipes = pipes_dict[year]              
                        pipe_selected_idx = pipes[pipes["BlockID"]==blk_id_to_copy].index[0]
                        
                        pipes.loc[pipe_selected_idx,"age"]= pipes_selection_to_copy.loc[row,"age"]
                        pipes.loc[pipe_selected_idx,"p_fail"]= pipes_selection_to_copy.loc[row,"p_fail"]
                        pipes.loc[pipe_selected_idx,"failed"]= pipes_selection_to_copy.loc[row,"failed"]
                        
                    # if the current block has a wwtp
                    else: 
                        blocks.loc[block_selected_idx,"age_wwtp"] = pipes_selection_to_copy.loc[row,"age"]
                        blocks.loc[block_selected_idx,"p_fail_wwt"]= pipes_selection_to_copy.loc[row,"p_fail"]
                        blocks.loc[block_selected_idx,"fail_wwtp"]= pipes_selection_to_copy.loc[row,"failed"]
                    
                    
                # Save in dictionary od dataframes                 
                blocks_dict[year] = blocks.copy()
                pipes_dict[year] = pipes.copy()
                
        #endregion
        
        else:
            
            st_partial =time.time()
            " RUN FAILURE MODEL"
            verbose_(f"{iteration} running deterioration model")
            
            # Run your stochastic models
            # Run pipes deterioration  over the years of simulation, including the warm-up
            pipes_dict, times = pipes_deterioration(pipes_dict,
                                                    year_o=year_o, 
                                                    year_ph=year_ph, 
                                                    year_step=year_step,    
                                                    a=a,    
                                                    b=b,
                                                    times=times)
               
            blocks_dict = update_blocks_information(blocks_dict,
                                                    pipes_dict,
                                                    year_o=year_o, 
                                                    year_ph=year_ph,
                                                    columns=n_columns)
            
            # Run WWTP deterioration  over the years of simulation, without warm-up 
            #Failure at lifespan
            blocks_dict, times = wwtp_deterioration_lifespan(blocks_dict,
                                                        year_o=year_i, 
                                                        year_ph=year_ph, 
                                                        year_step=year_step,    
                                                        ls_wwtp=ls_wwtp,    
                                                        ls_wwtp_small=ls_wwtp_small,
                                                        times=times)
            
            # Run wwtp deterioration over the years of simulation, including the warm-up
            #Stochastic failure
            if wwtp_stochastic_failure:
                blocks_dict, times = wwtp_deterioration_stochastic(blocks_dict,
                                                        year_o=year_o, 
                                                        year_ph=year_ph, 
                                                        year_step=year_step,    
                                                        a=a_wwtp,    
                                                        b=b_wwtp,
                                                        times=times)
            
            partial_time = time.time() - st_partial
            """ EXECUTION TIME """
            verbose_(f"\n Partial time: {partial_time} s\n", notify)
            
        total_time = time.time() - st
        """ EXECUTION TIME """
        verbose_(f"\n total time: {total_time} s\n", notify)
        

        # Save output files
        if True:
             
                
            for i, year in enumerate(pipes_dict.keys()):
                # Save output files
                # Restart counting the execution time for each iteration
                st_out = float(time.time())
                verbose_(f"\n\n_______________save output {year} ________________ ", notify)

                blocks=blocks_dict[year]
                pipes=pipes_dict[year]

                # Step 9
                verbose_("\nStep 9 - Save output files ... ", notify)
                
                # check the coordinates reference system
                if not blocks.crs == pipes.crs:
                    pipes.to_crs(blocks.crs)  # re-project to the same crs as the blocks

                # 9.2 create shapefile
                save_shp_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_deterioration_blocks, year=year, iteration=iteration)
                save_shp_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_deterioration_pipes, year=year, iteration=iteration)


                # 9.3 create csv file            
                save_csv_file(df=blocks, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_deterioration_blocks, year=year, iteration=iteration)
                save_csv_file(df=pipes, output_path=output_path, module=module, connectivity=connectivity,  case_study=case_study, name=name_deterioration_pipes, year=year, iteration=iteration)

                # verbose_(f"{'_'*20}", notify)

                """ EXECUTION TIME """
                verbose_(f"\n output time: {time.time() - st_out} s\n", notify)
                verbose_(f"_______________________________ ", notify)
            
                # total_time = time.time() - st
                # """ EXECUTION TIME """
                # verbose_(f"\n total time: {total_time} s\n", notify)
                # times.append(total_time)
        
        times.append(total_time)      
        times_dict[iteration] = times
    
           
            

    return blocks_dict, pipes_dict, times_dict

    

# %% [markdown]
# find files

# %%
if bool_run_step_by_step: result_dir = DATA_DIR/ case_study/ "output/csv" / module

# %%
def find_files_with_str (dir_path:Path, connectivity:str, case_study:str, name:str,  search_string:str ):
    """ @param dir_path: Define the directory path
        @param search_string: Define the string to search for in the file names
    """
    files=[]

    # Use the glob method to search for all files in the directory that contain the string
    for f in dir_path.iterdir():
        if not f.is_file() :
            continue
        f_name = f.name

        if search_string in f_name and name in f_name and f_name.startswith(connectivity):
            files.append(f)
        
    # Print the list of matching files
    # for file in files:
    #     print(file)
    
    # print(len(files))

    return files

# %% [markdown]
# get distributions over time after the deterioration has finished
# %%
def g_distributions(result_dir, age_range:list, iterations:int, connectivity=connectivity, case_study=case_study, name:str=name_pipes):
    
    df__ = pd.DataFrame(index=range(1), data=0, columns=age_range)


    # Prepare empty dictionaries
    df_distributions = dict([(k, df__.copy()) for k in ["frequency_failure",
                                                        "frequency_survival", 
                                                        "frequency_pipes", 
                                                        "density_failure",
                                                        "density_survival", 
                                                        "cumulative_failure",                                                     
                                                        "cumulative_survival",                                                    
                                                        ]])
    
    
    # list of files o include in the distributions
    files = []
    for iteration in range(1, iterations+1):
        files = find_files_with_str(dir_path=result_dir, connectivity=connectivity, case_study=case_study, name=name, search_string=f'[{iteration}]')

        if not files: 
            continue
        
        first_failure_recorded = []
        
        frequency_failure = dict([(age_, 0) for age_ in age_range])
        frequency_survival = dict([(age_, 0) for age_ in age_range])
        frequency_pipes = dict([(age_, 0) for age_ in age_range])
        
        for file in files:
        
        # LOOP PER SIMULATION RUN
            pipes = pd.read_csv(file, sep=";", header=0, usecols=["BlockID", "age", "failed"])

            amount_pipes = pipes.shape[0]
            
            for idx, pipe in pipes.iterrows():
                pipe_id = pipes.loc[idx, "BlockID"]

                # print(idx, year, pipe[f'age_{year}'], pipe[f'failed_{year}'])

                age = pipe[f'age']

                if pipe_id not in first_failure_recorded:
                    if pipe[f'failed'] == 1:
                        frequency_pipes[age] += 1
                        frequency_failure[age] += 1

                        first_failure_recorded.append(pipe_id)
                        
                        
                    else:

                        frequency_pipes[age] += 1
                        frequency_survival[age] += 1
                        
        ## ------Finished loop through the pipes
            
        # Save to dataframes
        
        for age in age_range:

            df_distributions["frequency_failure"].loc[iteration, age] = frequency_failure[age]  # save failing pipes in the DataFrame
            df_distributions["frequency_survival"].loc[iteration, age] = frequency_survival[age]  # save surviving pipes in the DataFrame
            df_distributions["frequency_pipes"].loc[iteration, age] = frequency_pipes[age]  # save surviving pipes in the DataFrame
        
        # Probability density
        df_distributions["density_failure"].loc[iteration,:] = df_distributions["frequency_failure"].loc[iteration,:]/(sum(df_distributions["frequency_failure"].loc[iteration])* year_step)
        df_distributions["density_failure"].loc[iteration,:] = df_distributions["density_failure"].loc[iteration,:] * year_step
        
    # Cumulative density

    sum_= df_distributions["density_failure"].loc[:, 0]
    for age_, series in df_distributions["density_failure"].items():
        print(age_, series)
        if  age_==0:
            df_distributions["cumulative_failure"][age_] = series
        else:
            sum_ += series
    #         print(k, "\n", sum_)
            df_distributions["cumulative_failure"][age_] = sum_
    
    df_distributions["cumulative_failure"] = df_distributions["cumulative_failure"]*year_step
        
    return df_distributions


# %% [markdown]
# Empty DataFrame for failure curves 
# %%
if bool_run_step_by_step: 
    min_age=0
    max_age= max(max(__d_ages), tot_simulation_year)
    df__ = pd.DataFrame(index=range(iterations), data=0, columns=range(min_age, max_age+1, year_step))

    # Prepare empty dictionaries
    df_distributions = dict([(k, df__.copy()) for k in ["frequency_failure",
                                                    "frequency_survival", 
                                                    "frequency_pipes", 
                                                    "density_survival", 
                                                    "density_failure", 
                                                    "cumulative_survival",
                                                    "cumulative_failure"]])


 
# %% 
if bool_run_step_by_step: pd.set_option("display.max_columns",1000)

# %% [markdown]
# ## Run Test Case
""" UNCOMMENT TO RUN STEP BY STEP """
# %%
if bool_run_step_by_step:
    plot=True
    grid = 10
    case_study = f"Case_{grid}x{grid}_20xFlow"
    input_path = TEST_DATA_DIR/case_study/"output/shp"
    output_path = TEST_DATA_DIR /case_study/"output"
    connectivity= "decentralised"
    read_module="hydraulic_design"
    module="deterioration"

# %%
if bool_run_step_by_step:    
    tt = float(time.time())

    blocks_dict, pipes_dict, 
    times_dict = run_deterioration(iterations=1,
                year_i=year_i,
                year_f=year_f, 
                year_step=year_step,  
                years_warm_up=years_warm_up,
                planning_horizon=planning_horizon,                  
                d_ages=d_ages,    
                d_failures=d_failures,
                input_path=input_path,
                output_path=output_path,
                connectivity=connectivity,
                case_study=case_study,
                read_module=read_module,
                name_blocks=name_blocks,
                name_pipes=name_pipes,
                wwtp_stochastic_failure=True,
                verbose=verbose,
                notify=notify)

    total_time = time.time() - tt

# %% [markdown]
# ploting results of test case

# %%
if bool_run_step_by_step:
    result_dir = TEST_DATA_DIR /case_study/ "output/csv"/ module/connectivity
    result_dir

# %%
if bool_run_step_by_step:
    # create df to plot the probability distributions.
    age_range = range( 0, tot_simulation_year, year_step)
    print(age_range)

# %%
if bool_run_step_by_step:
    df_distributions= g_distributions(result_dir, age_range, iterations= 1)
    df_distributions

# %% 

# %%
if bool_run_step_by_step:
    sum_ = 0
    for col_, v in df_distributions["density_failure"].iteritems():
        if col_ - 1 == 0:

            df_distributions["cumulative_survival"][col_] = 1 - v
        else:
            sum_ += v

            df_distributions["cumulative_survival"][col_] = 1 - sum_
    df_distributions["cumulative_survival"]=df_distributions["cumulative_survival"]*year_step

# %%
if bool_run_step_by_step: df_distributions["density_failure"]

# %%
if bool_run_step_by_step:
    sum_=0
    for col_, _ in df_distributions["density_failure"].items():
        print(col_)
        v = df_distributions["density_failure"].loc[:,col_]
        print(v)
        if  col_==0:
            df_distributions["cumulative_failure"].loc[:,col_] = v
        else:
            sum_ += v
    #         print(k, "\n", sum_)
            df_distributions["cumulative_failure"].loc[:,col_] = sum_

    df_distributions["cumulative_failure"] *= year_step
    df_distributions["cumulative_failure"]
    
# %%



# %% [markdown]
# read and plot failure over time
# %%
if bool_run_step_by_step:
    test_blocks_dict=dict()
    test_pipes_dict=dict()
    name_b=name_blocks
    name_p=name_pipes

    for year in range(year_i, year_f+1, year_step):

            # Start counting the execution time
            st = float(time.time())

            # Read input 
            path_blocks=input_path/module/connectivity/fr"{connectivity.lower()}_{case_study}_{name_b}_({year})[1]/{connectivity.lower()}_{case_study}_{name_b}_({year})[1].shp"
            print(path_blocks)
            blocks = gpd.read_file(path_blocks)
            test_blocks_dict[year] = blocks
            
            
            path_pipes=input_path/module/connectivity/f"{connectivity.lower()}_{case_study}_{name_p}_({year})[1]/{connectivity.lower()}_{case_study}_{name_p}_({year})[1].shp"
            pipes = gpd.read_file(path_pipes)
            print(path_pipes)
            test_pipes_dict[year] = pipes

# %%
if bool_run_step_by_step:
    # test_pipes_dict[2038].replace( f'Non_failed',0, inplace=True)
    sum(test_pipes_dict[2023]["fail_wwtp"]>0)

# %%
if bool_run_step_by_step:
    plot_development(poly_dict=test_blocks_dict, 
                        lineStr_dict=test_pipes_dict, 
                        feature_poly="failed_wwt", units_poly = "[delta]", cmap_poly= pl.cm.viridis,
                        feature_lineStr="failed", units_lineStr = "[-]", cmap_lineStr=pl.cm.viridis,
                        module=module, connectivity=connectivity, case_study=case_study, plot=plot, 
                        backgroud_layers=["has_ww", "has_wwtp"], cmap_background_layers=[pl.cm.Greys, pl.cm.Blues],
                        path=output_path,
                        verbose=False,
                        )

# %%
# Copy deteriorarion from centralised to decentralised to match failures location
# iteration = 55
# path_to_module= TEST_DATA_DIR /case_study/ "output/shp"/ module
# for year in range(year_o, year_f, year_step):
#     path_to_shapefile_a= path_to_module/"centralised"/f"centralised_{case_study}_{name_blocks}_({year})[{iteration}]" #centralised
#     path_to_shapefile_b= path_to_module/"decentralised"/f"decentralised_{case_study}_{name_blocks}_({year})[{iteration}]" #decentralised
#     path_to_updatedshapefile_b= path_to_module/"decentralised"/f"decentralised_{case_study}_{name_blocks}_({year})[{iteration}](c)" #decentralised#decentralised
    
    
    
#     # specify the columns to copy from df_a to df_b
#     columns_to_copy = ["age", "p_fail", "failed"] 
#     copy_columns_between_dataframes(path_to_shapefile_a, path_to_shapefile_b,path_to_updatedshapefile_b, columns_to_copy)

# # %%
