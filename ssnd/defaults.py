"""
@file
@author Natalia Duque
@section LICENSE

Sequenced Sewer Networks Design (SSND)

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

## >>>>>>>>>>>>>>
import numpy as np
from math import pow
from pathlib import Path
import os

# ATTRIBUTES DECLARATION ---------------------------------------------------------------------------------------
"""
GLOBAL VARIABLES 
"""

PROJECT_DIR = Path(os.getcwd()).absolute().parent.parent
DATA_DIR = PROJECT_DIR / "Data" / "data"
TEST_DATA_DIR = PROJECT_DIR / "tests" / "data"

"""
USER DEFINED VARIABLES
"""
# case_study = "USTER_200"
# case_study = "Uster_200_08flow"
case_study = "Uster_200_200L"
# case_study = "USTER_200_x10Pop"
connectivity = "centralised"


name_blocks = "blocks"
name_pipes = "pipes"




#######################
# SIMULATION TIME
#######################

# Simulation time given the urban development input 
years_simulation = 20 # depends on the input data from the urban development simulation
year_step = 5  # time steps in years for the hydraulic design (based on Input form urban development)

year_i = 2018
year_f = year_i + years_simulation  # final year for the urban development simulation

# starting year for the warm-up of the network deterioration model, X years before the starting year for the simulation
years_warm_up = 130
year_o = year_i - years_warm_up 

planning_horizon = 20
year_ph = year_i + planning_horizon  # planning horizon to compute costs 


####################
#  TOPOLOGY
####################

drop = "max"

# Wastewater design flow factors: given the wastewater production per block 
peak_factor= 1.2 # average daily peak factor in Switzerland 
return_factor= 1.0 # return factor 
L_person_day= 200. # [L/person/day]  in Switzerland

scale_factor_PE = 2.8  # Scale factor for the industrial and commercial wastewater contamination load. Calibrated for the case study with the real PE data.
growth_rate_PE = 0.01
ARA_internal_WW = 0.01 #[m3/s]

convertionRate_Lday_m3sec = float(1./(1000.*24.*3600.))         #[L/person/day] to [m3/person/sec]
convertionRate_MLyear_m3sec = float(1000./(365.*24.*3600.))     #[ML/year] to [m3/sec]

test_grid = 6

####################
#  HYDRAULIC DESIGN
####################


# LIST OF COMMERCIAL PIPE DIAMETERS [in m]
# commercial_diameters = [float(val) for val in (0.225, 0.25, 0.35, 0.4, 0.5, 0.6, 0.80, 1.0, 1.20, 1.5, 2.0, 2.5, 3.0)]
commercial_diameters = [float(val) for val in (0.125, 0.16, 0.20, 0.25, 0.315, 0.4, 0.5, 0.63, 0.71, 0.8, 1.0, 1.20, 1.5, 2.0, 2.5, 3.0)] # in Switzerland
# commercial_diameters =  (0.2, 0.25, 0.3, 0.35, 0.38, 0.4,0.45, 0.5, 0.53, 0.6, 0.7, 0.80,
#  0.9, 1, 1.05, 1.20, 1.35, 1.4, 1.5 ,1.6,1.8, 2, 2.2, 2.4)

existing_treatment_id = 671  # Block ID with existing WWTP ARA_Uster(200m block size)

# PIPES INTERNAL ROUGHNESS
material = "pvc"  # choose material "pvc", "concrete", other


# MINIMUM AND MAXIMUM VELOCITY
v_min = 0.75  # For PVC pipes
v_max = 10.0  # For PVC pipes


# KINEMATIC VISCOSITY OF WATER [m2/s]
# nu = 1.14E-6   # at 15°C [Source: Butler and Davies(2011), Urban Drainage]

# EXCAVATION LIMITS
min_depth = 1  # Minimum excavation depth [m]
max_depth = 5  # Maximum excavation depth [m]

# PUMP height [m]
# pump = 7

# minimum discharge
min_discharge = 0.00000188  # [m3/s]

# PIPE SLOPE CHANGE
# A change in slopes implies a more precise design but increases the computational time.
# 1 cm per 100 linear meters
delta_slope = 0.0001  # [-]
min_slope_limit = 0.001  # [-]
max_slope_limit = 0.10  # [-]

# TYPE OF THE PIPE
# The pipes can be (1) outer-branch pipes or (2) inner-branch pipes
type_external = "external"
type_internal = 2


#######################
# DETERIORATION MODEL
#######################
random_seed=10
iterations = 1

initial_pipes_age = 0  # initial (arbitrary) pipe age for all pipes in the network

# ----------------------------------------------
# --------------REFERENCE  DATA ----------------
# ----------------------------------------------
d_ages_60 = np.array([1, 20, 30, 40, 60])  # Reference quantile values up to 60 years old pipes

d_ages = np.array([1, 55, 75, 100, 135])  # Reference quantile values from master thesis of Arreaza Bauer (2011)
d_failures = np.array([0, 0.25, 0.50, 0.75, 0.99])  # Reference quantiles
d_sim_times = np.linspace(start=0, stop=max(d_ages + 10), num=max(d_ages + 10))
distribution = "Weibull"


d_ages_wwtp = np.array([0, 15, 15, 15, 25])  # Reference quantile 
d_failures_wwtp = np.array([0, 0.25, 0.50, 0.75, 1.0])  # Reference quantiles

# -----


#######################
# COSTS
#######################

# life spans for the assets
ls_pipes=80  # 50% of Weibull function based on reference data from (Arreaza Bauer, 2011) 
ls_wwtp=25  # (Maurer and Herlyn 2006)
ls_wwtp_small=20  # (Maurer and Herlyn 2006) 

real_interest_rate= 0.02  # (Maurer and Herlyn 2006) 
scale_factor = 1.0 

# END OF ATTRIBUTES DECLARATION---------------------------------------------------------------------------------
