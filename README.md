# Pipe-by-Pipe (PBP) Sewer Dimensioning Algorithm

The PBP algorithm is a process to design a sewer network given a Network topology. 
The output of the model is the feasible hydraulic design with specific pipe diameters and slopes. 
We include the evaluation of hydraulic constraints, placement of pumping stations and drops when needed.

## Instalation of python dependencies
**venv folder with python.exe depenedency**
(in fot there) create a new environment folder named 'venv' for this project using the following command 
	`python -m venv venv`
	
Aactivate virtual environment using the file `activate.bat`. run the path to that file
	`C:\> <venv>\Scripts\activate.bat`

**python requirements**
Install python development requirements which help during the development process
	`pip install -r requirements_dev.txt`
	
**setup.py **
Run the following line in a new terminal of the current project to run the `setup.py` file which contains the project metadata and helps intalling the required python packages for the current project. This command installs the packages and dependencies in a **developers mode (-e)** in the **current directory (.)**. 

	`pip install -e .`

**gdal wheel**
To work with GIS data you need to use gdal. When the normal `pip install gdal`command does not work for the installation, you would probably need to installe it through a 'wheel' for the corresponding python version. Always make sure the wheel your download corresponds to the python version you are using in your project environment. 



## Input data
We start with a given topology of manholes and sections, for instance:

### manholes

The input data for the manholes is given first and separated by a single space in the following order:  
see "test\data\Example_Input-Data.txt" for the format  

First line : "Manholes" nuber_of_manholes(integer)  
Following lines, one per Manhole: id x y outlet  
- id: id(integer)  
- x: x-coordinate(float)  
- y: y-coordinate(float  
- z: z-coordinate(float)  
- outlet: 1 when the manhole is an outlet, 0 otherwise (boolean)  


**Manholes 6**

|id | x | y | z | outlet | 
|---:|---:|---:|---:|---:|
|0 | 1.0 |13.0 |10.0| 0 | 
|1 | 1.0 |13.0 |10.0| 0 |
|2 |2.0 |3.0 |10.0| 0 |
|3 | 8.0 |9.0 |10.0 |0 | 
|4 |15.0 |11.0 |10.0 |0 | 
|5 |14.0 |1.0 |10.0| 1 |

### sections
The input data for the sections include:

First line : "Sections" nuber_of_manholes(integer)  
Following lines, one per Pipe: id_up id_down type design_flow
- id of the pipe (integer)
- id_up: id of the upstream manhole (integer)
- id_down: id of the downstream manhole (integer)
- type: 1: external (upstream most) pipes, 2: internal pipes (boolean)
- design_flow: design flow for the pipe

**Sections 5**

|id | manhole_up| manhole_down | design_flow|
|---:|---:|---:|---:|
|0|3 |1 |0.2|
|1 |2 |1 |0.2|
|2 |4 |2 |0.1|
|3| 4| 2| 0.6|
|4| 5 |2 |0.4|


## Algorithm

1. Read the Input Data using the helper_functions.py `read_network_file` and save it into a dataframe with `network_to_datafram`
2. create already expected columns to save the hydraulic design in the pipes dataframe.
3. Run the algorithm using the `walk_graph` function performs the following steps:
	3.1. `prepare_hydraulics_data`
	3.2. Run the `walk_graph` function where each pipe gets design using the `find_best_diameter_slope` function. The metod starts by designing the upstreammost pipes and keeps going downwards until it finds intersection with other incoming pipes. The design continues when all the upstreampipes are designed until it reaches the outlet.

```python
"""
@file
@author Natalia Duque, Christian Foerster
@section LICENSE

Sequenced Sewer Networks Design (SSND)

This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import time
import importlib
from ssnd.defaults import *
from ssnd.algorithm import *
from ssnd.help_functions.helper_functions import *
from ssnd.cost_functions import *

""" READ INPUT FILE """
# calls the working path and input file from the global variables.
path = local_path
file = os.path.join(path, filename)
print("file path: {}".format(file))

st = float(time.time())  # Start counting the execution time

""" ORGANISE DATA """
# organize data in a dataframe
manholes, pipes = network_to_dataframe(read_network_file(file))
verbose_("{} pipes, {} manholes".format(pipes.shape[0], manholes.shape[0]), verbose=True)

""" PREPARE EXPECTED DATAFRAME"""
hydraulic_design_columns = ["length",
							"terrain_slope",
							"diameters",
							"hd_diameter",
							"chosen_slopes",
							"hd_slope",
							"up_elevations",
							"down_elevations",
							"pumps",
							"drops",
							"exact_flow",
							"normal_depth/diameter",
							"normal_depth",
							"angle",
							"h_radius",
							"area",
							"velocity",
							"Froude",
							"tau",
							# "cost_pipe",
							# "cost_pump",
							# "cost_drop",
							# "cost_total"
							]
add_empty_columns_df(pipes, hydraulic_design_columns)

""" RUN ALGORITHM """
# Run design algorithm by walking the graph from the extremes to the outfall
df_design = walk_graph(manholes, pipes, v_min=v_min, v_max=v_max, n=n, roughness=roughness,
					   min_depth=min_depth, max_depth=max_depth, diameters=commercial_diameters, verbose=True)

print("\n time: ", time.time() - st, "s\n")

""" PRINT STATEMENTS """
verbose_(" ---------- HYDRAULIC DESIGN ------------", verbose=True)
print_full_df(df_design)
verbose_(" ------------", verbose=True)

costs = run_costs(pipes, manholes)
verbose_("Total costs of the network: {} CHF".format(costs), verbose=True)


```