from setuptools import setup, find_packages

setup(
    name="ssnd",
    version="1.0",
    description="hydraulic design of sewer networks.",
    long_description="A tool for designing sewer networks to get a feasible slope-diameter combination for each pipe.",
    classifiers=["Development Status :: early stage", "Programming Language :: Python :: 3.7"],
    install_requires=["numpy", 
                        "pandas", 
                        "geopandas", 
                        #"gdal", 
                        "matplotlib", 
                        "plotly", 
                        "pathlib", 
                        "seaborn", 
                        "shapely", 
                        "scipy", 
                        "pyproj",
                        "inquirer", 
                        "pyswmm", 
                        "sctriangulate", 
                        "tqdm",
                        "mapclassify",
                        "cartopy"],
    packages=find_packages(exclude=("doc", ".git", ".idea", "venv", "Data")),
    keywords="Transitions in Urban  Networks - sewers wastewater management",
    author="Natalia Duque",
    author_email="natalia.duquevillarreal@eawag.ch",
    license="GNU GENERAL PUBLIC LICENSE v3",
)
